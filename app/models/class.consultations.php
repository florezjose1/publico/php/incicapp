<?php

class Consultations{
    public function updateUsers($campo,$valor,$id){
        $connection = new Connnection();
        $connect = $connection->get_connection();
        $query = "UPDATE usuarios SET $campo = :valor WHERE id_usu = :id_usu";
        $stm = $connect->prepare($query);
        $stm->bindParam(':valor',$valor);
        $stm->bindParam(':id_usu',$id);
        $stm->execute();
    }

    public function insertLogs($id_user,$date){
        $connection = new Connnection();
        $connect    = $connection->get_connection();
        $query      = "INSERT INTO logs(id_usu,entrada_users) VALUES(:id_usu,:entrada_users)";
        $stm        = $connect->prepare($query);
        $stm->bindParam(':id_usu',$id_user);
        $stm->bindParam(':entrada_users',$date);
        $stm->execute();
    }
}