<?php

    session_start();
    include 'models/conexion.php';
    $connection = new Connnection();
    $connect    = $connection->get_connection();
    if(isset($_SESSION['usuarios'])) {
?>

<?php
    require_once 'controladores/principalControllers.php';
    require_once 'controladores/vars.php';
    $query = $connect->prepare("SELECT * FROM usuarios WHERE id_usu = :idu");
    $query->execute(array(":idu"=>$_SESSION['usuarios']));
    $user = $query->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="es">
<head>
     <meta charset="utf-8">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="shortcut icon" href="../public/img/logosApp.ico" type="image/x-icon" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>IncicAp</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href='../public/css/compiled.css' rel='stylesheet' id='compiled.css-css'   type='text/css' media='all' />
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
    <!-- Material Design Bootstrap -->
    <link href="../public/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="../public/css/style.css" rel="stylesheet">
    <!-- estilos css de alertas -->
    <link rel="stylesheet" href="../public/css/jquery.toast.css">

    
    
</head>
<body class="backUsuarios">
    <?php include 'inc/header.php';?>



<div class="container-fluid conImgFondo">
    <div class="container-salto">

    </div>
    <div class="container contenido">
        <?php include 'inc/enfermedadess.php';?>
        <?php include 'inc/plagass.php';?>
        <?php include 'inc/cultivoss.php';?>
        <div class="volverInicio" id="irarriba"><a href="#">Volver al Inicio</a></div>


        <!-- menu usuarios -->
        <input type="hidden" id="deplegarMenu2" value="0">

        <!-- codigo de usuario-->
        <input type="hidden" id="cod_usu" value="<?php if(isset($user['id_usu'])){echo $user['id_usu'];} ?>">


        <!-- enfermedades-->
        <!-- input valor signo de despligue de cultivos de enfermedades-->
        <input type="hidden" id="opt_despl_cultivosEnfer" value="0">

        <!-- input cantidad de cultivos por usuario-->
        <input type="hidden" id="cant_cult_X_usuario" value="0">
        <!-- input para ocultar menu de cultivos-->
        <input type="hidden" id="i_ocultMenu_cult" value="0">

        <!-- input cantidad de lotes por cultivo por usuario-->
        <input type="hidden" id="cant_lotes_cult_X_usuario" value="0">
        <!-- input para ocultar menu de lotes-->
        <input type="hidden" id="i_ocultMenu_lotes" value="0">

        <!-- graficas de enfermedad-->
        <input type="hidden" id="graEnfermedad" value="0">

       

        <!-- input valor para submenu de detalles de enfermedades por cultivo-->
        <input type="hidden" id="opt_inf_enf_x_cultivo" value="0">


        <!-- input buscador enfermedades-->
        <input type="hidden" id="val_opt_busquedaEnf_Cult" value="0" >
        <!-- input ocultar menu de buscador enfermedades-->
        <input type="hidden" id="opt_ocult_bus_enf" value="0">




        <!-- input de inicio de sesion usuaio ejecutar funcion de enfermedades y plagas primer valor encontrado-->
        <input type="hidden" id="cult_inicio">
        <input type="hidden" id="lote_inicio">
        <input type="hidden" id="nomCult_inicio">



        <!-- enfermedades-->
        <!-- input valor signo de despligue de cultivos de enfermedades-->
        <input type="hidden" id="opt_despl_cultivosPlag" value="0">

        <!-- input cantidad de cultivos por usuario-->
        <input type="hidden" id="cant_cult_X_usuarioPlag" value="0">
        <!-- input para ocultar menu de cultivos-->
        <input type="hidden" id="i_ocultMenu_cult_Plag" value="0">

        <!-- input cantidad de lotes por cultivo por usuario-->
        <input type="hidden" id="cant_lotes_cult_X_usuarioPlag" value="0">
        <!-- input para ocultar menu de lotes-->
        <input type="hidden" id="i_ocultMenu_lotesPlag" value="0">

        <!-- graficas de Plagas-->
        <input type="hidden" id="graPlag" value="0">


        <!-- input valor para submenu de detalles de enfermedades por cultivo-->
        <input type="hidden" id="opt_inf_plag_x_cultivo" value="0">

        <!-- input buscador enfermedades-->
        <input type="hidden" id="val_opt_busquedaPlag_Cult" value="0" >
        <!-- input ocultar menu de buscador enfermedades-->
        <input type="hidden" id="opt_ocult_bus_Plag" value="0">

        <div class="conten_inicio_tuturial">
            <div class="cotainer contenido_tutorial">
                <div class="col-xs-12 separador "  ></div>
                    <div class="col-xs-12"><hr></div>

                    <div class="mensajesBienvenida">
                         <center>
                            <?php
                                if( $user['genero'] == 0 ) {
                                    $variable = "Bienvenido ";
                                }else{
                                    $variable = "Bienvenida ";
                                }
                            ?>
                            <h4 class="modal-title w-100" id="myModalLabel">
                            <?php
                                $name = $user['nombre'];
                                $porciones = explode(" ", $name);
                                echo $variable.ucwords($porciones[0]);
                            ?>
                            </h4>
                            <hr>
                            <br>
                            <p style="text-align: center; ">En esta sección podrás escoger la opción de ver nuestro video introductorio de como usar nuestro servicio.</p>
                            <div class="botones">
                                <center>
                                    <button type="button" class="btn btn-danger btn-sm saltar1" onclick="saltarTutorial()" >Saltar </button>
                                    <button type="button" class="btn btn-primary btn-sm" onclick="siguiente()">Ver</button>                        
                                </center>
                            </div>
                        </center><hr>
                    </div>
                    <div class="videoIntro"  >
                    <center><p>Video introductorio</p></center>
                        <div class="video-responsive">
                           <!-- <iframe   src="https://www.youtube.com/embed/FqsYx-4iGFA" frameborder="0" allowfullscreen="allowfullscreen"></iframe>-->
                        </div>
                        <center>
                            <button type="button" class="btn btn-danger btn-sm" onclick="saltarVideo()" >Omitir</button>
                        </center>
                        <hr>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
    
   <!-- JQuery -->
    <script src="../public/js/jquery-1.9.1.min.js "></script>
    <script type="text/javascript" src="../public/js/jquery.toast.js"></script>

    <script src="../public/js/highcharts.js"></script>
    <script src="../public/js/exporting.js"></script>
    <script src="../public/js/data.js"></script>
    <script src="../public/js/drilldown.js"></script>

    <script src="../public/js/functionss.js"></script>
    <script src="../public/js/enfermedadess.js"></script>
    <script src="../public/js/plagass.js"></script>
    <script src="../public/js/cultivoss.js"></script>
    <!---<script src="../public/js/turs.js"></script>-->

    
    <!--<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>-->
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="../public/js/tether.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../public/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="../public/js/mdb.min.js"></script>
    <script type="text/javascript" src="../public/js/jquery.form.js"></script>
    <script type="text/javascript" src="../public/js/updateUsers.js"></script>

    <script>
        $(document).ready(function() {
            funtion_inicio();
        });
    </script>
    


</body>
</html>

    
<?php
    }else{
        echo '<script> window.location="../index.php"; </script>';
    }
?>