<?php

require_once 'conexion.php';
date_default_timezone_set('America/Bogota');

class PrincipalController extends Conexion {

    // consultar informacion de usuario referente a si este tiene registro de cultivos hecho o no.!
    public function InformacionUsuario($request) {
        $usu = htmlentities(addslashes($request->usu));
        $query ="SELECT * FROM `registo_cultivo` WHERE `id_usu` = '$usu'  ";
        $result = $this->EjecutarAppCultivos($query);
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id'=>$row->id_usu, 'descripcion'=>$row->descripcion  );
            if ($post != 0 ) {
                echo true;
            }else{
                echo false;
            }
        }   
    }

    public function cultivosXusuario($request) {
        $usu = htmlentities(addslashes( $request->usu ));
        $query="SELECT tipo_cultivos.id_cult, tipo_cultivos.nombre, registo_cultivo.lotes FROM `tipo_cultivos`,`registo_cultivo` WHERE tipo_cultivos.id_cult = registo_cultivo.tipo_cultivo AND id_usu = '$usu' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[] = array( 'id'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre));
        }
        echo json_encode($post);
    }
    # funcion consulta de lotes por cultivo registrados por usuario
    public function lotescultivosXusuario($request) {
        $usu = htmlentities(addslashes( $request->usu ));
        $id_cult = htmlentities(addslashes( $request->id_cult ));
        $query="SELECT registo_cultivo.lotes FROM `registo_cultivo` WHERE id_usu = '$usu' AND registo_cultivo.tipo_cultivo = '$id_cult' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[] = array('lotes'=>utf8_encode($row->lotes));
        }
        echo json_encode($post);
    }
    #consultas enfermedades
    public function informacionEnfermedadesXcultivoLote($request) {
        $usu = htmlentities(addslashes( $request->usu ));
        $cult = htmlentities(addslashes( $request->id_cult ));
        $lote = htmlentities(addslashes( $request->lote ));
        
        $var = htmlentities(addslashes( $request->var ));

        $enf_1 = "enf_1_L".$lote;
        $enf_2 = "enf_2_L".$lote;
        $enf_3 = "enf_3_L".$lote;
        $enf_4 = "enf_4_L".$lote;
        $enf_5 = "enf_5_L".$lote;

        if ($var == 1 ) {
            $fecha1 = htmlentities(addslashes( $request->fecha1 ));
            $fecha2 = htmlentities(addslashes( $request->fecha2 ));
            $query = "SELECT id_registro, fecha_r,  
            tipo_cultivos.`nombre` AS cultivo,  
            $enf_1 AS enf1,      $enf_2 AS enf2, 
            $enf_3 AS enf3,      $enf_4 AS enf4, $enf_5 AS enf5,
            control_enfermedades.`id_usu`,  
            usuarios.`nombre` AS nom_us
            FROM usuarios, tipo_cultivos, control_enfermedades 
            WHERE usuarios.`id_usu` = control_enfermedades.`id_usu`
            AND control_enfermedades.`id_usu` = '$usu'
            AND control_enfermedades.`tipo_cultivo` = tipo_cultivos.`id_cult`
            AND control_enfermedades.`tipo_cultivo` = '$cult'  
            AND `fecha_r` >= '$fecha1' AND `fecha_r` <= '$fecha2'
            ORDER BY  id_registro DESC  
            ";
        }else{
            $query = "SELECT id_registro, fecha_r,  
            tipo_cultivos.`nombre` AS cultivo,  
            $enf_1 AS enf1,      $enf_2 AS enf2, 
            $enf_3 AS enf3,      $enf_4 AS enf4, $enf_5 AS enf5,
            control_enfermedades.`id_usu`,  
            usuarios.`nombre` AS nom_us
            FROM usuarios, tipo_cultivos, control_enfermedades 
            WHERE usuarios.`id_usu` = control_enfermedades.`id_usu`
            AND control_enfermedades.`id_usu` = '$usu'
            AND control_enfermedades.`tipo_cultivo` = tipo_cultivos.`id_cult`
            AND control_enfermedades.`tipo_cultivo` = '$cult'  
            ORDER BY  id_registro DESC  
            lIMIT 0,10
            ";
        }

        
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $fe = $row->fecha_r;
            $date=substr($fe, 0, 10 );

            $e1 = 0+$row->enf1;
            if ($e1 == 9999 ) {
                $e1 = 9999;
            }else{
                if ($e1 == 0 ) {
                  $e1 = 0;
                }
            }

            $e2 = 0+$row->enf2;
            if ($e2 == 9999 ) {
                $e2 = 9999;
            }else{
                if ($e2 == 0 ) {
                    $e2 = 0;
                }
            }
            
            $e3 = 0+$row->enf3;
            if ($e3 == 9999 ) {
                $e3 = 9999;
            }else{
                if ($e3 == 0 ) {
                    $e3 = 0;
                }
            }
            $e4 = 0+$row->enf4;
            if ($e4 == 9999 ) {
                $e4 = 9999;
            }else{
                if ($e4 == 0 ) {
                    $e4 = 0;
                }
            }
            $e5 = 0+$row->enf5;
            if ($e5 == 9999 ) {
                $e5 = 9999;
            }else{
                if ($e5 == 0 ) {
                    $e5 = 0;
                }
            }


            $post[]= array( 'id'=>$row->id_registro, 'fecha'=>$date, 'cultivo'=>utf8_encode($row->cultivo), 'enf1'=>$e1, 'enf2'=>$e2, 'enf3'=>$e3, 'enf4'=>$e4, 'enf5'=>$e5, 'id_usu'=>$row->id_usu, 'usuario'=>utf8_encode($row->nom_us)
            );
        }
        echo json_encode($post);
    }
    public function infoEnfCultivosX_lotes_Torta($request) {
        $id_cul = htmlentities(addslashes($request->id_cult));
        $id_usu = htmlentities(addslashes($request->usu));
        $lote = htmlentities(addslashes( $request->lote ));

        $enf_1 = "enf_1_L".$lote;
        $enf_2 = "enf_2_L".$lote;
        $enf_3 = "enf_3_L".$lote;
        $enf_4 = "enf_4_L".$lote;
        $enf_5 = "enf_5_L".$lote;

            $query =" SELECT `fecha_r`, AVG(`$enf_1`) AS enf1 FROM `control_enfermedades` WHERE `$enf_1` != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC lIMIT 0,10 ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $enf1 =0+$row->enf1;
            }
            $query =" SELECT `fecha_r`, AVG(`$enf_2`) AS enf2 FROM `control_enfermedades` WHERE $enf_2 != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC  ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $enf2=0+$row->enf2;
            }
            $query =" SELECT `fecha_r`, AVG(`$enf_3`) AS enf3 FROM `control_enfermedades` WHERE $enf_3 != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC  ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $enf3 =0+$row->enf3;
            }
            $query =" SELECT `fecha_r`, AVG(`$enf_4`) AS enf4 FROM `control_enfermedades` WHERE $enf_4 != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC  ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $enf4 =0+$row->enf4;
            }
            $query =" SELECT `fecha_r`, AVG(`$enf_5`) AS enf5 FROM `control_enfermedades` WHERE $enf_5 != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC  ";
            $result = $this->EjecutarAppCultivos($query);
            $post = array();
            while ($row = mysqli_fetch_object($result)) {
                $enf5 =0+$row->enf5;
                $post[]= array( 'fecha'=>$row->fecha_r, 'enf1'=>0+$enf1, 'enf2'=>0+$enf2, 'enf3'=>0+$enf3, 'enf4'=>0+$enf4, 'enf5'=>0+$enf5   );
            }
        echo json_encode($post);
    }
    #informacion de cultivos registrados por usuario
    public function InformacionEnfermedadesUser($request) {
        $id_cult = htmlentities(addslashes($request->id));
        $id_usu  = htmlentities(addslashes( $request->usu ));

        $query = "SELECT id_usu, id_enfe, id_cult, nombre, sigla, limite_porcentaje FROM registro_enfermedades WHERE id_usu = '$id_usu' AND id_cult = '$id_cult' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id_usu'=>$row->id_usu, 'id_enfe'=>$row->id_enfe, 'id_cult'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre), 'sigla'=>utf8_encode($row->sigla), 'limite'=>$row->limite_porcentaje);
        }
        echo json_encode($post);
    }
    // funcion que me muestra si el usuario tiene registrado cultivos para ser mostrados
    public function selectCultivosUsuario($request) {
        $id_usu = htmlentities(addslashes($request->id_usu));
        $query = "SELECT tipo_cultivos.nombre, registo_cultivo.descripcion, registo_cultivo.id, registo_cultivo.tipo_cultivo, registo_cultivo.lotes 
          FROM `tipo_cultivos`, registo_cultivo, usuarios 
          WHERE registo_cultivo.tipo_cultivo = tipo_cultivos.id_cult 
          AND registo_cultivo.id_usu = usuarios.id_usu 
          AND usuarios.`id_usu` = '$id_usu'  ";

        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id'=>$row->id, 'tipoC'=>$row->tipo_cultivo, 'nombre'=>utf8_encode($row->nombre), 'descripcion'=>utf8_encode($row->descripcion), 'lotes'=>$row->lotes  );
        }
        echo json_encode($post);
    }
    public function cultivosUsuario($request) {
        $usu = htmlentities(addslashes( $request->usu ));
        $query="SELECT tipo_cultivos.id_cult, tipo_cultivos.nombre FROM `tipo_cultivos`,`registo_cultivo` WHERE tipo_cultivos.id_cult = registo_cultivo.tipo_cultivo AND id_usu = '$usu' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[] = array( 'id'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre));
        }
        echo json_encode($post);
    }
    public function UpdateDetallesEnfermedad($request) {
        $id_usu     = htmlentities(addslashes( utf8_decode( $request->usu    )   ));
        $cod        = htmlentities(addslashes( utf8_decode( $request->cod    )   ));
        $cult       = htmlentities(addslashes( utf8_decode( $request->cul    )   ));
        $nombre     = htmlentities(addslashes( $request->nombre   ));
        $sigla      = htmlentities(addslashes( utf8_decode( $request->sigla  )   ));
        $limite     = htmlentities(addslashes( utf8_decode( $request->limite )   ));

        $query ="UPDATE registro_enfermedades SET nombre =  '".utf8_decode($nombre)."', sigla = '$sigla', limite_porcentaje = '$limite' WHERE id_usu = '$id_usu' AND id_enfe = '$cod'  AND id_cult = '$cult'  ";
        $result = $this->EjecutarAppCultivos($query);
        if ($result) {
            echo true;
        }else{
            echo false;
        }
    }
    public function InformacionEnfermedades($request) {
        $id_Cul = htmlentities(addslashes( $request->cul ));
        $id_enf = htmlentities(addslashes( $request->id ));

        $query = "SELECT id_enfe, limite_porcentaje, nombre,  sigla, id_cult,  descripcion,  sint_sig FROM enfermedades 
        WHERE id_cult = '$id_Cul' AND id_enfe = '$id_enf' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id_enfe'=>$row->id_enfe, 'limite'=>$row->limite_porcentaje, 'id_cult'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre), 'sigla'=>utf8_encode($row->sigla), 'descripcion'=>utf8_encode($row->descripcion), 'sint_sig'=>utf8_encode($row->sint_sig) );
        }
        echo json_encode($post);
    }
    public function insertEnfermedadCultivoLotes($request) {
        $variable   = htmlentities(addslashes( $request->variable));
        $id         = htmlentities(addslashes($request->id)) ;
        $fecha      = htmlentities(addslashes($request->fecha));
        $usu        = htmlentities(addslashes($request->usu));
        if ($variable == 1 ) {
            $query = "SELECT tipo_cultivo, fecha_r, id_usu FROM control_enfermedades WHERE fecha_r = '$fecha'  AND tipo_cultivo = '$id'  AND id_usu = '$usu'  ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $post[]= array( 'tipo_cultivo'=>$row->tipo_cultivo  );
                if ($post != 0 ) {
                    echo true;
                }else{
                    echo false;
                }
            }
        }else{
            if ($variable == 2 ) {
                $id1        = htmlentities(addslashes($request->id1));
                $planSem    = htmlentities(addslashes($request->planSem));
                $planEnf    = htmlentities(addslashes($request->planEnf));
                $lote       = htmlentities(addslashes($request->lote));
                $por =( ( $planEnf / $planSem) * 100);
                $e = '  enf_'.$id1.'_L'.$lote;
                $query="UPDATE `control_enfermedades` SET $e ='$por' 
                    WHERE  `fecha_r` = '$fecha'     AND `tipo_cultivo` = '$id'      AND `id_usu` = '$usu' ";
                $result = $this->EjecutarAppCultivos($query);
                if ($result) {
                    echo true;
                }else{
                    echo false;
                }
            }else{
                if ($variable == 3 ) {
                    $id = htmlentities(addslashes($request->id));
                    $id1 = htmlentities(addslashes($request->id1));
                    $planSem = htmlentities(addslashes($request->planSem));
                    $planEnf = htmlentities(addslashes($request->planEnf));
                    $fecha = htmlentities(addslashes($request->fecha));
                    $usu = htmlentities(addslashes($request->usu));
                    $lote = htmlentities(addslashes($request->lote));
                    $por =( ( $planEnf / $planSem) * 100);
                    $e = 'enf_'.$id1.'_L'.$lote;
                    $query="INSERT INTO `control_enfermedades`(`fecha_r`, `tipo_cultivo`, `$e`, `id_usu`) VALUES ('$fecha','$id','$por','$usu')";
                    $result = $this->EjecutarAppCultivos($query);
                    if ($result) {
                        echo true;
                    }else{
                        echo false;
                    }
                }
            }
        }
    }
    public function InformacionEnfermedadesxUser($request) {
        $id_enfe = htmlentities(addslashes($request->id));
        $id_usu = htmlentities(addslashes( $request->usu ));
        $id_Cul = htmlentities(addslashes( $request->cul ));

        $query = "SELECT id_usu, id_enfe, id_cult, nombre, sigla, limite_porcentaje FROM registro_enfermedades WHERE id_usu = '$id_usu'  AND  id_cult = '$id_Cul'  AND id_enfe = '$id_enfe' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id_usu'=>$row->id_usu, 'id_enfe'=>$row->id_enfe, 'id_cult'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre), 'sigla'=>utf8_encode($row->sigla), 'limite'=>$row->limite_porcentaje);
        }
        echo json_encode($post);
    }
    public function InformacionCult_LoteEnfxUser($request) {
        $id_enfe = htmlentities(addslashes($request->id));
        $id_usu = htmlentities(addslashes( $request->usu ));
        $id_Cul = htmlentities(addslashes( $request->cul ));

        $query = "SELECT id_usu, id_enfe, id_cult, nombre, sigla, limite_porcentaje FROM registro_enfermedades WHERE id_usu = '$id_usu'  AND  id_cult = '$id_Cul'  AND id_enfe = '$id_enfe' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id_usu'=>$row->id_usu, 'id_enfe'=>$row->id_enfe, 'id_cult'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre), 'sigla'=>utf8_encode($row->sigla), 'limite'=>$row->limite_porcentaje);
        }
        echo json_encode($post);
    }

    #consultas Plagas
    public function informacionpPlagasXcultivoLote($request) {
        $usu = htmlentities(addslashes( $request->usu ));
        $cult = htmlentities(addslashes( $request->id_cult ));
        $lote = htmlentities(addslashes( $request->lote ));
        
        $var = htmlentities(addslashes( $request->var ));

        $plag_1 = "plag_1_L".$lote;
        $plag_2 = "plag_2_L".$lote;
        $plag_3 = "plag_3_L".$lote;
        $plag_4 = "plag_4_L".$lote;
        $plag_5 = "plag_5_L".$lote;

        if ($var == 1 ) {
            $fecha1 = htmlentities(addslashes( $request->fecha1 ));
            $fecha2 = htmlentities(addslashes( $request->fecha2 ));
            $query = "SELECT id_registro, fecha_r,  
            tipo_cultivos.`nombre` AS cultivo,  
            $plag_1 AS plag1,      $plag_2 AS plag2, 
            $plag_3 AS plag3,      $plag_4 AS plag4, $plag_5 AS plag5,
            control_plagas.`id_usu`,  
            usuarios.`nombre` AS nom_us
            FROM usuarios, tipo_cultivos, control_plagas 
            WHERE usuarios.`id_usu` = control_plagas.`id_usu`
            AND control_plagas.`id_usu` = '$usu'
            AND control_plagas.`tipo_cultivo` = tipo_cultivos.`id_cult`
            AND control_plagas.`tipo_cultivo` = '$cult'  
            AND `fecha_r` >= '$fecha1' AND `fecha_r` <= '$fecha2'
            ORDER BY  id_registro DESC  
            ";
        }else{
            $query = "SELECT id_registro, fecha_r,  
            tipo_cultivos.`nombre` AS cultivo,  
            $plag_1 AS plag1,      $plag_2 AS plag2, 
            $plag_3 AS plag3,      $plag_4 AS plag4, $plag_5 AS plag5,
            control_plagas.`id_usu`,  
            usuarios.`nombre` AS nom_us
            FROM usuarios, tipo_cultivos, control_plagas 
            WHERE usuarios.`id_usu` = control_plagas.`id_usu`
            AND control_plagas.`id_usu` = '$usu'
            AND control_plagas.`tipo_cultivo` = tipo_cultivos.`id_cult`
            AND control_plagas.`tipo_cultivo` = '$cult'  
            ORDER BY  id_registro DESC  
            lIMIT 0,10
            ";
        }
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
           $fe = $row->fecha_r;
            $date=substr($fe, 0, 10 );

            $e1 = 0+$row->plag1;
            if ($e1 == 9999 ) {
                $e1 = 9999;
            }else{
                if ($e1 == 0 ) {
                  $e1 = 0;
                }
            }

            $e2 = 0+$row->plag2;
            if ($e2 == 9999 ) {
                $e2 = 9999;
            }else{
                if ($e2 == 0 ) {
                    $e2 = 0;
                }
            }
            
            $e3 = 0+$row->plag3;
            if ($e3 == 9999 ) {
                $e3 = 9999;
            }else{
                if ($e3 == 0 ) {
                    $e3 = 0;
                }
            }
            $e4 = 0+$row->plag4;
            if ($e4 == 9999 ) {
                $e4 = 9999;
            }else{
                if ($e4 == 0 ) {
                    $e4 = 0;
                }
            }
            $e5 = 0+$row->plag5;
            if ($e5 == 9999 ) {
                $e5 = 9999;
            }else{
                if ($e5 == 0 ) {
                    $e5 = 0;
                }
            }


            $post[]= array( 'id'=>$row->id_registro, 'fecha'=>$date, 'cultivo'=>utf8_encode($row->cultivo), 'plag1'=>$e1, 'plag2'=>$e2, 'plag3'=>$e3, 'plag4'=>$e4, 'plag5'=>$e5, 'id_usu'=>$row->id_usu, 'usuario'=>utf8_encode($row->nom_us)       );
        }
        echo json_encode($post);
    }
    public function infoPlagCultivosX_lotes_Torta($request) {
        $id_cul = htmlentities(addslashes($request->id_cult));
        $id_usu = htmlentities(addslashes($request->usu));
        $lote = htmlentities(addslashes( $request->lote ));

        $plag_1 = "plag_1_L".$lote;
        $plag_2 = "plag_2_L".$lote;
        $plag_3 = "plag_3_L".$lote;
        $plag_4 = "plag_4_L".$lote;
        $plag_5 = "plag_5_L".$lote;

            $query ="SELECT `fecha_r`, AVG(`$plag_1`) AS plag1 FROM `control_plagas` WHERE `$plag_1` != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC lIMIT 0,10 ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $plag1 =0+$row->plag1;
            }

            $query ="SELECT `fecha_r`, AVG(`$plag_2`) AS plag2 FROM `control_plagas` WHERE  $plag_2 != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC lIMIT 0,10 ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $plag2 =0+$row->plag2;
            }

            $query ="SELECT `fecha_r`, AVG(`$plag_3`) AS plag3  FROM `control_plagas` WHERE $plag_3 != 9999  AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC lIMIT 0,10 ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $plag3 =0+$row->plag3;
            }

            $query ="SELECT `fecha_r`, AVG(`$plag_4`) AS plag4 FROM `control_plagas` WHERE `$plag_4` != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC lIMIT 0,10 ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $plag4 =0+$row->plag4;
            }

            $query ="SELECT `fecha_r`, AVG(`$plag_5`) AS plag5 FROM `control_plagas` WHERE `$plag_5` != 9999 AND `tipo_cultivo` = \"$id_cul\" AND `id_usu` = \"$id_usu\" ORDER BY `id_registro` DESC lIMIT 0,10 ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $plag5 =0+$row->plag5;
                $post[]= array('fecha'=>$row->fecha_r, 'plag1'=>$plag1, 'plag2'=>$plag2, 'plag3'=>$plag3, 'plag4'=>$plag4, 'plag5'=>$plag5);
            }
        
        echo json_encode($post);
    }
    public function insertPlagasCultivo($request) {

        $variable   = htmlentities(addslashes($request->variable));
        $id         = htmlentities(addslashes($request->id));
        $fecha      = htmlentities(addslashes($request->fecha));
        
        $usu        = htmlentities(addslashes($request->usu));
        

        if ($variable == 1 ) {
            $query = "SELECT tipo_cultivo, fecha_r, id_usu FROM control_plagas WHERE fecha_r = '$fecha'  AND tipo_cultivo = '$id'  AND id_usu = '$usu'  ";
            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $post[]= array( 'tipo_cultivo'=>$row->tipo_cultivo  );
                if ($post != 0 ) {
                    echo true;
                }else{
                    echo false;
                }
            }
        }else{
            if ($variable == 2 ) {
                $id1        =   htmlentities(addslashes($request->id1));
                $lote        = htmlentities(addslashes($request->lote));
                $planSem    =   htmlentities(addslashes($request->planSem));
                $planEnf    =   htmlentities(addslashes($request->planEnf));

                $por =( ( $planEnf / $planSem) * 100);

                $p = 'plag_'.$id1.'_L'.$lote;

                $query="UPDATE `control_plagas` SET $p ='$por' 
                    WHERE  `fecha_r` = '$fecha'     AND `tipo_cultivo` = '$id'      AND `id_usu` = '$usu' ";
                $result = $this->EjecutarAppCultivos($query);
                if ($result) {
                    echo true;
                }else{
                    echo false;
                }
            }else{
                if ($variable == 3 ) {
                    $id1        =   htmlentities(addslashes($request->id1));
                    $lote        = htmlentities(addslashes($request->lote));
                    $planSem    =   htmlentities(addslashes($request->planSem));
                    $planEnf    =   htmlentities(addslashes($request->planEnf));

                    $por =( ( $planEnf / $planSem) * 100);

                    $p = 'plag_'.$id1.'_L'.$lote;

                    $query="INSERT INTO `control_plagas`(`fecha_r`, `tipo_cultivo`, `$p`, `id_usu`) VALUES ('$fecha','$id','$por','$usu')";
                    $result = $this->EjecutarAppCultivos($query);
                    if ($result) {
                        echo true;
                    }else{
                        echo false;
                    }
                }
            }
        }
    }
    public function InformacionPlagasxUser($request) {
        $id_plag = htmlentities(addslashes($request->id));
        $id_usu = htmlentities(addslashes( $request->usu ));
        $id_Cul = htmlentities(addslashes( $request->cul ));

        $query = "SELECT id_usu, id_plag, id_cult, nombre, sigla, limite_porcentaje FROM registro_plagas WHERE id_usu = '$id_usu'  AND  id_cult = '$id_Cul'  AND id_plag = '$id_plag' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id_usu'=>$row->id_usu, 'id_plag'=>$row->id_plag, 'id_cult'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre), 'sigla'=>utf8_encode($row->sigla), 'limite'=>$row->limite_porcentaje);
        }
        echo json_encode($post);
    }
    #informacion de registro de plagas de cultivos registrados por usuario
    public function InformacionPlagsUser($request) {
        $id_cult = htmlentities(addslashes($request->id));
        $id_usu = htmlentities(addslashes( $request->usu ));

        $query = "SELECT id_usu, id_plag, id_cult, nombre, sigla, limite_porcentaje FROM registro_plagas WHERE id_usu = '$id_usu' AND id_cult = '$id_cult' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id_usu'=>$row->id_usu, 'id_plag'=>$row->id_plag, 'id_cult'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre), 'sigla'=>utf8_encode($row->sigla), 'limite'=>$row->limite_porcentaje);
        }
        echo json_encode($post);
    }
    public function UpdateDetallesPlagas($request) {
        $id_usu = htmlentities(addslashes( $request->usu ));
        $cod = htmlentities(addslashes( $request->cod ));
        $cult = htmlentities(addslashes( $request->cul ));
        $nombre = htmlentities(addslashes( $request->nombre ));
        $sigla = htmlentities(addslashes( $request->sigla ));
        $limite = htmlentities(addslashes( $request->limite ));

        $query ="UPDATE registro_plagas SET nombre = '$nombre', sigla = '$sigla', limite_porcentaje = '$limite' WHERE id_usu = '$id_usu' AND id_plag = '$cod'  AND id_cult = '$cult'  ";
        $result = $this->EjecutarAppCultivos($query);
        if ($result) {
            echo true;
        }else{
            echo false;
        }
    }
    public function InformacionPlagas($request) {
        $id_Cul = htmlentities(addslashes( $request->cul ));
        $id_plag = htmlentities(addslashes( $request->id ));

        $query = "SELECT id_plaga, limite_porcentaje, nombre,  sigla, id_cult,  descripcion,  sint_sig FROM plagas 
        WHERE id_cult = '$id_Cul' AND id_plaga = '$id_plag' ";
        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id_plaga'=>$row->id_plaga, 'limite'=>$row->limite_porcentaje, 'id_cult'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre), 'sigla'=>utf8_encode($row->sigla), 'descripcion'=>utf8_encode($row->descripcion), 'sint_sig'=>utf8_encode($row->sint_sig) );
        }
        echo json_encode($post);
    }

    public function selectInformacionCultivosXRegistrar($request) {
        $query = "SELECT * FROM tipo_cultivos ";

        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'id'=>$row->id_cult, 'nombre'=>utf8_encode($row->nombre)  );
        }
        echo json_encode($post);
    }
    public function detalleXCultivosUsuario($request) {
        $id = htmlentities(addslashes($request->id));
        $query = "SELECT tipo_cultivos.nombre, registo_cultivo.descripcion, registo_cultivo.id, registo_cultivo.lotes  
          FROM `tipo_cultivos`, registo_cultivo, usuarios 
          WHERE registo_cultivo.tipo_cultivo = tipo_cultivos.id_cult 
          AND registo_cultivo.id_usu = usuarios.id_usu 
          AND registo_cultivo.id = '$id'  ";

        $result = $this->EjecutarAppCultivos($query);
        $post = array();
        while ($row = mysqli_fetch_object($result)) {
            $post[] = array( 'id'=>$row->id, 'nombre'=>utf8_encode($row->nombre), 'descripcion'=>utf8_encode($row->descripcion), 'lotes'=>$row->lotes );
        }
        echo json_encode($post);
    }
    public function actualizarCultivoXusuario($request) {
        $id             =   htmlentities(addslashes($request->id));
        $tipo           =   htmlentities(addslashes($request->tipo));
        $descripcion    =   htmlentities(addslashes($request->descripcion));
        $lotes          =   htmlentities(addslashes($request->lotes));
        $id_usu         =   htmlentities(addslashes($request->id_usu));
        $query = "UPDATE `registo_cultivo` SET `tipo_cultivo`='$tipo', `lotes`='$lotes', `descripcion`='$descripcion' WHERE `id` = '$id' AND `id_usu` = '$id_usu'  ";

        $result = $this->EjecutarAppCultivos($query);
        if ($result) {
            echo true;
        }else{
            echo false;
        }
    }
    public function deleteCultivoXusuario($request) {
        $id = htmlentities(addslashes($request->id));
        $query = "DELETE FROM `registo_cultivo` WHERE `id` = '$id'  ";

        $result = $this->EjecutarAppCultivos($query);
        if ($result) {
            echo true;
        }else{
            echo false;
        }
    }
    public function verifRegistroCultivoUsuario($request) {
        $id_usu = htmlentities(addslashes($request->id_usu));
        $tipo = htmlentities(addslashes($request->tipo));
        $query = "SELECT tipo_cultivo, id_usu FROM  registo_cultivo WHERE tipo_cultivo = '$tipo' AND id_usu = '$id_usu' ";

        $result = $this->EjecutarAppCultivos($query);
        while ($row = mysqli_fetch_object($result)) {
            $post[]= array( 'tipo_cultivo'=>$row->tipo_cultivo  );
            if ($post != 0 ) {
                echo true;
            }else{
                echo false;
            }
        }
    }
    public function insertRegistroCultivo($request) {
        $id_usu         = htmlentities(addslashes($request->id_usu));
        $tipo           = htmlentities(addslashes($request->tipo));
        $lotes          = htmlentities(addslashes($request->lotes));
        $descripcion    = htmlentities(addslashes($request->descripcion));

        $query = "INSERT INTO `registo_cultivo`(`id`, `tipo_cultivo`, `lotes`, `descripcion`, `id_usu`) 
          VALUES (NULL ,'$tipo', '$lotes', '$descripcion','$id_usu')";

        $result = $this->EjecutarAppCultivos($query);
        if ($result) {
            echo true;
        }else{
            echo false;
        }
    }
    
    public function new_registrarUsuarios($request) {
        $variable = htmlentities(addslashes($request->variable));
        if ($variable == 1 ) {
            $correo = htmlentities(addslashes($request->correo));
            $query ="SELECT * FROM usuarios WHERE correo = '$correo'";

            $result = $this->EjecutarAppCultivos($query);
            while ($row = mysqli_fetch_object($result)) {
                $post[]= array( 'id'=>$row->id_usu  );
                if ($post != 0 ) {
                    echo true;
                }else{
                    echo false;
                }
            }            
        }else{
            if ($variable == 2 ) {
                $nombre     = htmlentities(addslashes($request->nombre));
                $telefono   = htmlentities(addslashes($request->telefono));
                $genero     = htmlentities(addslashes($request->genero));
                $correo     = htmlentities(addslashes($request->correo));
                $clave      = htmlentities(addslashes($request->clave));
                $claveMd5   = MD5($clave);

                $query1 = "INSERT INTO `usuarios` 
                (`nombre`, `telefono`, `genero`, `correo`, `clave`) 
                VALUES ('$nombre', '$telefono', '$genero', '$correo', '$claveMd5' ); ";
                $result1 = $this->EjecutarAppCultivos($query1);
                //mail($correo, 'App cultivo', 'message')
                if ($result1 ){


                    // El mensaje
                    $mensaje = "Hola ".$nombre." 1\r\Gracias por utilizar nuestro serivicio.\r\nNo olvides contactarnos.";

                    // Si cualquier línea es más larga de 70 caracteres, se debería usar wordwrap()
                    $mensaje = wordwrap($mensaje, 70, "\r\n");

                    // Enviarlo
                    mail('rojosefo132@gmail.com', 'Incicapp', $mensaje);



                    echo true;
                    $query ="SELECT * FROM usuarios WHERE correo = '$correo'";

                    /*$date = date('Y-m-d');
                    $fecha = strtotime ( '-1 day' , strtotime ( $date ) ) ;
                    $fecha = date ( 'Y-m-d' , $fecha );*/

                    
                    $fecha = date('Y-m-01');

                    $result = $this->EjecutarAppCultivos($query);
                    while ($row = mysqli_fetch_object($result)) {
                        $id= $row->id_usu  ;

                        $query1="INSERT INTO `control_enfermedades` 
                            ( `fecha_r`, `tipo_cultivo`, 
                            `enf_1_L1`, `enf_2_L1`, `enf_3_L1`, `enf_4_L1`, `enf_5_L1`, 
                            `enf_1_L2`, `enf_2_L2`, `enf_3_L2`, `enf_4_L2`, `enf_5_L2`,
                            `enf_1_L3`, `enf_2_L3`, `enf_3_L3`, `enf_4_L3`, `enf_5_L3`,
                            `enf_1_L4`, `enf_2_L4`, `enf_3_L4`, `enf_4_L4`, `enf_5_L4`,
                            `enf_1_L5`, `enf_2_L5`, `enf_3_L5`, `enf_4_L5`, `enf_5_L5`,
                            `id_usu`) 
                        VALUES  ('$fecha', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ),
                            ('$fecha', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ),
                            ('$fecha', '3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ),
                            ('$fecha', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ),
                            ('$fecha', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ) ";
                        $result = $this->EjecutarAppCultivos($query1);

                        $query2="INSERT INTO `control_plagas` 
                        (`fecha_r`, `tipo_cultivo`, 
                        `plag_1_L1`, `plag_2_L1`, `plag_3_L1`, `plag_4_L1`, `plag_5_L1`,
                        `plag_1_L2`, `plag_2_L2`, `plag_3_L2`, `plag_4_L2`, `plag_5_L2`, 
                        `plag_1_L3`, `plag_2_L3`, `plag_3_L3`, `plag_4_L3`, `plag_5_L3`, 
                        `plag_1_L4`, `plag_2_L4`, `plag_3_L4`, `plag_4_L4`, `plag_5_L4`, 
                        `plag_1_L5`, `plag_2_L5`, `plag_3_L5`, `plag_4_L5`, `plag_5_L5`, 
                        `id_usu`) 
                        VALUES  ('$fecha', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ),
                            ('$fecha', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ),
                            ('$fecha', '3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ),
                            ('$fecha', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ),
                            ('$fecha', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$id' ) ";
                        $result = $this->EjecutarAppCultivos($query2);


                        $query3 ="SELECT `id_enfe`,`id_cult`,`nombre`,`sigla`,`limite_porcentaje` FROM `enfermedades` ";
                        $result3 = $this->EjecutarAppCultivos($query3);
                        while ($row = mysqli_fetch_object($result3)) {
                            $id_enfe= $row->id_enfe;
                            $id_cult= $row->id_cult;
                            $nombre= $row->nombre;
                            $sigla= $row->sigla;
                            $limite= $row->limite_porcentaje;

                            $query4 = "INSERT INTO `registro_enfermedades`(`id_usu`, `id_enfe`, `id_cult`, `nombre`, `sigla`, `limite_porcentaje`) VALUES ('$id', '$id_enfe', '$id_cult','$nombre','$sigla','$limite')";
                            $result = $this->EjecutarAppCultivos($query4);
                        }
                        $query5 ="SELECT `id_plaga`,`id_cult`,`nombre`,`sigla`,`limite_porcentaje` FROM `plagas` ";
                        $result5 = $this->EjecutarAppCultivos($query5);
                        while ($row = mysqli_fetch_object($result5)) {
                            $id_plag= $row->id_plaga;
                            $id_cult= $row->id_cult;
                            $nombre= $row->nombre;
                            $sigla= $row->sigla;
                            $limite= $row->limite_porcentaje;

                            $query6 = "INSERT INTO `registro_plagas`(`id_usu`, `id_plag`, `id_cult`, `nombre`, `sigla`, `limite_porcentaje`) VALUES ('$id', '$id_plag', '$id_cult','$nombre','$sigla','$limite')";
                            $result = $this->EjecutarAppCultivos($query6);
                        }
                    }
                    echo true;
                }else{
                    echo false;
                }
            }
        }
    }


}