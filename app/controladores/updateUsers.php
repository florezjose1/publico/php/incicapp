<?php

//sleep(1);

require_once '../models/conexion.php';
require_once '../models/class.consultations.php';
require_once '../inc/resize.php';

session_start();
$conection  = new Connnection();
$modelo     = new Consultations();
$connect    = $conection->get_connection();

$newName        = htmlentities(addslashes($_POST['newName']));
$newTelefono    = htmlentities(addslashes($_POST['newTelefono']));
$newCorreo      = htmlentities(addslashes($_POST['newEmail']));
$passNew        = htmlentities(addslashes($_POST['newPass']));
$passVerify     = htmlentities(addslashes($_POST['newPassTwo']));
$passE          = md5($passNew);
$id_user        = htmlentities(addslashes($_POST['id_usu']));

// Registro del usuario que ingreso
$stmFirst   = $connect->prepare("SELECT * FROM usuarios WHERE id_usu = :uid");
$stmFirst->execute(array(":uid" => $_SESSION['usuarios']));
$user       = $stmFirst->fetch(PDO::FETCH_ASSOC);
// End Registro del usuario que ingreso

$query  = "SELECT correo FROM usuarios WHERE correo = :correo";
$stmTwo = $connect->prepare($query);
$stmTwo->bindParam(':correo',$newCorreo);
$stmTwo->execute();
$count  = $stmTwo->rowCount();

//print_r($_FILES);
if(isset($_POST['subir']) && $_POST['subir'] == 'Subir'){
    //print_r($_FILES);
    if(isset($_FILES['file'])){
//            echo 'seguimos';
        $carpeta    = '../../public/img/imgUsers/';
        if(is_dir($carpeta) && is_writable($carpeta)){
            $result = count($_FILES['file']['name']);
            $endExtension = array('jpg','gif','png','jpeg','JPG','GIF','PNG','JPEG');
            $msj    = '';

            for($i = 0; $i < $result; $i ++){
                $fileName = $_FILES['file']['name'][$i];
                $fileTemp = $_FILES['file']['tmp_name'][$i];
                $fileSize = $_FILES['file']['size'][$i];

                $string         = substr(md5(uniqid(rand())),0,12);
                $nameNewFile    = $string . '.jpg';
                $thumbName      = 'thumb_' . $nameNewFile;
                $extencion      = pathinfo($fileName,PATHINFO_EXTENSION);
                $altName        = basename($fileName, '.' . $extencion);
                $fileInfo       = pathinfo($fileName);

                if(!isset($_SESSION['usuarios'])){
                    header('location: ../../index.php');
                }else {
                    $imagenBinario  = addslashes(file_get_contents($fileTemp));

                    if($id_user === $user['id_usu']){// Si el id del usuario es el mismo que se recoje en el formulario pasamos el primer bloque de seguridad
                        if(strlen($newCorreo) > 0 && strlen($newName) > 0 && strlen($id_user) > 0 && strlen($passNew) > 0 && strlen($imagenBinario) > 0){//Si los campos del formulario no estan vacios pasamos el segundo bloque de seguridad
                            if($count == 0 || $user['correo'] == $newCorreo){// Si el usuario es diferente a los que existen en la DB pasamos tercer bloque de seguridad
                                if($passNew == $passVerify){// Si las contraseñas son iguales parar el cuarto bloque de seguridad
                                    if($fileSize < 1300000){
                                        if(in_array($fileInfo['extension'],$endExtension)){// Si la extencion de la img son JPG, GIF & PNG pasar quinto bloque de seguridad
                                        //
                                        /**
                                         * Creando thumnails, y redireccionando las imagenes originales
                                         **/
                                            copy($fileTemp, $carpeta . $fileName);
                                            $thumb = new thumbnail($carpeta . $fileName);
                                            $thumb->size_width(400);
                                            $thumb->size_height(300);
                                            $thumb->jpeg_quality(100);
                                            $thumb->save($carpeta . $thumbName);
                                            //Dimension imagen original
                                            $thumb = new thumbnail($carpeta . $fileName);
                                            $thumb->size_width(400);
                                            $thumb->size_height(300);
                                            $thumb->jpeg_quality(100);
                                            $thumb->save($carpeta . $nameNewFile);
                                            unlink($carpeta . $nameNewFile);

                                            $stm = $modelo->updateUsers('nombre', $newName,$id_user);
                                            $stm = $modelo->updateUsers('telefono', $newTelefono,$id_user);
                                            $stm = $modelo->updateUsers('correo', $newCorreo,$id_user);
                                            $stm = $modelo->updateUsers('clave', $passE,$id_user);
                                            $stm = $modelo->updateUsers('name_photo', $nameNewFile,$id_user);
                                            $stm = $modelo->updateUsers('alt_photo', $altName,$id_user);

                                            // UPDATE DE LA FOTO DEL USUARIO
                                            $foto   = $connect->prepare("UPDATE usuarios set photo = '$imagenBinario' WHERE id_usu = '$id_user'");
                                            $result = $foto->execute();
                                            // FIN UPDATE
                                            //echo $result;

                                            //mail($newCorreo,'Cambios realizados','Has solicitado cambios en tu usuario: <br>'.'Nuevo usuario: '.$newCorreo.'<br>'.'Nueva clave: '.$passNew);
                                            echo '6';
                                        }else{
                                            echo '4';
                                        }
                                    }else{
                                        echo '8';
                                    }
                                }else{
                                    echo '5';
                                }
                            }else{
                                echo '1';
                            }
                        }else{
                            echo '7';
                        }
                    }else{
                        header('location: ../perfil.php');
                    }
                }

            }
        }
    }else{
//        echo 'imagen vacia';
//        echo '2';
        if(!isset($_FILES['file'])){
            if($id_user === $user['id_usu']){
                if(strlen($newCorreo) > 0 && strlen($newTelefono) > 0 && strlen($newName) > 0 && strlen($id_user) > 0 && strlen($passNew) > 0){
                    if($count == 0 || $user['correo'] == $newCorreo){
                        if($passNew == $passVerify){
                            $stm = $modelo->updateUsers('nombre', $newName,$id_user);
                            $stm = $modelo->updateUsers('telefono', $newTelefono,$id_user);
                            $stm = $modelo->updateUsers('correo', $newCorreo,$id_user);
                            $stm = $modelo->updateUsers('clave', $passE,$id_user);

                            //mail($newCorreo,'Cambios realizados','Has solicitado cambios en tu usuario: <br>'.'Nuevo usuario: '.$newCorreo.'<br>'.'Nueva clave: '.$passNew);
                            echo '6';
                            //var_dump($_FILES['file']);
                        }else{
                            echo '5';
                        }
                    }else{
                        echo '1';
                    }
                }else{
                    echo '7';
                }
            }else{
                header('location: ../perfil.php');
            }
        }
    }

}else{
    header('location: ../perfil.php');
} // Fin de validacion