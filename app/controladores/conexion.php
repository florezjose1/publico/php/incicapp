<?php


require_once 'vars.php';
class Conexion extends Variables{


    public function ConectarAppCultivos(){
        $connect = mysqli_connect($this->host,$this->user,$this->pass,$this->app)
            or die ("Error". mysqli_error($connect));
        return $connect;   
    }
 
    public function EjecutarAppCultivos($sql){
        $connect = $this->ConectarAppCultivos();
        $result = $connect->query($sql);
        return $result;
    }
    
   
}

?>