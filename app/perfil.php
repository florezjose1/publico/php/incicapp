<?php

session_start();
include 'models/conexion.php';
$connection = new Connnection();
$connect    = $connection->get_connection();
if(isset($_SESSION['usuarios'])) {
    ?>

    <?php
    require_once 'controladores/principalControllers.php';
    require_once 'controladores/vars.php';
    // recoger el registro competo, del usuario que ingreso
    $query = $connect->prepare("SELECT * FROM usuarios WHERE id_usu = :idu");
    $query->execute(array(":idu"=>$_SESSION['usuarios']));
    $user = $query->fetch(PDO::FETCH_ASSOC);
    ?>
    <!DOCTYPE html>
    <html lang="es">
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

        <?php include 'inc/enlaces.php';?>

    </head>
    <body class="backUsuarios">
<?php# include 'inc/analyticstracking.php';?>


    <div class="container-fluid conImgFondo">

        <div class="container ">

            <form action="controladores/updateUsers.php" id="updatePerfil" method="post" enctype="multipart/form-data">
                <div class="card testimonial-card" >
                    <div class="card-up default-color-dark">
                        <a href="usuario.php" style="float: left; color: #fff;  margin-left: 20px; font-size: 30px; margin-top: 10px;"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                        <center><h1 style="color: #eee; margin-bottom: 35px;">Perfil de Usuario</h1></center>
                    </div>
                        <div class="avatar">
                            <ul class="thumb">
                                <li>
                                    <input type="file" id="archivo1" name="file[]">
                                    <?php
                                    if(null == $user['photo']){
                                        if($user['genero'] == 0){
                                            echo '<div id="photo-1" class="rounded-circle img-responsive linkH" ></div>';
                                        }else{
                                            echo '<div id="photo-1" class="rounded-circle img-responsive linkM" ></div>';
                                        }
                                    }else{
                                        echo '
                                        <div id="photo-1" >
                                            <img class="rounded-circle img-responsive viewPhoto" id="thumb-1" src="data:image/*;base64,'.base64_encode($user['photo']).'">
                                        </div>';
                                        #echo '<div id="photo-1" class="link"><img class="link" src="../public/img/imgUsers/thumb_'.$user['name_photo'].'"></div>';
                                    }
                                    ?>
                                    <div id="cerrar-photo-1" class="cerrar-photo cerrar-photo-p"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="card-block">
                            <h4 class="card-title"><?php echo ucwords($user['nombre'])?></h4>
                            <hr>
                            <input  type="hidden" name="id_usu" id="id_usu" value="<?php if(isset($user['id_usu'])){echo $user['id_usu'];}?>">
                            <h6>Nombre</h6>
                            <input class="form-user-perfil" style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; "  type="text" name="newName" id="newName" value="<?php echo $user['nombre']?>">
                            <h6>Telefono</h6>
                            <input class="form-user-perfil" style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; "  type="text" name="newTelefono" id="newTelefono" value="<?php echo $user['telefono']?>">
                            <h6>Email</h6>
                            <input class="form-user-perfil" style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; "  type="text" name="newEmail" id="newEmail" value="<?php echo $user['correo']?>">
                            <h6>Contraseña</h6>
                            <input class="form-user-perfil" style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; "  type="password" name="newPass" id="newPass">
                            <h6>Repetir contrasña</h6>
                            <input class="form-user-perfil" style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; "  type="password" name="newPassTwo" id="newPassTwo">
                            <p>* Para poder actualizar debes colocar la contraseña o introducir una nueva ! </p>
                            <center>
                                <div class="boton">
                                    <input type="hidden" id="subir" name="subir" value="Subir">
                                    <input type="submit" id="uploadbtn" class="uploadbtn btn btn-primary btn-sm" value="Actualizar">
                                </div>
                                <br><br>
                                <div class="msj"></div>
                                <div id="resultado"></div>
                            </center>
                            <br>
                            <!--BARRA DE PROGRESO-->
                            <div class="progress">
                                <div class="bar"></div >
                                <div class="percent">0%</div>
                            </div>
                            <!--FIN BARRA DE PROGRESO-->
                        </div>
                    </div>
                </form>
            </div>


            <div class="volverInicio" id="irarriba"><a href="#">Volver al Inicio</a></div>

        </div>
    </div>


    </body>
    </html>


    <?php
}else{
    echo '<script> window.location="../index.php"; </script>';
}
?>















