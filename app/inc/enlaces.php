    <meta charset="utf-8">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="shortcut icon" href="../public/img/logosApp.ico" type="image/x-icon" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>IncicAp</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href='../public/css/compiled.css' rel='stylesheet' id='compiled.css-css'   type='text/css' media='all' />
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
    <!-- Material Design Bootstrap -->
    <link href="../public/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="../public/css/style.css" rel="stylesheet">
    <!-- estilos css de alertas -->
    <link rel="stylesheet" href="../public/css/jquery.toast.css">
    <!--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">-->
    <!--Perfil estilos-->
    <link rel="stylesheet" href="../public/css/perfil.css">




    <!-- JQuery -->
    <script src="../public/js/jquery-1.9.1.min.js "></script>
    <script type="text/javascript" src="../public/js/jquery.toast.js"></script>

    <script src="../public/js/highcharts.js"></script>
    <script src="../public/js/exporting.js"></script>
    <script src="../public/js/data.js"></script>
    <script src="../public/js/drilldown.js"></script>

    <script src="../public/js/functionss.js"></script>
    <script src="../public/js/enfermedadess.js"></script>
    <script src="../public/js/plagass.js"></script>
    <script src="../public/js/cultivoss.js"></script>
    <!---<script src="../public/js/turs.js"></script>-->

    
    <!--<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>-->
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="../public/js/tether.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="../public/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="../public/js/mdb.min.js"></script>
    <script type="text/javascript" src="../public/js/jquery.form.js"></script>
    <script type="text/javascript" src="../public/js/updateUsers.js"></script>
