<!--Navbar-->
<div class="header-principal" >
    <nav class="navbars-header">
        <div class="container">
            <div class="row">
                <div  class="conHeader col-xs-12 col-sm-12 col-md-12 col-lg-8" style="padding: 5px;">
                    <img style="float: left;" src="../public/img/logosApp.png" alt="" height="70px">
                    <!--<h1><a href="usuario.php">App Cultivos</a></h1>-->
                    <h3 style="color: #212121; margin-top: 15px; margin-left: 120px;">
                    <a href="" class="titleApp">Incidencias de Cultivos Agrícolas</a></h3>
                    <a href="" class="titleApp1">IncicApp</a></h3>
                    <ul>
                        <li style="display: inline;">
                            <span onclick="deplegueMenu2()" id="botonMenuUsuarios" class="deplegueMenu" style="color:#a4a0a0;">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        </li>
                    </ul>        
                </div>
                <div class="conOptions col-xs-12 col-sm-12 col-md-12 col-lg-4" style="padding: 15px;">
                    <div class="options" style="">
                        
                        <a href="perfil.php" style=" margin-right: 20px; " >
                            <?php 
                                $name = $user['nombre'];
                                $porciones = explode(" ", $name);
                                echo ucwords($porciones[0]);
                            ?>
                        </a>
                        <a href="logout.php" >Cerrar Sesion</a>
                    </div>
                </div>        
            </div>
        </div>
    </nav>
    <div class="container-fluid container-botones" >
        <div class="container botones-optiones" style="background-color:#15a089;z-index:1;">
            <a id="iniciobtn" href="usuario.php">Inicio</a>
            <a id="enfermBtn" >Enfermedades</a>
            <a id="plagasBoton">Plagas</a>
            <a id="cultivosBoton">Cultivos</a>
        </div>
    </div>    
</div>