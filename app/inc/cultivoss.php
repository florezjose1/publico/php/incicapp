<div id="infoCultivos">
    <div class="col-xs-12 separador "  ></div>
    <div class="col-xs-12"><hr></div>
    <div id="detallesCultivo" class="container" style="height: 580px; margin-bottom: 180px;">
        <center>
            <div class="col-md-3"></div>
            <div class="col-xs-12 col-md-6">
                <h3><b>Detalles Cultivos</b></h3>
            </div>
            <div class="col-xs-12 col-md-3 regCultivo">
                <button class="btn btn-botonesExp btn-md" onclick="abrirModalRegistro()" >Registrar</button>
            </div>
            <div id="cultivos">
                <table class="table table-bordered table_cultivos" style="width: 96%; margin:auto;overflow-x:auto;">
                    <thead class="tableHeadDetalles ">
                    <tr>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th colspan="2">Opciones</th>
                    </tr>
                    </thead>
                    <tbody id="cuerpoCultivos" class="tablecuerpoDetalles"></tbody>
                </table>
            </div>
        </center>
    </div>

    <!-- Modal registro de cultivos -->
    <button id="modalRegistroCultivo" data-toggle="modal" data-target="#myModal" style="display: none" ></button>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <center><h4 class="modal-title w-100" id="myModalLabel">Registar Cultivo</h4></center>
                </div>
                <div class="modal-body">
                    <form action="" method="post">
                        <center>

                        	<div class="col-xs-12 col-md-6">
                                <h6>Selecciona Cultivo</h6>
                                <select class="tipo_cul" id="tipo_cultivo"  >
                                </select>                                
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <h6># Lotes</h6>
                                <input type="number" class="tipo_cul" id="cant_lotes" style="background: transparent;border: 1px solid #2aabd2;border-radius: 5px;margin-bottom: 10px;padding-left: 15px;width: 90%;height: 20px;">
                            </div>
                            <br><br>

                            <h6>Descripcion</h6><br>
                            <textarea id="desRegisCultivo" class="md-textarea descRegCult" length="300" style="width: 90%; border: 1px solid #e8e3e3; height: 20px;"></textarea>
                            <label for="desRegisCultivo">Max(300 caracteres) </label>
                            <input style="display: none; " type="reset" id="resert_RegistroCultivo">
                        </center>
                    </form>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-primary btn-sm" onclick="inserRegisCultivo()">Registrar</button>
                        <button type="button" id="cerrarModalRegistroCultivos" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                    </center>
                </div>
            </div>
        </div>
    </div>


    <!-- Button detales de actualizar cultivos -->
    <button id="detalle_cultivo" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalUpdate" style="display: none;">
    </button>
    <div class="modal fade" id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <center><h4 class="modal-title w-100" id="myModalLabel">Actualizar Cultivo</h4></center>
                </div>
                <div class="modal-body">
                    <form action="" method="post">
                        <center>
                        	<div class="col-xs-12 col-md-6">
                                <h6>Tipo Cultivo</h6>
                                <select class="tipo_cul" id="detalle_tipo_cul" disabled >
                                    <option id="fresaD" value="1">Fresa</option>
	                                <option id="duraznoD" value="2">Durazno</option>
	                                <option id="papaD" value="3">Papa</option>
                                </select>                                
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <h6># Lotes</h6>
                                <input type="number" class="tipo_cul" id="detalle_cant_lotes" style="background: transparent;border: 1px solid #2aabd2;border-radius: 5px;margin-bottom: 10px;padding-left: 15px;width: 90%;height: 20px;">
                            </div>
                            <br><br>
                            <br><br>

                            <hr>
                            <h6>Descripcion</h6><br>
                            <textarea id="detalle_desRegisCultivo" class="md-textarea descRegCult" length="300" style="width: 90%; border: 1px solid #e8e3e3; height: 20px;"></textarea>
                            <label for="detalle_desRegisCultivo">Max(300 caracteres) </label>
                            <input style="display: none; " type="reset" id="resertInserCultivo">
                        </center>
                    </form>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".actualizarUsuario" >Actualizar</button>
                        <button id="cerrarModalUpdateCultivos" type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade actualizarUsuario" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="margin-top: 0px;  ">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div class="modal-body" style="margin: auto; height: 480px; padding-top: 150px;  ">
                    <center><h3 > Estas seguro de actualizar registro ?</h3></center>
                    <br><br>
                    <center>
                        <button type="button" class="btn btn-primary"  onclick="actualizarCultivoXusuario()" >Si</button>
                        <button type="button" class="btn btn-danger" id="noActcultivo" data-dismiss="modal">No</button>
                    </center>
                </div>
            </div>
        </div>
    </div>





    <!-- moda de delete cultivos -->
    <button id="ModalDeleteCultivo" class="btn btn-primary" data-toggle="modal" data-target=".deleteCultivo" style="display: none"></button>
    <div class="modal fade deleteCultivo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="margin-top: 0px;  ">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body" style="margin: auto; height: 300px; padding-top: 40px;  ">
                    <input type="text" id="id_cultivo" style="display: none; ">
                    <center><h3 > Estas seguro de Eliminar Cultivo de <span id="cultivoDetalle"></span> ?</h3></center>
                    <br><br>
                    <center>
                        <button type="button" class="btn btn-primary"  onclick="deleteCultivoUsuario()" >Si</button>
                        <button type="button" class="btn btn-danger" id="noDeleteCultivo" data-dismiss="modal">No</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
    