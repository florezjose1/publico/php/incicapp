<div id="val-infoUsuariosEnf">
<div class="col-xs-12 separador" ></div>
<div class="col-xs-12"><hr></div>
	<div id="graficasEnfermedades" class="container">
		<div class="detallesLineales" id="graficalineal">
			<center>
				<h2 id="titelEnferdades">Control de Enfermedades <span id="nom_enf"></span>
				<span class="btnBuscadorDatosC_g" onclick="mostrarMenuBuscar()" ><i class="fa fa-search" aria-hidden="true"></i></span>

				</h2>
				<p id="numeroLoteEnfermedad"></p>
				
				<div class="contentBtnBuscar" >
					<input class="input_optbusqueda" style="" type="date" id="fechaInEnf" value="<?php echo date('Y-m-01');?>">
					<input class="input_optbusqueda" style="" type="date" id="fechaFiEnf" value="<?php echo date('Y-m-d');?>">
					<select class="input_optbusqueda" id="tip_CultivoBuscar" onchange="buscarLotes()" >
			        </select>
			        <select class="input_optbusqueda" id="lotes_CultivoBuscar" style="display: inline-block;">
			        </select>
			        <button class="btn botonesExp"  onclick="busquedaEnf_cult_Xfecha()">Buscar</button>

				</div>
				<hr>
			</center>
			<div class="col-xs-12 col-md-3 btngraficas">
				<ul class="menu menu-enfermedades">
		            <li>
		                <i id="sigEnfCultivos" class="icoDesp" style="float: left;margin-right: 10px; margin-top: 10px; margin-left: 10px;">+</i>
		                <a onclick="mostrarCultivosUsuario()">Cultivos
		                </a>
		                <div class="sub-menuCultivos" id="sub-cultivos_enf" style="display: none;"></div>
		            </li>
		            <li>
		                <a onclick="abrirModalRegistroEnfermedades()" style="margin-left: 10px;">Registrar Incidencia</a>
		            </li>
		            <li>
		                <i id="desplegar1_Enf" class="icoDesp" style="float: left;margin-right: 10px; margin-top: 10px; margin-left: 10px;">+</i>
		                <a onclick="detalleEnfermedadesUsuario()">Detalles Enfermedades 
		                </a>
		  		        <div class="sub-menuCultivos" id="sub_detalles" style="display: none;"></div>
		            </li>
		        </ul>
			</div>
			<div class="col-xs-12 col-md-9">
		        <div id="optGrafica" style="display: block;  "></div>

		        <div id="detaEnfermedaes" style="display: none">
		            <h4 style="text-align: center; ">Lista de Enfermedades <span id="nomEnfDetalles"></span></h4>
		            <hr>
		            <table class="table table-bordered">
		                <thead class="tableHeadDetalles">
		 	                <tr>
		 	                    <th>Nombre</th>
		                        <th>Sigla</th>
		                        <th>Limite Porcentaje ( % )</th>
		                        <th colspan="2">Opciones</th>
		                    </tr>
		                </thead>
		                <tbody id="cuerpoEnfermedad" class="tablecuerpoDetalles"></tbody>
		            </table>
		        </div>
		    </div>
		</div>
	</div>
</div>


		<!-- Modal registro enfermedades de cultivos -->
		<button id="regisControl" data-toggle="modal" data-target="#registroContEnf" style="display: none;"></button>
		<div class="modal fade" id="registroContEnf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                </button>
		                <center><h4 class="modal-title w-100" id="myModalLabel">Registar Incidencia</h4></center>
		            </div>
		            <div class="modal-body">
		            <form action="" method="post">
		                <center>
		                <div class="row">
		                    <div class="col-xs-12 col-md-6">
		                        <h6>Seleccionar Cultivo</h6>
		                        <select class="tipo_cul" id="tip_Cultivo" onchange="selectEnfermedad()" >
		                        </select>
		                    </div>		
		                    <div class="col-xs-12 col-md-6">
		                        <h6>Seleccione Lote</h6>
		                        <select class="tipo_cul" id="tip_lote" style="display: inline-block;" >
		                        	
		                        </select>
		                    </div>	
		                    <div class="col-xs-12 col-md-6">
		                        <h6>Seleccionar Enfermedad</h6>
		                        <select class="tipo_cul" id="tip_enfermedad"  >
		                        </select>
		                    </div>
		                    <div class="col-xs-12 col-md-6">
		                         <h6># plantas sembradas </h6>
		                            <input class="detalleInput" type="number" id="plantasSem" style="background: transparent;border: 1px solid #2aabd2;border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 90%;padding: 1px;" >
		                       
		                    </div>
		                    <div class="col-xs-12 col-md-6">
		                         <h6># plantas Enfermas </h6>
		                            <input class="detalleInput tipo_cul" type="number" id="plantasEnf" style="background: transparent;border: 1px solid #2aabd2;border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 90%;padding: 1px;" >
		                    </div>
		                    <div class="col-xs-12 col-md-6">
		                    	<h6>Fecha </h6>
		                    	<input class="detalleInput" type="date" id="fechaR" value="<?php echo date('Y-m-d');?>" style="background: transparent;border: 1px solid #2aabd2;border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 90%;padding: 1px;" >
		                    </div>

		                    <input style="display: none; " type="reset" id="resertInserCultivoXlotes">
		                </div>
		                    
		                </center>
		            </form>
		            </div>
		            <div class="modal-footer">
		                <center>
		                    <button type="button" class="btn  enfRegistro btn-primary btn-sm" onclick="insertRegPlanEnfermas()">Registrar</button>
		                    <button type="button" class="btn  enfRegistro btn-danger btn-sm" id="cerrarRegCultivos" data-dismiss="modal">Cerrar</button>
		                </center>
		            </div>
		        </div>
		    </div>
		</div>

	<!-- Button trigger modal -->
	<button style="display: none;" id="updateInformacionEnfermedades" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#UpdateinfoDetallesEnf">
	</button>
	<!-- Modal -->
	<div class="modal fade" id="UpdateinfoDetallesEnf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	        <!--Content-->
	        <div class="modal-content">
	            <!--Header-->
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	                <center><h4 class="modal-title w-100" >
	                	Actualizar Detalles de Enfermedad
	                </h4></center>
	            </div>
	            <!--Body-->
	            <div class="modal-body" id="contenidoDetalles">
	            <center>
	            		<h5>Nombre</h5>
	            		<input type="hidden" id="cod_enf" >
	            		<input type="hidden" id="cod_cult" >
	            		<input type="hidden" id="nom_cult" >
	            		<input class="input" style="border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 50%; " type="text" id="nomEnfUpdate" value="">

	            		<h5>Sigla</h5>
	            		<input class="input" style="border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 50%; " type="text" id="sigEnfUpdate" value="">

	            		<h5>LImite %</h5>
	            		<input class="input" style="border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 50%; " type="text" id="limEnfUpdate" value="">

	            		<div class="text-center">
                        <center><button class="btn btn-primary btn-sm" onclick="UpdateDetallesEnfermedad()">Actualizar</button></center>
                    </div>
	            </center>
	            </div>
	            <!--Footer-->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default " id="cerrarUpdateEnf" data-dismiss="modal">Cerrar</button>
	            </div>
	        </div>
	        <!--/.Content-->
	    </div>
	</div>
	<!-- Button trigger modal -->
	<button style="display: none;" id="detallesInformacion" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#infoDetalles">
	</button>
	<!-- Modal -->
	<div class="modal fade" id="infoDetalles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	        <!--Content-->
	        <div class="modal-content">
	            <!--Header-->
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	                <center><h4 class="modal-title w-100" id="nameDetalleEnf" ></h4></center>
	            </div>
	            <!--Body-->
	            <div class="modal-body" id="contenidoDetalles">
	            	<center><h6><b></b></h6></center>
	            	<p id="descripcionEnfermedad" class="type-text-detalles"></p>
	            	<center><h6><b>Sintomas y Signos</b></h6></center>
	            	<p id="sinSigEnfermedad" class="type-text-detalles"></p>
	            </div>
	            <!--Footer-->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default " data-dismiss="modal">Cerrar</button>
	            </div>
	        </div>
	        <!--/.Content-->
	    </div>
	</div>