<div id="val-infoUsuariosPlag">
	<div class="col-xs-12 separador" ></div>
	
	<div class="col-xs-12"><hr></div>
		<div id="graficasPlagas" class="container">
	    	<div class="detallesLineales" id="graficalineal">
	    			<center>
						<h2 id="titelPlagas">Control de Plagas <span id="nom_plag"></span>
						
						<span class="btnBuscadorDatosC_g" onclick="mostrarMenuBuscarPlag()" ><i class="fa fa-search" aria-hidden="true"></i></span>

						</h2>
						<p id="numeroLotePlagas"></p>
							
						<div class="contentBtnBuscarPlag" >
							<input class="input_optbusqueda" style="" type="date" id="fechaInPlag" value="<?php echo date('Y-m-01');?>">
							<input class="input_optbusqueda" style="" type="date" id="fechaFiPlag" value="<?php echo date('Y-m-d');?>">
							<select class="input_optbusqueda" id="tip_CultivoBuscarPlag" onchange="buscarLotesPlag()" >
					        </select>
					        <select class="input_optbusqueda" id="lotes_CultivoBuscarPlag" style="display: inline-block;">
					        </select>
					        <button class="btn botonesExp"  onclick="busquedaPlag_cult_Xfecha()">Buscar</button>
		
						</div>
						<hr>
					</center>
	        	<div class="col-xs-12 col-md-3 btngraficas">
	            	<ul class=" menu menu-enfermedades">
	                	<li>
	                    	<i id="sigPlagCultivos" style="float: left;margin-right: 10px; margin-top: 10px; margin-left: 10px;">+</i><a onclick="mostrarPlagCultivosUsuario()">Cultivos
	                        	
		                    </a>
		                    <div class="sub-menuCultivos" id="sub_plag" style="display: none;"></div>
		                </li>
		                <li>
		                    <a onclick="abrirModalRegistroPlagas()" style="margin-left: 10px;">Registrar Incidencia</a>
		                </li>
		                <li>
		                	<i id="desplegar2plag" class="icoDesp" style="float: left;margin-right: 10px; margin-top: 10px; margin-left: 10px;">+</i>
		                    <a onclick="detallePlagasUsuario()">Detalle de Plagas 
		                    	
		                    </a>
		                    <div class="sub-menuPlagas" id="sub_detallesPlagas" style="display: none;">
		                    
		                    </div>
		                </li>
	            	</ul>
	        	</div>
	        	<div class="col-xs-12 col-md-9">
			    	
	           		<div id="optGraficaPlaga" style="height:350px;"></div>

	            	<div id="detaPlagasUSu" style="display: none">
	                	<h4 style="text-align: center; ">Lista de Plagas <span id="nomPlaDetalles"></span></h4>
	                	<table class="table table-bordered">
	                    	<thead class="tableHeadDetalles">
			                    <tr>
			                        <th>Nombre</th>
			                        <th>Sigla</th>
			                        <th>Limite Porcentaje ( % )</th>
		                        	<th colspan="2">Opciones</th>
			                    </tr>
	                    	</thead>
	                    	<tbody id="cuerpoPlagUsuario" class="tablecuerpoDetalles"></tbody>
	                	</table>
	            	</div>
	        	</div>
	    	</div>
		</div>
		<!-- Modal registro Plagas de cultivos -->
		<button id="regisControlPlag" data-toggle="modal" data-target="#registroControlPlag" style="display: none;"></button>
		<div class="modal fade" id="registroControlPlag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                </button>
		                <center><h4 class="modal-title w-100" id="myModalLabel">Registar Control</h4></center>
		            </div>
		            <div class="modal-body">
		            <form action="" method="post">
		            	<center>
		                <div class="row">
		                    <div class="col-xs-12 col-md-6">
		                        <h6>Seleccionar Cultivo</h6>
		                        <select class="tipo_cul" id="tip_CultivoPlag" onchange="selectPlagasRegistro()" >
		                        </select>
		                    </div>
		                    <div class="col-xs-12 col-md-6">
		                        <h6>Seleccione Lote</h6>
		                        <select class="tipo_cul" id="tip_lotePlag" style="display: inline-block;" >
		                        </select>
		                    </div>
		                    <div class="col-xs-12 col-md-6">
		                        <h6>Seleccione Plaga</h6>
		                        <select class="tipo_cul" id="tip_Plag"  >
		                        </select>
		                    </div>
		                    <div class="col-xs-12 col-md-6">
		                         <h6># plantas sembradas </h6>
		                            <input class="detalleInput" type="number" id="plantasSemPlag" style="background: transparent;border: 1px solid #2aabd2;border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 90%;padding: 1px;" >
		                       
		                    </div>
		                    <div class="col-xs-12 col-md-6">
		                         <h6># plantas Enfermas </h6>
		                            <input class="detalleInput" type="number" id="plantasPlag" style="background: transparent;border: 1px solid #2aabd2;border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 90%;padding: 1px;" >
		                       
		                    </div>
		                    <div class="col-xs-12 col-md-6">
		                    	<h6>Fecha </h6>
		                    	<input class="detalleInputPlag" type="date" id="fechaRPlag" style="background: transparent;border: 1px solid #2aabd2;border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 90%;padding: 1px;" value="<?php echo date('Y-m-d');?>" >
		                    </div>
		                </div>
		                <input style="display: none; " type="reset" id="resertInserCultivoPlag">
		            </form>
		            </div>
		            <div class="modal-footer">
		                <center>
		                    <button type="button" class="btn btn-primary btn-sm" onclick="insertRePlanPlagas()">Registrar</button>
		                    <button id="cerrarRegCultivosPlag" type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>
		                </center>
		            </div>
		        </div>
		    </div>
		</div>


	<!-- Button trigger modal -->
	<button style="display: none;" id="detallesInformacionPlagas" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#infoDetallesPlagas">
	</button>

	<!-- Modal -->
	<div class="modal fade" id="infoDetallesPlagas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	        <!--Content-->
	        <div class="modal-content">
	            <!--Header-->
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	                <center><h4 class="modal-title w-100" id="nameDetallePlag" ></h4></center>
	            </div>
	            <!--Body-->
	            <div class="modal-body" id="contenidoDetalles">
	            	<center><h6><b>Descripcion</b></h6></center>
	            	<p id="descripcionPlaga" class="type-text-detalles"></p>
	            	<center><h6><b>Sintomas y Signos</b></h6></center>
	            	<p id="sinSigPlaga" class="type-text-detalles"></p>
	            </div>
	            <!--Footer-->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default " data-dismiss="modal">Cerrar</button>
	            </div>
	        </div>
	        <!--/.Content-->
	    </div>
	</div>

	<!-- Button trigger modal -->
	<button style="display: none;" id="updateInformacionPlagas" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#UpdateinfoDetallesPlag">
	</button>
	<!-- Modal -->
	<div class="modal fade" id="UpdateinfoDetallesPlag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	        <!--Content-->
	        <div class="modal-content">
	            <!--Header-->
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	                <center><h4 class="modal-title w-100" >
	                	Actualizar Detalles de Plagas
	                </h4></center>
	            </div>
	            <!--Body-->
	            <div class="modal-body" id="contenidoDetalles">
	            <center>
	            		<h5>Nombre</h5>
	            		<input type="hidden" id="cod_plag" >
	            		<input type="hidden" id="cod_cultPlag" >
	            		<input type="hidden" id="nom_cultplag" >
	            		<input class="input" style="border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 50%; " type="text" id="nomPlagUpdate" value="">

	            		<h5>Sigla</h5>
	            		<input class="input" style="border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 50%; " type="text" id="sigPlagUpdate" value="">

	            		<h5>LImite %</h5>
	            		<input class="input" style="border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px;width: 50%; " type="text" id="limPlagUpdate" value="">

	            		<div class="text-center">
                        <center><button class="btn btn-primary btn-sm" onclick="UpdateDetallesPlagas()">Actualizar</button></center>
                    </div>
	            </center>
	            </div>
	            <!--Footer-->
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default " id="cerrarUpdatePlag" data-dismiss="modal">Cerrar</button>
	            </div>
	        </div>
	        <!--/.Content-->
	    </div>
	</div>





</div>