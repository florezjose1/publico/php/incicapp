<?php
    session_start();
    include 'app/models/conexion.php';
    if(isset($_SESSION['usuarios'])){
        echo '<script> window.location="app/usuario.php"; </script>';
    }   
?>

<?php 
    require_once 'app/controladores/principalControllers.php';
    require_once 'app/controladores/vars.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="public/img/logosApp.ico" type="image/x-icon" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>IncicAp  </title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href='public/css/compiled.css' rel='stylesheet' id='compiled.css-css'   type='text/css' media='all' />
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
    <!-- Material Design Bootstrap -->
    <link href="public/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="public/css/style.css" rel="stylesheet">
    <!-- estilos css de alertas -->
    <link rel="stylesheet" href="public/css/jquery.toast.css">

</head>

<body class="backUsuarios">
<?php #include 'app/inc/analyticstracking.php';?>

<!-- deplegar menu -->
<input type="hidden" id="deplegarMenu1" value="0">

<!--Navbar-->
<nav class="navbar" style="color: #fff; padding: 15px; background-color: rgba(0, 105, 92, 0.24);">
    <div class="container">
        <div class="row">
            <div  class="conHeader col-xs-12 col-sm-12 col-md-12 col-lg-8" style="padding: 5px;">
                    <img style="float: left;" src="public/img/logosApp.png" alt="" height="70px">
                    <!--<h1><a href="usuario.php">App Cultivos</a></h1>-->
                    <h3 style="color: #ffffff; margin-top: 15px; margin-left: 120px;">

                        <a href="" class="title1HeadIndex" style="color: #fff;">Incidencias de Cultivos Agrícolas</a>
                        <a href="" class="title2HeadIndex" style="color: #fff;">IncicApp</a></h3>

                    </h3>
                    <ul>
                        <li style="display: inline;">
                            <span onclick="deplegueMenu1()" id="botonMenuUsuarios" class="deplegueMenu">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        </li>
                    </ul>        
                </div>
                <div class="conOptions col-xs-12 col-sm-12 col-md-12 col-lg-4" style="padding: 15px; margin-bottom: 20px;">
                <div class="options" >
                    <a id="btnIniciarSesion" href="" style="color: #eeeeee; margin-right: 20px; " data-toggle="modal" data-target="#modal-login">Iniciar sesion </a>
                </div>
            </div>        
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="container container-registro">
        <center style="margin: auto;">
            <h3 class="modal-title"  id="myModalLabel"><b>Registrate</b></h3>
        </center>
        <form action="" method="post">
        <center>
            <p>Nombre</p>
            <input style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; background: #EEE;height: 10px;" type="text" required  id="nombreRu" class="form-control">
            
            <p>Telefono</p>
            <input  style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; background: #EEE;height: 10px;" type="number" required  id="telefonoRU" class="form-control">
            
            <center><h6>Genero</h6>
                <fieldset class="form-group">
                    <input name="genero" type="radio" id="masculino" value="0" checked="checked">
                    <label for="masculino">Masculino</label>
                          
                    <input name="genero" type="radio" value="1" id="femenino">
                    <label for="femenino">Femenino</label>
                </fieldset>
            </center>
            <p>Correo</p>
            <input  style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; background: #EEE;height: 10px;" type="email" required  id="correoRU" class="form-control">
                        
            <p>Contraseña</p>
            <input  style="width: 50%; border: 1px solid #2aabd2; border-radius: 5px; margin-bottom: 10px; padding-left: 15px; background: #EEE;height: 10px;" type="password" required  id="claveRU" class="form-control">
            <input type="reset" style="display: none;" id="resetRegistro" class="form-control">
            
            <div class="text-center">
                <button type="button" class="btn btn-primary btn-sm" onclick="new_registroUsuario()">Registrar</button>
            </div>
        </center>
        </form>
    </div>
</div>
    

   
    <p style="color: #fff; bottom: 0px;position: fixed; z-index:0;"> Copyright &copy; IncicApp <?php echo date('Y',time()) ?>     </p>



<!--
<div class="container" >
    <div class="card testimonial-card" >
        <div class="card-up default-color-dark">
        </div>
        <div class="avatar">
            <img src="public/img/jose4.jpeg" class="rounded-circle img-responsive" style="border: 0px solid #fff;">
        </div>
        <div class="card-block">
            <h4 class="card-title">Jose Rodolfo</h4>
            <hr>
            <p><i class="fa fa-quote-left"></i> Analisis y Desarrollo en Sistemas de Informacion</p>
        </div>
    </div>
</div>-->



<!-- Modal Login -->
<div class="modal fade modal-ext" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content modal-md">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="w-100"><i class="fa fa-user"></i> Iniciar sesion</h3>
            </div>
            <!--Body-->
            <div class="modal-body">
                <form method="post" action="validacion/validar.php">
                    <div class="md-form">
                        <i class="fa fa-envelope prefix"></i>
                        <input type="text" id="form2" name="correo" class="form-control">
                        <label for="form2">Email</label>
                    </div>

                    <div class="md-form">
                        <i class="fa fa-lock prefix"></i>
                        <input type="password" id="form3" name="clave"  class="form-control">
                        <label for="form3">Contraseña</label>
                    </div>
                    <div class="text-center">
                        <center>  <button type="submit" class="btn btn-sm btn-primary btn-sm" name="entrar" >Iniciar Sesion </button>
                        <!--<br><p>Olvidaste la <a href="#">Contrasena?</a></p></center>-->
                    </div>
                </form>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm ml-auto" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

    <!-- JQuery -->
    <script src="public/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="public/js/jquery.toast.js"></script>
    <script src="public/js/functionss.js"></script>
    <!--<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>-->
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="public/js/tether.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="public/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="public/js/mdb.min.js"></script>
</body>

</html>
