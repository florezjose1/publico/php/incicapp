function busquedaPlag_cult_Xfecha(){
    $('#val_opt_busquedaPlag_Cult').val(1);
    var cult = $('#tip_CultivoBuscarPlag').val();
    var lote = $('#lotes_CultivoBuscarPlag').val();
    var usu = $('#cod_usu').val();
    selectPlagasCultivos(cult,lote,usu);
}
function mostrarPlagCultivosUsuario(){
	var o = $('#opt_despl_cultivosPlag').val();
	if (o == 0 ) {
        $('#opt_inf_plag_x_cultivo').val(1);
        detallePlagasUsuario();

		$('#sigPlagCultivos').html('-')
		$('#opt_despl_cultivosPlag').val(1);
		ejecutarMostrarCultivosPlag();
	}else{
		$('#sigPlagCultivos').html('+')
		$('#opt_despl_cultivosPlag').val(0);
        $('#sub_plag').hide('slow');
	}
}
// funcion que me muestra cultivos registrados por usuario
function ejecutarMostrarCultivosPlag() {
	var usu = $('#cod_usu').val();
    var param ={'Funcion':'cultivosXusuario', 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data) {
        	//console.log(data);
        	var json = JSON.parse(data);
            
            //caso especial
            // los datos se estan trayendo de forma descendentes, por lo que 
            // accedemos al ultimos id del cultivo para que este sea el contador y así 
            // poder hacer el ciclo de ocultar y/o mostrar submenu
            var a = json.length
            var h = json[a-1].id;
        	$('#cant_cult_X_usuarioPlag').val(h)
        	$('#nom_enf').html( (json[0].nombre).toUpperCase());

        	var chtml ="";
        	for (var i = 0; i< json.length; i++) {
        		var nombre = json[i].nombre;
        		var idC = parseInt(json[i].id);
                chtml = chtml + '<li>'+ 
                '<a onclick=\'detalleCultivoXusuarioPlag("'+idC+'","'+nombre+'","'+usu+ '")\' >'+ 
                    '<i class="fa fa-angle-double-right"></i> ' + nombre + 
                '</a>' + 
                '<ul id=\'loteXCult_plag'+idC+'\'  style="display: none;">'+
                    
                '</ul>' + '</li>';
        	}
        	$('#sub_plag').html(chtml);
        	$('#sub_plag').show('slow');
        }
    });
}
function detalleCultivoXusuarioPlag(idC,nombre,usu) {
    var cc = $('#cant_cult_X_usuarioPlag').val(); // cantidad de cultivos
    var m = $('#i_ocultMenu_cult_Plag').val(); // input-funcion de ocultar menu de cultivos por usuario
    if (m == 0 ) {
        for(var i = 0; i <= cc; i++) {
            if (i == idC ) {
                h = '#loteXCult_plag'+i;
                mostrarcultivosXusuarioPlag(idC,nombre,usu);
                //$(h).show('slow');
            }else{
                h = '#loteXCult_plag'+i;
                $(h).hide('slow');
            }
        }
        $('#i_ocultMenu_cult_Plag').val(1);  
    }else{
        h = '#loteXCult_plag'+idC;
        $(h).hide('slow');
        $('#i_ocultMenu_cult_Plag').val(0);
    }
}
// funcion con cantidad de lotes registrados por cultivos por usuario
function mostrarcultivosXusuarioPlag(idC,nombre,usu) {
    $('#nom_enf').html( (nombre).toUpperCase());
    var param ={'Funcion':'lotescultivosXusuario', 'usu':usu, 'id_cult':idC };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data) {
            ////console.log(data + ' **');

            var json = JSON.parse(data);
            var cant = json[0].lotes;
            $('#cant_lotes_cult_X_usuarioPlag').val(cant)
            var L_html ="";
            for (var i = 1; i<= cant; i++) {
                var lote = parseInt(i);
                L_html = L_html + '<li>'+ 
                '<a onclick=\'detalleXlotesPlag("'+lote+'","'+idC+'","'+usu+ '")\' >'+ 
                    '<i class="fa fa-caret-right"></i> Lote' + i + 
                '</a>' + 
                '<ul id=\'id_contenCul_Plag'+idC+'Xlotes'+lote+'\'  style="display: none;">'+
                    '<li><a onclick=\'mostarGraficalinealPlag("'+idC+'","'+lote+'","'+usu+ '")\' >'+
                        '<i class="">-</i> Grafica Lineal</a>'+
                    '</li>'+
                    '<li><a onclick=\'mostrarGraficabarrasPlag("'+idC+'","'+lote+'","'+usu+ '")\' >'+
                        '<i class="">-</i>  Grafica Barras</a>'+
                    '</li>'+
                    '<li><a onclick=\'mostrarGraficatortaPlag("'+idC+'","'+lote+'","'+usu+ '")\' >'+
                        '<i class="">-</i>  Grafica Torta</a>'+
                    '</li>'+
                '</ul>' + '</li>';
            }
            $('#loteXCult_plag'+idC).html(L_html);
            $('#loteXCult_plag'+idC).show('slow');
        }
    });
}
function mostarGraficalinealPlag(idC,lote,usu) {
    $('#graPlag').val(0);
    selectPlagasCultivos(idC,lote,usu);
}
function mostrarGraficabarrasPlag(idC,lote,usu) {
    $('#graPlag').val(1);
    selectPlagasCultivos(idC,lote,usu);
}
function mostrarGraficatortaPlag(idC,lote,usu) {
    $('#graPlag').val(2);
    selectPlagasCultivos(idC,lote,usu);
}
function detalleXlotesPlag(lote,idC,usu){
    $('#i_ocultMenu_cult_Plag').val(0);
    var cl = $('#cant_lotes_cult_X_usuarioPlag').val(); // cantidad de lotes
    var a = $('#i_ocultMenu_lotesPlag').val(); // input-funcion de ocultar menu de lotes por cultivo

    ////console.log(id + ' lllll ' + cl)
    if (a == 0 ) {
        ////console.log('entro')
        for(var i = 0; i <= cl; i++) {
            if (i == lote ) {
                n = '#id_contenCul_Plag'+idC+'Xlotes'+i;
                $(n).show('slow');
                mostarGraficalinealEnf(idC,lote,usu);
            }else{
                n = '#id_contenCul_Plag'+idC+'Xlotes'+i;
                $(n).hide('slow');
            }
        }
        $('#i_ocultMenu_lotesPlag').val(1);     
    }else{
        n = '#id_contenCul_Plag'+idC+'Xlotes'+lote;
        $(n).hide('slow');
        $('#i_ocultMenu_lotesPlag').val(0);
    }
}
function selectPlagasCultivos(idC,lote,usu) {
    var bus = $('#val_opt_busquedaPlag_Cult').val();
    ////console.log(bus + ' ...........')
    var usu = $('#cod_usu').val();
    if (bus != 0 ) {
        var fechaIN = $('#fechaInPlag').val();
        var fechaFi = $('#fechaFiPlag').val();
        var param ={'Funcion':'informacionpPlagasXcultivoLote', 'usu':usu, 'id_cult':idC, 'lote':lote, 'fecha1':fechaIN, 'fecha2':fechaFi, 'var':'1' };
    }else{
        var param ={'Funcion':'informacionpPlagasXcultivoLote', 'usu':usu, 'id_cult':idC, 'lote':lote, 'var':'2' };
    }
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            //console.log("data", data);
            $('#numeroLotePlagas').html('<b>Lote ' + lote+'</b>');
            //console.log("lote", lote);


            var datos = JSON.parse(data);
            var nombre = datos[0].cultivo;
            $('#nom_plag').html(nombre.toUpperCase())
                    var plag_1 = []; // variable para guardar array
                    var plag_2 = []; // variable para guardar array
                    var plag_3 = []; // variable para guardar array
                    var plag_4 = []; // variable para guardar array
                    var plag_5 = []; // variable para guardar array
                    var fecha = [];

                    var lim_1_for_E1 = 1;
                    var lim_2_for_E2 = 1;
                    var lim_3_for_E3 = 1;
                    var lim_4_for_E4 = 1;
                    var lim_5_for_E5 = 1;

                    var result1 = []; // array torta
                    var result2 = [];
                    var result3 = [];
                    var result4 = [];
                    var result5 = [];

                    // for en cual vamos a acceder a cada una de las posiciones del array
                    //for(var i = json.length-1; i>=0; i-- ) {
                    for(var i = datos.length-1; i>=0; i-- ) {
                        // variable que me trae enfermedad referente a la posicion del for
                        var plaga_1 = datos[i].plag1;
                        var plaga1_1 = datos[i].plag1;
                        ////console.log(plaga_1 + ' p-> ' + i)
                        ////console.log(plaga1_1 + ' p -> ' + i)
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (plaga_1 == 0 ) { 
                            proE1 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la plaga para al 
                            // final pasar a armar el array
                            if (plaga_1 != 9999 ) {
                                proE1 = plaga_1;
                            }else{
                                ////console.log(plaga_1 + ' ===========' + i)
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (plaga_1 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_plag1 = 0;
                                    for(var n_1_E1 = i; n_1_E1 >= 0; n_1_E1-- ) {
                                        cont_1_plag1 += 1;
                                        var val_1_e1 = datos[n_1_E1].plag1;
                                        if (val_1_e1 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e1 = 121212;
                                        }
                                    }
                                }
                                ////console.log(val_1_e1 + ' < < < ' + i)

                                if (plaga1_1 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_plag1 = 0;
                                    for(var n_2_E1 = i; n_2_E1 < datos.length; n_2_E1++ ) {
                                        cont_2_plag1 += 1;
                                        var val_2_e1 = datos[n_2_E1].plag1;
                                        if (val_2_e1 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e1 = 323232;
                                        }
                                    }
                                }
                                ////console.log(val_2_e1 + ' > > > ' + i)

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_plag1-1) + parseInt(cont_2_plag1-1);
                                ////console.log(espacios + ' ***********')

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                ////console.log(val_1_e1 + ' ====*****====' + val_2_e1)
                                if (val_1_e1 == 121212 || val_2_e1 == 323232) {
                                    var constt =898989;
                                }else{
                                    if (val_1_e1 == val_2_e1 ) {
                                        var constt = val_1_e1;
                                    }else{
                                        if (val_1_e1 > val_2_e1) {
                                            var constt = parseFloat(val_1_e1 - val_2_e1) / parseInt(espacios);
                                            ////console.log(constt +  '> > > > > > > > > > > > > > >')
                                        }else{
                                            if (val_2_e1 == 9999 ) {
                                                var constt =898989; ////console.log('kkkkkkkkkkkk');
                                            }else{
                                                ////console.log('< < < < < < < < < < < < < <')
                                                var constt = parseFloat(val_2_e1 - val_1_e1)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E1 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {
                                    ////console.log(lim_1_for_E1 + ' ************+')
                                    for (var aE1 = lim_1_for_E1; aE1 <= reFor_E1; aE1++) {
                                        var rt_E1 = parseFloat(constt * aE1);
                                        lim_1_for_E1 += 1;
                                        if (lim_1_for_E1 > reFor_E1) {
                                            lim_1_for_E1 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E1 = 545454;
                                }
                                if (rt_E1 != 545454 ) {
                                    if (val_1_e1 == val_2_e1 ) {
                                        proE1 = val_1_e1;
                                    }else{
                                        if (val_1_e1 > val_2_e1 ) {
                                            proE1 = (parseFloat(val_1_e1 - rt_E1)).toFixed(1);
                                        }else{
                                            proE1 = (parseFloat(val_1_e1 + rt_E1)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE1 = 121212;
                                }

                            }
                        }
                        ////console.log(proE1 +  '========00')
                        if (proE1 != 121212) {
                            plag_1.push(parseFloat(proE1));
                            result1.push([ datos[i].fecha , parseFloat(proE1)  ]);
                        }

                        // variable que me trae plaga referente a la posicion del for
                        var enfermedad_2 = datos[i].plag2;
                        var enfermedad2_2 = datos[i].plag2;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_2 == 0 ) { 
                            proE2 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_2 != 9999 ) {
                                proE2 = enfermedad_2;
                                //////console.log(proE2 + ' = p = ' + i)
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_2 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_plag2 = 0;
                                    for(var n_1_E2 = i; n_1_E2 >= 0; n_1_E2-- ) {
                                        cont_1_plag2 += 1;
                                        var val_1_e2 = datos[n_1_E2].plag2;
                                        if (val_1_e2 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e2 = 121212;
                                        }
                                    }
                                }
                                ////console.log(val_1_e2 + ' < < < ' + i)

                                if (enfermedad2_2 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_plag2 = 0;
                                    for(var n_2_E2 = i; n_2_E2 < datos.length; n_2_E2++ ) {
                                        cont_2_plag2 += 1;
                                        var val_2_e2 = datos[n_2_E2].plag2;
                                        if (val_2_e2 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e2 = 323232;
                                        }
                                    }
                                }
                                ////console.log(val_2_e2 + ' > > > ' + i)

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_plag2-1) + parseInt(cont_2_plag2-1);
                                ////console.log(espacios + ' ***********')

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                ////console.log(val_1_e1 + ' ====*****====' + val_2_e1)
                                if (val_1_e2 == 121212 || val_2_e2 == 323232) {
                                    var constt = 898989;
                                }else{
                                    if (val_1_e2 == val_2_e2 ) {
                                        var constt = val_1_e2;
                                    }else{
                                        if (val_1_e2 > val_2_e2) {
                                            var constt = parseFloat(val_1_e2 - val_2_e2) / parseInt(espacios);
                                            ////console.log(constt +  '> > > > > > > > > > > > > > >')
                                        }else{
                                            if (val_2_e2 == 9999 ) {
                                                var constt =898989;
                                            }else{
                                                ////console.log('< < < < < < < < < < < < < <')
                                                var constt = parseFloat(val_2_e2 - val_1_e2)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E2 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {
                                    ////console.log(lim_2_for_E2 + ' ************+')

                                    for (var aE2 = lim_2_for_E2; aE2 <= reFor_E2; aE2++) {
                                        var rt_E2 = parseFloat(constt * aE2);
                                        lim_2_for_E2 += 1;
                                        if (lim_2_for_E2 > reFor_E2) {
                                            lim_2_for_E2 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E2 = 545454;
                                }
                                if (rt_E2 != 545454 ) {
                                    if (val_1_e2 == val_2_e2 ) {
                                        proE2 = val_1_e2;
                                    }else{
                                        if (val_1_e2 > val_2_e2 ) {
                                            proE2 = (parseFloat(val_1_e2 - rt_E2)).toFixed(1);
                                        }else{
                                            proE2 = (parseFloat(val_1_e2 + rt_E2)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE2 = 121212;
                                }

                            }
                        }
                        if (proE2 != 121212) {
                            plag_2.push(parseFloat(proE2));
                            result2.push([ datos[i].fecha , parseFloat(proE2)] );
                        }

                        // variable que me trae enfermedad referente a la posicion del for
                        var enfermedad_3 = datos[i].plag3;
                        var enfermedad3_3 = datos[i].plag3;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_3 == 0 ) { 
                            proE3 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_3 != 9999 ) {
                                proE3 = enfermedad_3;
                                ////console.log(proE3 + ' = p = ' + i)
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_3 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_plag3 = 0;
                                    for(var n_1_E3 = i; n_1_E3 >= 0; n_1_E3-- ) {
                                        cont_1_plag3 += 1;
                                        var val_1_e3 = datos[n_1_E3].plag3;
                                        if (val_1_e3 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e3 = 121212;
                                        }
                                    }
                                }
                                ////console.log(val_1_e3 + ' < < < ' + i)

                                if (enfermedad3_3 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_plag3 = 0;
                                    for(var n_2_E3 = i; n_2_E3 < datos.length; n_2_E3++ ) {
                                        cont_2_plag3 += 1;
                                        var val_2_e3 = datos[n_2_E3].plag3;
                                        if (val_2_e3 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e3 = 323232;
                                        }
                                    }
                                }
                                ////console.log(val_2_e3 + ' > > > ' + i)

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_plag3-1) + parseInt(cont_2_plag3-1);
                                ////console.log(espacios + ' ***********')

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                ////console.log(val_1_e1 + ' ====*****====' + val_2_e1)
                                if (val_1_e3 == 121212 || val_2_e3 == 323232) {
                                    var constt =898989;
                                }else{
                                    if (val_1_e3 == val_2_e3 ) {
                                        var constt = val_1_e3;
                                    }else{
                                        if (val_1_e3 > val_2_e3) {
                                            var constt = parseFloat(val_1_e3 - val_2_e3) / parseInt(espacios);
                                            ////console.log(constt +  '> > > > > > > > > > > > > > >')
                                        }else{
                                            if (val_2_e3 == 9999 ) {
                                                var constt =898989;
                                            }else{
                                                ////console.log('< < < < < < < < < < < < < <')
                                                var constt = parseFloat(val_2_e3 - val_1_e3)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E3 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {
                                    ////console.log(lim_3_for_E3 + ' ************+')

                                    for (var aE3 = lim_3_for_E3; aE3 <= reFor_E3; aE3++) {
                                        var rt_E3 = parseFloat(constt * aE3);
                                        lim_3_for_E3 += 1;
                                        if (lim_3_for_E3 > reFor_E3) {
                                            lim_3_for_E3 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E3 = 545454;
                                }
                                if (rt_E3 != 545454 ) {
                                    if (val_1_e3 == val_2_e3 ) {
                                        proE3 = val_1_e3;
                                    }else{
                                        if (val_1_e3 > val_2_e3 ) {
                                            proE3 = (parseFloat(val_1_e3 - rt_E3)).toFixed(1);
                                        }else{
                                            proE3 = (parseFloat(val_1_e3 + rt_E3)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE3 = 121212;
                                }

                            }
                        }
                        if (proE3 != 121212) {
                            plag_3.push(parseFloat(proE3));
                            result3.push([ datos[i].fecha , parseFloat(proE3)] );
                        }

                        // variable que me trae enfermedad referente a la posicion del for
                        var enfermedad_4 = datos[i].plag4;
                        var enfermedad4_4 = datos[i].plag4;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_4 == 0 ) { 
                            proE4 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_4 != 9999 ) {
                                proE4 = enfermedad_4;
                                ////console.log(proE4 + ' = p = ' + i)
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_4 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_plag4 = 0;
                                    for(var n_1_E4 = i; n_1_E4 >= 0; n_1_E4-- ) {
                                        cont_1_plag4 += 1;
                                        var val_1_e4 = datos[n_1_E4].plag4;
                                        if (val_1_e4 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e4 = 121212;
                                            ////console.log('aaaaaaaaaaaaaaaaaa')
                                        }
                                    }
                                }
                                ////console.log(val_1_e4 + ' < < < ' + i)

                                if (enfermedad4_4 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_plag4 = 0;
                                    for(var n_2_E4 = i; n_2_E4 < datos.length; n_2_E4++ ) {
                                        cont_2_plag4 += 1;
                                        var val_2_e4 = datos[n_2_E4].plag4;
                                        if (val_2_e4 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e4 = 323232;
                                            ////console.log('oooooooooooooooooo')
                                        }
                                    }
                                }
                                ////console.log(val_2_e4 + ' > > > ' + i)

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_plag4-1) + parseInt(cont_2_plag4-1);
                                ////console.log(espacios + ' ***********')

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                ////console.log(val_1_e1 + ' ====*****====' + val_2_e1)
                                if (val_1_e4 == 121212 || val_2_e4 == 323232) {
                                    var constt =898989;
                                    ////console.log('iiiiiiiiiiiiiiiiiiiiiii')
                                }else{
                                    if (val_1_e4 == val_2_e4 ) {
                                        var constt = val_1_e4;
                                    }else{
                                        if (val_1_e4 > val_2_e4) {
                                            var constt = parseFloat(val_1_e4 - val_2_e4) / parseInt(espacios);
                                            ////console.log(constt +  '> > > > > > > > > > > > > > >')
                                        }else{
                                            if (val_2_e4 == 9999 ) {
                                                var constt =898989;
                                            }else{
                                                ////console.log('< < < < < < < < < < < < < <')
                                                var constt = parseFloat(val_2_e4 - val_1_e4)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E4 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {
                                    ////console.log(lim_4_for_E4 + ' ************+')

                                    for (var aE4 = lim_4_for_E4; aE4 <= reFor_E4; aE4++) {
                                        var rt_E4 = parseFloat(constt * aE4);
                                        lim_4_for_E4 += 1;
                                        if (lim_4_for_E4 > reFor_E4) {
                                            lim_4_for_E4 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E4 = 545454;
                                    ////console.log('eeeeeeeeeeeeeeee')
                                }
                                if (rt_E4 != 545454 ) {
                                    ////console.log('11111111111111111111')
                                    if (val_1_e4 == val_2_e4 ) {
                                        proE4 = val_1_e4;
                                    }else{
                                        if (val_1_e4 > val_2_e4 ) {
                                            proE4 = (parseFloat(val_1_e4 - rt_E4)).toFixed(1);
                                        }else{
                                            proE4 = (parseFloat(val_1_e4 + rt_E4)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE4 = 121212;
                                    ////console.log('2222222222222222222')
                                }

                            }
                        }
                        if (proE4 != 121212) {
                            ////console.log('hhhhhhhhhhhhhhhhhh')
                            plag_4.push(parseFloat(proE4));
                            result4.push([ datos[i].fecha , parseFloat(proE4)] );
                        }

                        // variable que me trae enfermedad referente a la posicion del for
                        var enfermedad_5 = datos[i].plag5;
                        var enfermedad5_5 = datos[i].plag5;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_5 == 0 ) { 
                            proE5 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_5 != 9999 ) {
                                proE5 = enfermedad_5;
                                ////console.log(proE5 + ' = p = ' + i)
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_5 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_plag5 = 0;
                                    for(var n_1_E5 = i; n_1_E5 >= 0; n_1_E5-- ) {
                                        cont_1_plag5 += 1;
                                        var val_1_e5 = datos[n_1_E5].plag5;
                                        if (val_1_e5 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e5 = 121212;
                                        }
                                    }
                                }
                                ////console.log(val_1_e5 + ' < < < ' + i)

                                if (enfermedad5_5 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_plag5 = 0;
                                    for(var n_2_E5 = i; n_2_E5 < datos.length; n_2_E5++ ) {
                                        cont_2_plag5 += 1;
                                        var val_2_e5 = datos[n_2_E5].plag5;
                                        if (val_2_e5 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e5 = 323232;
                                            ////console.log('oooooooooooooooooo')
                                        }
                                    }
                                }
                                ////console.log(val_2_e5 + ' > > > ' + i)

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_plag5-1) + parseInt(cont_2_plag5-1);
                                //////console.log(espacios + ' ***********')

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                ////console.log(val_1_e1 + ' ====*****====' + val_2_e1)
                                if (val_1_e5 == 121212 || val_2_e5 == 323232) {
                                    var constt =898989;
                                    ////console.log('iiiiiiiiiiiiiiiiiiiiiii')
                                }else{
                                    if (val_1_e5 == val_2_e5 ) {
                                        var constt = val_1_e5;
                                    }else{
                                        if (val_1_e5 > val_2_e5) {
                                            var constt = parseFloat(val_1_e5 - val_2_e5) / parseInt(espacios);
                                            ////console.log(constt +  '> > > > > > > > > > > > > > >')
                                        }else{
                                            if (val_2_e5 == 9999 ) {
                                                var constt =898989;
                                            }else{
                                                ////console.log('< < < < < < < < < < < < < <')
                                                var constt = parseFloat(val_2_e5 - val_1_e5)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E5 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {
                                    ////console.log(lim_5_for_E5 + ' ************+')

                                    for (var aE5 = lim_5_for_E5; aE5 <= reFor_E5; aE5++) {
                                        var rt_E5 = parseFloat(constt * aE5);
                                        lim_5_for_E5 += 1;
                                        if (lim_5_for_E5 > reFor_E5) {
                                            lim_5_for_E5 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E5 = 545454;
                                    ////console.log('eeeeeeeeeeeeeeee')
                                }
                                if (rt_E5 != 545454 ) {
                                    ////console.log('11111111111111111111')
                                    if (val_1_e5 == val_2_e5 ) {
                                        proE5 = val_1_e5;
                                    }else{
                                        if (val_1_e5 > val_2_e5 ) {
                                            proE5 = (parseFloat(val_1_e5 - rt_E5)).toFixed(1);
                                        }else{
                                            proE5 = (parseFloat(val_1_e5 + rt_E5)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE5 = 121212;
                                    ////console.log('2222222222222222222')
                                }

                            }
                        }
                        if (proE5 != 121212) {
                            ////console.log('hhhhhhhhhhhhhhhhhh')
                            plag_5.push(parseFloat(proE5));
                            result5.push([ datos[i].fecha , parseFloat(proE5)] );
                        }

                        fecha.push(datos[i].fecha);
                    }
                    
                    ////console.log(enf_1 + ' <======> array final ')
                    ////console.log(enf_2 + ' <======> array final ')
                    ////console.log(enf_3 + ' <======> array final ')
                    ////console.log(enf_4 + ' <======> array final ')
                    ////console.log(enf_5 + ' <======> array final ')

                    // variable para saber que tipo de grafica desea ver el usuario
                    // somo primera instancia, a este se le va a mostrar grafica lineal
                    var optioGrafic = $('#graPlag').val();
                    ////console.log(optioGrafic + ' *******+')
                    if (optioGrafic == 0 ) {
                        linealPlag(idC, fecha, plag_1, plag_2, plag_3, plag_4, plag_5);
                    }else{
                        if (optioGrafic == 1 ) {
                            barrasPlag(idC, fecha, plag_1, plag_2, plag_3, plag_4, plag_5);
                        }else{
                            if (optioGrafic == 2 ) {
                                var id_cul = $('#id_CultConsultaP').val();
                                var id_usu = $('#cod_usu').val();
                                var fechaIn = $('#fechaInP').val();
                                var fechaFi = $('#fechaFiP').val();
                                var a = $('#tipoBusqFechasPlag').val();
                                
                                //if (a == 0 ) {
                                    var param ={'Funcion':'infoPlagCultivosX_lotes_Torta', 'usu':usu, 'id_cult':idC, 'lote':lote };
                                //}else{
                                //    var param ={'Funcion':'infoPlagasCultivosTorta', 'id_cul':id_cul, 'id_usu':id_usu, 'fechaIn':fechaIn, 'fechaFi':fechaFi, 'opt':'1' };
                                //}
                                $.ajax({
                                    data: JSON.stringify(param),
                                    type: "JSON",
                                    url: 'ajax.php',
                                    success: function (data) {
                                        var json = JSON.parse(data);
                                        var proplag1 = json[0].plag1;
                                        var proplag2 = json[0].plag2;
                                        var proplag3 = json[0].plag3;
                                        var proplag4 = json[0].plag4;
                                        var proplag5 = json[0].plag5;

                                        tortaPlag(idC,proplag1,proplag2,proplag3,proplag4,proplag5,result1,result2,result3,result4,result5 );
                                    }
                                });
                            }
                        }
                    }
        }
    });
}

function linealPlag(id, fecha, plag_1, plag_2, plag_3, plag_4, plag_5) {
    //console.log("linealPlag", 'linealPlag');
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionPlagsUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            ////console.log(data + ' enfermedades por cultivo');
            var ne = JSON.parse(data);
            var w = ne.length;
            ////console.log(w + ' 00======!!!!!!!!!!==============00');
            var n1,n2,n3,n4,n5;
            if (w == 2 ) {
                n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                $('#detaPlagas').hide('slow');
                $('#optGraficaPlaga').css('display','block');
                Highcharts.chart('optGraficaPlaga', {
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: fecha
                    },
                    yAxis: {
                        title: {
                            text: '% incidencias'
                        },
                        plotLines: [{
                            value: l1,
                            color: 'red',
                            width: 2,
                            label: {
                                text: '<div> <br> ' + sig1 + ' </div>',
                                //align: 'center',
                                style: {
                                    color: 'gray'
                                }
                            }
                        },{
                            value: l2,
                            color: 'red',
                            width: 2,
                            label: {
                                text: sig2,
                                //align: 'center',
                                style: {
                                    color: 'gray'
                                }
                            }
                        }]
                    },
                    tooltip: {
                        crosshairs: true,
                        shared: true
                    },
                    plotOptions: {
                        spline: {
                            marker: {
                                radius: 4,
                                lineColor: '#666666',
                                lineWidth: 1
                            }
                        }
                    },
                    series: [{
                        name: n1 + ' ' + sig1,
                        data: plag_1
                    }, {
                        name: n2 + ' ' + sig2,
                        data: plag_2
                    }]
                });
            }else{
                if (w == 3 ) {
                    n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                    n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                    n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                    $('#detaPlagas').hide('slow');
                    $('#optGraficaPlaga').css('display','block');
                    Highcharts.chart('optGraficaPlaga', {
                        chart: {
                            type: 'spline'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: fecha
                        },
                        yAxis: {
                            title: {
                                text: '% incidencias'
                            },
                            plotLines: [{
                                value: l1,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: '<div> <br> ' + sig1 + ' </div>',
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            },{
                                value: l2,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: sig2,
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            },{
                                value: l3,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: '<div> <br> ' + sig3 + ' </div>',
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            }]
                        },
                        tooltip: {
                            crosshairs: true,
                            shared: true
                        },
                        plotOptions: {
                            spline: {
                                marker: {
                                    radius: 4,
                                    lineColor: '#666666',
                                    lineWidth: 1
                                }
                            }
                        },
                        series: [{
                            name: n1 + ' ' + sig1,
                            data: plag_1
                        }, {
                            name: n2 + ' ' + sig2,
                            data: plag_2
                        }, {
                            name: n3 + ' ' + sig3,
                            data: plag_3
                        }]
                    });
                }else{
                    if (w == 4 ) {
                        n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                        n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                        n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                        n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        $('#detaPlagas').hide('slow');
                        $('#optGraficaPlaga').css('display','block');
                        Highcharts.chart('optGraficaPlaga', {
                            chart: {
                                type: 'spline'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                categories: fecha
                            },
                            yAxis: {
                                title: {
                                    text: '% insidencias'
                                },
                                plotLines: [{
                                    value: l1,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: '<div> <br> ' + sig1 + ' </div>',
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l2,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: sig2,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l3,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: '<div> <br> ' + sig3 + ' </div>',
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l4,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: sig4,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                }]
                            },
                            tooltip: {
                                crosshairs: true,
                                shared: true
                            },
                            plotOptions: {
                                spline: {
                                    marker: {
                                        radius: 4,
                                        lineColor: '#666666',
                                        lineWidth: 1
                                    }
                                }
                            },
                            series: [{
                                name: n1 + ' ' + sig1,
                                data: plag_1
                            }, {
                                name: n2 + ' ' + sig2,
                                data: plag_2
                            }, {
                                name: n3 + ' ' + sig3,
                                data: plag_3
                            }, {
                                name: n4 + ' ' + sig4,
                                data: plag_4
                            }]
                        });
                    }else{
                        if (w==5) {
                            n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                            n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                            n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                            n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                            n5 = ne[4].nombre; l5 = ne[4].limite; sig5 = ne[4].sigla;
                            $('#detaPlagas').hide('slow');
                            $('#optGraficaPlaga').css('display','block');
                            Highcharts.chart('optGraficaPlaga', {
                                chart: {
                                    type: 'spline'
                                },
                                title: {
                                    text: ''
                                },
                                subtitle: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: fecha
                                },
                                yAxis: {
                                    title: {
                                        text: '% incidencias'
                                    },
                                    plotLines: [{
                                        value: l1,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div> <br> ' + sig1 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l2,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig2 ,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l3,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div> <br> ' + sig3 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l4,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig4,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l5,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div> <br> ' + sig5 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    }]
                                },
                                tooltip: {
                                    crosshairs: true,
                                    shared: true
                                },
                                plotOptions: {
                                    spline: {
                                        marker: {
                                            radius: 4,
                                            lineColor: '#666666',
                                            lineWidth: 1
                                        }
                                    }
                                },
                                series: [{
                                    name: n1,
                                    data: plag_1
                                }, {
                                    name: n2,
                                    data: plag_2
                                }, {
                                    name: n3,
                                    data: plag_3
                                }, {
                                    name: n4,
                                    data: plag_4
                                },{
                                    name: n5,
                                    data: plag_5
                                }]
                            });
                        }
                    }
                }
            }
        }
    });
}
function barrasPlag(id, fecha, plag_1, plag_2, plag_3, plag_4, plag_5) {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionPlagsUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            ////console.log(data + ' enfermedades por cultivo');
            var ne = JSON.parse(data);
            var w = ne.length;
            ////console.log(w + ' 00======!!!!!!!!!!==============00');
            var n1,n2,n3,n4,n5;
            if (w == 2 ) {
                n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                $('#detaPlagas').hide('slow');
                $('#optGraficaPlaga').css('display','block');
                Highcharts.chart('optGraficaPlaga', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: fecha ,
                        crosshair: true
                    },
                    yAxis: {
                        title: {
                            text: '% incidencias'
                        },
                        plotLines: [{
                            value: l1,
                            color: 'red',
                            width: 2,
                            label: {
                                text: sig1,
                                //align: 'center',
                                style: {
                                    color: 'gray'
                                }
                            }
                        },{
                            value: l2,
                            color: 'red',
                            width: 2,
                            label: {
                                text: sig2,
                                //align: 'center',
                                style: {
                                    color: 'gray'
                                }
                            }
                        }]
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} % </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: n1 + ' ' + sig1 ,
                        data: plag_1

                    }, {
                        name: n2 + ' ' + sig2 ,
                        data: plag_2

                    }]
                });
            }else{
                if (w == 3 ) {
                    n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                    n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                    n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                    $('#detaPlagas').hide('slow');
                    $('#optGraficaPlaga').css('display','block');
                    Highcharts.chart('optGraficaPlaga', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: fecha ,
                            crosshair: true
                        },
                        yAxis: {
                            title: {
                                text: '% incidencias'
                            },
                            plotLines: [{
                                value: l1,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: sig1,
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            },{
                                value: l2,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: sig2,
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            },{
                                value: l3,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: sig3,
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            }]
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} % </b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: n1 + ' ' + sig1 ,
                            data: plag_1

                        }, {
                            name: n2 + ' ' + sig2 ,
                            data: plag_2

                        }, {
                            name: n3 + ' ' + sig3 ,
                            data: plag_3

                        }]
                    });
                }else{
                    if (w == 4 ) {
                        n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                        n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                        n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                        n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        $('#detaPlagas').hide('slow');
                        $('#optGraficaPlaga').css('display','block');
                        Highcharts.chart('optGraficaPlaga', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                categories: fecha ,
                                crosshair: true
                            },
                            yAxis: {
                                title: {
                                    text: '% incidencias'
                                },
                                plotLines: [{
                                    value: l1,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: sig1,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l2,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: sig2,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l3,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: sig3,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l4,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: sig4,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                }]
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} % </b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: [{
                                name: n1 + ' ' + sig1 ,
                                data: plag_1

                            }, {
                                name: n2 + ' ' + sig2 ,
                                data: plag_2

                            }, {
                                name: n3 + ' ' + sig3 ,
                                data: plag_3

                            }, {
                                name: n4 + ' ' + sig4 ,
                                data: plag_4

                            }]
                        });
                    }else{
                        if (w==5) {
                            n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                            n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                            n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                            n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                            n5 = ne[4].nombre; l5 = ne[4].limite; sig5 = ne[4].sigla;

                            $('#detaPlagas').hide('slow');
                            $('#optGraficaPlaga').css('display','block');
                            Highcharts.chart('optGraficaPlaga', {
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: ''
                                },
                                subtitle: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: fecha ,
                                    crosshair: true
                                },
                                yAxis: {
                                    title: {
                                        text: '% incidencias'
                                    },
                                    plotLines: [{
                                        value: l1,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div> <br> ' + sig1 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l2,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig2 ,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l3,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div> <br> ' + sig3 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l4,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig4,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l5,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div> <br> ' + sig5 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    }]
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    '<td style="padding:0"><b>{point.y:.1f} % </b></td></tr>',
                                    footerFormat: '</table>',
                                    shared: true,
                                    useHTML: true
                                },
                                plotOptions: {
                                    column: {
                                        pointPadding: 0.2,
                                        borderWidth: 0
                                    }
                                },
                                series: [{
                                    name: n1 + ' ' + sig1 ,
                                    data: plag_1
                                }, {
                                    name: n2 + ' ' + sig2 ,
                                    data: plag_2
                                }, {
                                    name: n3 + ' ' + sig3 ,
                                    data: plag_3
                                }, {
                                    name: n4 + ' ' + sig4 ,
                                    data: plag_4
                                },{
                                    name: n5 + ' ' + sig5 ,
                                    data: plag_5
                                }]
                            });
                        }
                    }
                } 
            }
        }
    });
}
function tortaPlag(id,proplag1,proplag2,proplag3,proplag4,proplag5,result1,result2,result3,result4,result5 ) {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionPlagsUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            ////console.log(data + ' enfermedades por cultivo');
            var ne = JSON.parse(data);
            var w = ne.length;
            if (w == 2 ) {
                n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                $('#detaPlagas').hide('slow');
                $('#optGraficaPlaga').css('display','block');
                Highcharts.chart('optGraficaPlaga', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}: {point.y:.1f}%'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                    },
                    series: [{
                        name: 'Enfermedades',
                        colorByPoint: true,
                        data: [{
                            name: n1 + ' ' + sig1,
                            y: proplag1,
                            drilldown: 'enf_1'
                        }, {
                            name: n2 + ' ' + sig2,
                            y: proplag2,
                            drilldown: 'enf_2'
                        }]
                    }],
                    drilldown: {
                        series: [{
                            name: n1  + ' ' + sig1,
                            id: 'enf_1',
                            data: result1
                        }, {
                            name: n2 + ' ' + sig2,
                            id: 'enf_2',
                            data: result2
                        }]
                    }
                });
            }else{
                if (w == 3 ) {
                    n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                    n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                    n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                    $('#detaPlagas').hide('slow');
                    $('#optGraficaPlaga').css('display','block');
                    Highcharts.chart('optGraficaPlaga', {
                        chart: {
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y:.1f}%'
                                }
                            }
                        },

                        tooltip: {
                            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                        },
                        series: [{
                            name: 'Enfermedades',
                            colorByPoint: true,
                            data: [{
                                name: n1 + ' ' + sig1,
                                y: proplag1,
                                drilldown: 'enf_1'
                            }, {
                                name: n2 + ' ' + sig2,
                                y: proplag2,
                                drilldown: 'enf_2'
                            }, {
                                name: n3 + ' ' + sig3,
                                y: proplag3,
                                drilldown: 'enf_3'
                            }]
                        }],
                        drilldown: {
                            series: [{
                                name: n1  + ' ' + sig1,
                                id: 'enf_1',
                                data: result1
                            }, {
                                name: n2 + ' ' + sig2,
                                id: 'enf_2',
                                data: result2
                            }, {
                                name: n3 + ' ' + sig3,
                                id: 'enf_3',
                                data: result3
                            }]
                        }
                    });
                }else{
                    if (w == 4 ) {
                        n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                        n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                        n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                        n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        $('#detaPlagas').hide('slow');
                        $('#optGraficaPlaga').css('display','block');
                        Highcharts.chart('optGraficaPlaga', {
                            chart: {
                                type: 'pie'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            plotOptions: {
                                series: {
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.name}: {point.y:.1f}%'
                                    }
                                }
                            },

                            tooltip: {
                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                            },
                            series: [{
                                name: 'Enfermedades',
                                colorByPoint: true,
                                data: [{
                                    name: n1 + ' ' + sig1,
                                    y: proplag1,
                                    drilldown: 'enf_1'
                                }, {
                                    name: n2 + ' ' + sig2,
                                    y: proplag2,
                                    drilldown: 'enf_2'
                                }, {
                                    name: n3 + ' ' + sig3,
                                    y: proplag3,
                                    drilldown: 'enf_3'
                                }, {
                                    name: n4 + ' ' + sig4,
                                    y: proplag4,
                                    drilldown: 'enf_4'
                                }]
                            }],
                            drilldown: {
                                series: [{
                                    name: n1 + ' ' + sig1,
                                    id: 'enf_1',
                                    data: result1
                                }, {
                                    name: n2 + ' ' + sig2,
                                    id: 'enf_2',
                                    data: result2
                                }, {
                                    name: n3 + ' ' + sig3,
                                    id: 'enf_3',
                                    data: result3
                                }, {
                                    name: n4 + ' ' + sig4,
                                    id: 'enf_4',
                                    data: result4
                                }]
                            }
                        });
                    }else{
                        n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                        n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                        n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                        n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        n5 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        $('#detaPlagas').hide('slow');
                        $('#optGraficaPlaga').css('display','block');
                        Highcharts.chart('optGraficaPlaga', {
                            chart: {
                                type: 'pie'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            plotOptions: {
                                series: {
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.name}: {point.y:.1f}%'
                                    }
                                }
                            },

                            tooltip: {
                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                            },
                            series: [{
                                name: 'Enfermedades',
                                colorByPoint: true,
                                data: [{
                                    name: n1 + ' ' + sig1,
                                    y: proplag1,
                                    drilldown: 'enf_1'
                                }, {
                                    name: n2 + ' ' + sig2,
                                    y: proplag2,
                                    drilldown: 'enf_2'
                                }, {
                                    name: n3 + ' ' + sig3,
                                    y: proplag3,
                                    drilldown: 'enf_3'
                                }, {
                                    name: n4 + ' ' + sig4,
                                    y: proplag4,
                                    drilldown: 'enf_4'
                                }, {
                                    name: n5 + ' ' + sig5,
                                    y: proplag5,
                                    drilldown: 'enf_5'
                                }]
                            }],
                            drilldown: {
                                series: [{
                                    name: n1 + ' ' + sig1,
                                    id: 'enf_1',
                                    data: result1
                                }, {
                                    name: n2 + ' ' + sig2,
                                    id: 'enf_2',
                                    data: result2
                                }, {
                                    name: n3 + ' ' + sig3,
                                    id: 'enf_3',
                                    data: result3
                                }, {
                                    name: n4 + ' ' + sig4,
                                    id: 'enf_4',
                                    data: result4
                                }, {
                                    name: n5 + ' ' + sig5,
                                    id: 'enf_5',
                                    data: result5
                                }]
                            }
                        });
                    }
                }
            }
        }
    });
}

function abrirModalRegistroPlagas() {
    $('#regisControlPlag').click();
    var usu = $('#cod_usu').val();
   // ////console.log(usu + ' codigo de usuario logueado')
    var param ={'Funcion':'cultivosUsuario', 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            ////console.log(data);
            var json = JSON.parse(data);
            var h = "";
            for(var i = 0; i < json.length; i++) {
                h = h + '<option value="' + json[i].id +'">' + json[i].nombre + '</option>';
            }
            $('#tip_CultivoPlag').show();
            $('#tip_Plag').show();
            $('#tip_CultivoPlag').html('<option value="0"></option>' + h);
        }
    });
}
function selectPlagasRegistro() {
    var id = $('#tip_CultivoPlag').val();
    var usu = $('#cod_usu').val();
    ////console.log(id + ' id cultivo');
    var param ={'Funcion':'InformacionPlagsUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            ////console.log(data);
            var json = JSON.parse(data);
            var h = "";
            for(var i = 0; i < json.length; i++) {
                h = h + '<option value="' + json[i].id_plag +'">' + json[i].nombre + '</option>';
            }
            $('#tip_Plag').html(h);

            var param ={'Funcion':'lotescultivosXusuario', 'id_cult':id, 'usu':usu };
            $.ajax({
                data: JSON.stringify(param),
                type: "JSON",
                url: 'ajax.php',
                success: function(data){
                    var json = JSON.parse(data);
                    var lhtm = "";
                    for(var i = 1; i <= json[0].lotes; i++) {
                        lhtm = lhtm + '<option value="' + i +'">Lote ' + i + '</option>';
                    }
                    $('#tip_lotePlag').html(lhtm);
                }
            }); 
        }
    });
}
function insertRePlanPlagas() {
    var id = $('#tip_CultivoPlag').val();
    var id1 = $('#tip_Plag').val();
    var planSem = $('#plantasSemPlag').val();
    var planEnf = $('#plantasPlag').val();
    var fecha = $('#fechaRPlag').val();
    var usu = $('#cod_usu').val();
    var lote = $('#tip_lotePlag').val();

    //console.log(lote + ' lotee')

    var f = new Date();
    var dia =  parseInt( f.getDate() );
    var mes =  parseInt( (f.getMonth() +1) );
    var anno =  parseInt( f.getFullYear() );


    if (dia < 10 ) {
        var dia = '0'+f.getDate();
    }
    if (mes < 10 ) {
        var mes = '0'+(f.getMonth() +1);
    }

    var n = f.getFullYear() + "-" + mes + "-" + dia ;

    //if (fecha == n ) {
        var param ={'Funcion':'insertPlagasCultivo', 'usu':usu, 'id':id, 'id1':id1, 'lote':lote, 'fecha':fecha, 'variable':'1' };
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: 'ajax.php',
            success: function (data) {
                //console.log('==============' + data)
                var lote = $('#tip_lotePlag').val();
                if (data != 0 ) {
                    //console.log('aaaaaa')
                   var param ={'Funcion':'insertPlagasCultivo', 'usu':usu, 'id':id, 'id1':id1, 'lote':lote, 'planSem':planSem, 'planEnf':planEnf, 'fecha':fecha, 'variable':'2' };
                }else{
                    //console.log('envio ')
                    var param ={'Funcion':'insertPlagasCultivo', 'usu':usu, 'id':id, 'id1':id1, 'lote':lote, 'planSem':planSem, 'planEnf':planEnf, 'fecha':fecha, 'variable':'3' };
                }
                $.ajax({
                    data: JSON.stringify(param),
                    type: "JSON",
                    url: 'ajax.php',
                    success: function (data) {
                        //console.log(data +  '????????')
                        if (data != 0 ) {
                            var menj = 'Processing! Please wait..';
                            var head = 'Registrado';
                            var texto = 'Se ha insertado un nuevo registro';
                            success(menj,head,texto);
                            setTimeout(function(){
                                $('#resertInserCultivoPlag').click();
                                $('#cerrarRegCultivosPlag').click();
                                var usu = $('#cod_usu').val();
                                selectPlagasCultivos(id,lote,usu)

                                var usu = $('#cod_usu').val();
                                var param ={'Funcion':'InformacionPlagasxUser', 'id':id1, 'cul':id, 'usu':usu };
                                $.ajax({
                                    data: JSON.stringify(param),
                                    type: "JSON",
                                    url: 'ajax.php',
                                    success: function(data){
                                        ////console.log(data)
                                        var json = JSON.parse(data)
                                        var limite = json[0].limite;
                                        var alert = parseFloat(planEnf/planSem)*100;

                                        setTimeout(function(){
                                            if (alert > limite) {
                                                $.toast({
                                                    text: "Debes tomar medidas curativas ! consulte al asesor técnico / agrónomo", // Text that is to be shown in the toast
                                                    heading: 'Alerta !', // Optional heading to be shown on the toast
                                                    icon: 'error', // Type of toast icon
                                                    showHideTransition: 'slide', // fade, slide or plain
                                                    allowToastClose: false, // Boolean value true or false
                                                    hideAfter: 20000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                                                    stack: false, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                                                    position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                                                    
                                                    
                                                    
                                                    textAlign: 'left',  // Text alignment i.e. left, right or center
                                                    loader: true,  // Whether to show loader or not. True by default
                                                    loaderBg: '#20c600',  // Background color of the toast loader
                                                    beforeShow: function () {}, // will be triggered before the toast is shown
                                                    afterShown: function () {}, // will be triggered after the toat has been shown
                                                    beforeHide: function () {}, // will be triggered before the toast gets hidden
                                                    afterHidden: function () {}  // will be triggered after the toast has been hidden
                                                });
                                            }else{
                                                $.toast({
                                                    text: "Debes tomar medidas preventivas, como labores culturales ! ", // Text that is to be shown in the toast
                                                    heading: 'Alerta !', // Optional heading to be shown on the toast
                                                    icon: 'warning', // Type of toast icon
                                                    showHideTransition: 'slide', // fade, slide or plain
                                                    allowToastClose: false, // Boolean value true or false
                                                    hideAfter: 20000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                                                    stack: false, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                                                    position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                                                    
                                                    
                                                    
                                                    textAlign: 'left',  // Text alignment i.e. left, right or center
                                                    loader: true,  // Whether to show loader or not. True by default
                                                    loaderBg: '#20c600',  // Background color of the toast loader
                                                    beforeShow: function () {}, // will be triggered before the toast is shown
                                                    afterShown: function () {}, // will be triggered after the toat has been shown
                                                    beforeHide: function () {}, // will be triggered before the toast gets hidden
                                                    afterHidden: function () {}  // will be triggered after the toast has been hidden
                                                });
                                            }                                            
                                        },3000);
                                    }
                                });
                            },1500);
                        }else{
                            var title = 'Processing!';
                            var ms = ' Please wait..';
                            var mensaje = 'Algo ha salido mal... Vuelve a intentarlo ! ';
                            error(title,ms,mensaje);
                        }
                    }
                });
            }
        });
    /*}else{
        var title = 'Procesando!';
        var ms = ' Por favor espere..';
        var mensaje = 'La fecha no es correcta ! ';
        error(title,ms,mensaje); 
    }*/
}





function detallePlagasUsuario(){
    var a = $('#opt_inf_plag_x_cultivo').val();
    if (a == 0 ) {
        $('#opt_despl_cultivosPlag').val(1); // damos valor a menu para que este se cierre al intentar abrir otro
        mostrarPlagCultivosUsuario(); //ejecutamos funcion para que me cierre menú abierto
        $('#sub_plag').hide('slow');

        $('#desplegar2plag').html('-')
        $('#opt_inf_plag_x_cultivo').val(1);
        detalleInfPlagCultivosUsu();

    }else{
        $('#desplegar2plag').html('+')
        $('#sub_detallesPlagas').hide('slow');
        $('#opt_inf_plag_x_cultivo').val(0);
    }
}
function detalleInfPlagCultivosUsu() {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'cultivosUsuario', 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            ////console.log(data);
            var datos = JSON.parse(data);
            
            var dd = "";
            for(var i = 0; i < datos.length; i++) {
                var nom = datos[i].nombre;
                dd = dd + '<li>'+ 
                    '<a onclick=\'detallesPlagasUsuXcult("'+datos[i].id+'","'+nom+ '")\' >'+ 
                        '<i class="fa fa-caret-right"></i> ' + datos[i].nombre + 
                    '</a>' + 
                '</li>';
            }
            $('#sub_detallesPlagas').html(dd); // lista de opciones de cultivos
            $('#sub_detallesPlagas').show('slow'); // abrimos contenedor de lista
        }
    });
}
function detallesPlagasUsuXcult(id,nom) {
    var usu = $('#cod_usu').val();
    var cul = id;

    var param ={'Funcion':'InformacionPlagsUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data) {
            ////console.log(data)
            var json = JSON.parse(data)
            var n = "";
            for(var a = 0; a < json.length; a++) {
                n = n + '<tr>'+
                    '<td>'+json[a].nombre+'</td>'+
                    '<td>'+json[a].sigla+'</td>'+
                    '<td>'+json[a].limite+'</td>'+
                    '<td>' +
                        '<a class="iconos" onclick="DetallesupdateInformacionPlagas('+json[a]['id_plag']+','+cul+')">' +
                            '<i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>' +
                        '</a>' +
                    '</td>' +
                    '<td>' +
                        '<a class="iconos" onclick="InformacionPlagas('+json[a]['id_plag']+','+cul+')">' +
                            '<i class=\"fa fa-low-vision\" aria-hidden=\"true\"></i>' +
                        '</a>' +
                    '</td>' +
                '</tr>'    
                '</tr>'    
            }
            $('#nom_cultplag').val(nom)
            $('#optGraficaPlaga').hide('slow');
            $('#detallesoptGraficaPlaga').hide('slow');

            $('#cuerpoPlagUsuario').html(n);
            $('#nomPlaDetalles').html(nom);
            $('#detaPlagasUSu').show('slow');
            
        }
    });
}
function DetallesupdateInformacionPlagas(id,cul) {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionPlagasxUser', 'id':id, 'cul':cul, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            ////console.log(data)
            $('#updateInformacionPlagas').click();
            var json = JSON.parse(data)
            var title = json[0].nombre;
            var sigla = json[0].sigla;
            var limite = json[0].limite;
            var cod = json[0].id_plag;
            var id_cult = json[0].id_cult;
            $('#cod_plag').val(cod)
            $('#cod_cultPlag').val(id_cult)
            $('#nomPlagUpdate').val(title);
            $('#sigPlagUpdate').val(sigla);
            $('#limPlagUpdate').val(limite);
        }
    });
}
function UpdateDetallesPlagas() {
    var usu = $('#cod_usu').val();
    var cod = $('#cod_plag').val();
    var cul = $('#cod_cultPlag').val();
    var nombre = $('#nomPlagUpdate').val();
    var sigla = $('#sigPlagUpdate').val();
    var limite = $('#limPlagUpdate').val();
    var param ={'Funcion':'UpdateDetallesPlagas', 'usu':usu, 'cod':cod, 'cul':cul, 'nombre':nombre, 'sigla':sigla, 'limite':limite };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            if (data != 0 ) {
                var menj = 'Processing! Please wait..';
                var head = 'Actualizado';
                var texto = 'Se ha Actualizado un nuevo registro';
                success(menj,head,texto);
                setTimeout(function(){
                    $('#cerrarUpdatePlag').click();
                    var n = $('#nom_cultplag').val();
                    detallesPlagasUsuXcult(cul,nombre)
                },1500);
            }else{
                var title = 'Procesando!';
                var ms = ' Por favor espere..';
                var mensaje = 'error ! Algo ha salido mal ! ';
                error(title,ms,mensaje); 
            }
        }
    });
}
function InformacionPlagas(id,cul) {
    //console.log(id +  ' !!!!!!!!!!!!!' + cul)
    var param ={'Funcion':'InformacionPlagas', 'id':id, 'cul':cul };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            //console.log(data)
            var json = JSON.parse(data)
            var title = json[0].nombre;
            var descripcion = json[0].descripcion;
            var sinSing = json[0].sint_sig;
            $('#detallesInformacionPlagas').click();
            $('#nameDetallePlag').html(title);
            $('#descripcionPlaga').html(descripcion);
            $('#sinSigPlaga').html(sinSing);
        }
    });
}
function buscarLotesPlag() {
    var id = $('#tip_CultivoBuscarPlag').val();
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'lotescultivosXusuario', 'id_cult':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            var json = JSON.parse(data);
            var lhtm = "";
            for(var i = 1; i <= json[0].lotes; i++) {
                lhtm = lhtm + '<option value="' + i +'">Lote ' + i + '</option>';
            }
            $('#lotes_CultivoBuscarPlag').html(lhtm);
            $('#lotes_CultivoBuscarPlag').show();
        }
    });   
}