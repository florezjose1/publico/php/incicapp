function deplegueMenu1() {
	var opt = $('#deplegarMenu1').val();
	if (opt == 0 ) {
		$('.conOptions').show('slow');
        $('.contenido-index').css('margin-top','70px');
        $('#deplegarMenu1').val(1);
    }else{
        $('.conOptions').hide('slow');
        $('#deplegarMenu1').val(0);
	}
}
function deplegueMenu2() {
	var opt = $('#deplegarMenu2').val();
	if (opt == 0 ) {
		$('.conOptions').show('slow');
		$('.container-botones').show('slow');
        $('.container-salto').html('<br><br><br><br><br><br><br><br><br><br>')
        $('#deplegarMenu2').val(1);
    }else{
		$('.container-botones').hide();
        $('.conOptions').hide('slow');
        $('.container-salto').html('');
        $('#deplegarMenu2').val(0);
	}
}


// funcion de inicio... esta la ejecutamos cada vez que el usuario inicia sesion
// donde preguntamos si este ya tiene cultivos registrados para mostrarle dependiendo
// cierta informacion
function funtion_inicio() {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionUsuario', 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            if (data != 0 ) {
                $('.mensajesBienvenida').css('display','none');
                
                $('#val-infoUsuariosEnf').css('display','block'); // ocultamos contenido de enfermedades
                $('#val-infoUsuariosPlag').css('display','block'); // ocultamos contenido de plagas

                $('#enfermBtn').removeClass('disabled');
                $('#plagasBoton').removeClass('disabled');
                $('#cultivosBoton').removeClass('disabled');
                selectCultivosXusuario();
                
                setTimeout(function(){
                    var cult = $('#cult_inicio').val();
                    var lote = $('#lote_inicio').val();
                    $('#graEnfermedad').val(0);
                    selectEnfermedadesCultivos(cult,lote,usu);
                    var nombre = $('#nomCult_inicio').val();
                    $('#nom_enf').html(nombre.toUpperCase());
                    $('#nom_plag').html(nombre.toUpperCase());

                    selectPlagasCultivos(cult,lote,usu)

                },500);
            }else{
                // agregamos clases para deshabilitar algunos botones y/o ocultamos ciertos contenedores
                // con informacion que aparecerá cuando este tenga datos registrados.
                $('#cultivosBoton').addClass('disabled');
                $('#enfermBtn').addClass('disabled');
                $('#plagasBoton').addClass('disabled');
                $('#val-infoUsuariosEnf').css('display','none'); // ocultamos contenido de enfermedades
                $('#val-infoUsuariosPlag').css('display','none'); // ocultamos contenido de plagas
                $('#infoCultivos').css('display','none'); // ocultamos contenido
                //$('#tutoInicio').click();
            }

            var usu = $('#cod_usu').val();
            var param ={'Funcion':'cultivosUsuario', 'usu':usu };
            $.ajax({
                data: JSON.stringify(param),
                type: "JSON",
                url: 'ajax.php',
                success: function (data) {
                    var json = JSON.parse(data);
                    var h = "";
                    for(var i = 0; i < json.length; i++) {
                        h = h + '<option value="' + json[i].id +'">' + json[i].nombre + '</option>';
                    }
                    $('#tip_CultivoBuscar').html('<option value="0">Selecciona Cultivo</option>' + h);
                    $('#tip_CultivoBuscar').show();

                    $('#tip_CultivoBuscarPlag').html('<option value="0">Selecciona Cultivo</option>' + h);
                    $('#tip_CultivoBuscarPlag').show();
                }
            });
        }
    });
}
function siguiente(){
    $('.mensajesBienvenida').css('display','none');
    $('.videoIntro').show('slow');
}
function saltarTutorial() {
    $('#infoCultivos').show('slow');
    $('.contenido_tutorial').hide();
}
function saltarVideo() {
    $('#infoCultivos').show('slow');
    $('.videoIntro').hide();
}
function mostrarMenuBuscar(){
    var n = $('#opt_ocult_bus_enf').val();
    if (n == 0 ) {
        $('.contentBtnBuscar').show('slow');
        $('#opt_ocult_bus_enf').val(1)
    }else{
        $('.contentBtnBuscar').hide('slow');
        $('#opt_ocult_bus_enf').val(0)
    }
}
function mostrarMenuBuscarPlag(){
    var n = $('#opt_ocult_bus_Plag').val();
    if (n == 0 ) {
        $('.contentBtnBuscarPlag').show('slow');
        $('#opt_ocult_bus_Plag').val(1)
    }else{
        $('.contentBtnBuscarPlag').hide('slow');
        $('#opt_ocult_bus_Plag').val(0)
    }
}

function new_registroUsuario() {
    var correo = $('#correoRU').val();
    var param ={'Funcion':'new_registrarUsuarios', 'correo':correo, 'variable':'1' };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'app/ajax.php',
        success: function(data){
            if (data != 0 ) {
                var title = 'Processing!';
                var ms = ' Por favor espere..';
                var mensaje = 'Email ya exite ! Prueba con otro ! ';
                error(title,ms,mensaje);
            }else{
                var nombre      = $('#nombreRu').val();
                var telefono    = $('#telefonoRU').val();
                var genero      = $('input:radio[name=genero]:checked').val();
                var correo      = $('#correoRU').val();
                var clave       = $('#claveRU').val();

                if (nombre.length != 0) {
                    if (telefono.length != 0 ) {
                        if (genero.length != 0 ) {
                            if ( /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(correo) ) {
                                if (clave.length != 0 ) {
                                    var param ={'Funcion':'new_registrarUsuarios', 'nombre':nombre,'telefono':telefono, 'genero':genero, 'correo':correo, 'clave':clave, 'variable':'2' };
                                    $.ajax({
                                        data: JSON.stringify(param),
                                        type: "JSON",
                                        url: 'app/ajax.php',
                                        success: function(data){
                                            if (data != 0) {
                                                var menj = 'Procesando! Por favor espere..';
                                                var head = 'Registrado';
                                                var texto = 'Usuario registrado';
                                                success(menj,head,texto);
                                                setTimeout(function(){
                                                    $('#resetRegistro').click();
                                                    $('#cerrarRegistro').click();
                                                    $('#btnIniciarSesion').click();
                                                },1500);
                                            }else{
                                                var title = 'Procesando!';
                                                var ms = ' Por Favor espere..';
                                                var mensaje = 'Algo ha salido mal! vuelve a intentarlo ! ';
                                                error(title,ms,mensaje);
                                            }
                                        }   
                                    });
                                }else{
                                    var title = 'Procesando!';
                                    var ms = ' Por favor espere..';
                                    var mensaje = 'Debes colocar una contrase単a! ';
                                    error(title,ms,mensaje); 
                                }
                            }else{
                                var title = 'Procesando!';
                                var ms = ' Por favor espere..';
                                var mensaje = 'Email Invalido ! Verifica! ';
                                error(title,ms,mensaje); 
                            }
                        }else{
                            var title = 'Procesando!';
                            var ms = ' Por favor espere..';
                            var mensaje = 'No has seleccionado genero ';
                            error(title,ms,mensaje); 
                        }
                    }else{
                        var title = 'Procesando!';
                        var ms = ' Por favor espere..';
                        var mensaje = 'Campo de telefono vacio ! ';
                        error(title,ms,mensaje); 
                    }
                }else{
                    var title = 'Procesando!';
                    var ms = ' Por favor espere..';
                    var mensaje = 'Campo de nombre vacio ! ';
                    error(title,ms,mensaje);                 
                }
            }
        }
    });
}




$(function(){
    // funciones probatorias para saber el porcentaje de scroll de nuestra pagina
    $("#detectascroll").on("click", function () {
        //scroll vertical
        var sv = $(document).scrollTop();
        //scroll horizontal
        var sh = $(document).scrollLeft();
        $(this).find("span").text("(" + sh + "," + sv+ ")");
    });
    $("#scrollelemento").on("click", function () {
        var boton = $(this);
        var elemento = boton.parent();
        //scroll vertical
        var sv = elemento.scrollTop();
        //scroll horizontal
        var sh = elemento.scrollLeft();
        boton.find("span").text("(" + sh + "," + sv+ ")");
    });

    // funcion ir a la seccion de cultivos
    $("#cultivosBoton").on("click", function(){
        var a = $( window ).width();
        if (a<768){
            deplegueMenu2();
        }
        var posicion = $("#detallesCultivo").offset().top;
        $("html, body").animate({
            scrollTop: posicion
        }, 1000);
    });

    $("#enfermBtn").on("click", function(){
        var a = $( window ).width();
        if (a<768){
            deplegueMenu2();
        }
        var posicion = $("#graficasEnfermedades").offset().top;
        $("html, body").animate({
            scrollTop: posicion
        }, 1000);
    });

    $("#plagasBoton").on("click", function(){
        var a = $( window ).width();
        if (a<768){
            deplegueMenu2();
        }
        var posicion = $("#graficasPlagas").offset().top;
        $("html, body").animate({
            scrollTop: posicion
        }, 1000);
    });

    $(document).on("scroll", function(){
        $('.header').css('padding','0px');
        var desplazamientoActual = $(document).scrollTop();
        var controlArriba = $("#irarriba");
        if(desplazamientoActual > 100 && controlArriba.css("display") == "none"){
            controlArriba.fadeIn(500);
        }
        if(desplazamientoActual < 100 && controlArriba.css("display") == "block"){
            controlArriba.fadeOut(500);
        }
    });
    $("#irarriba a").on("click", function(e){

        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 700);
    });
});


function success(menj, head, texto) {
    $.toast({
        text: menj,
        textAlign: 'center',
        bgColor: '#59ad85',
        position: 'top-right'
    })
    setTimeout(function(){
        $.toast({
            heading: head,
            text: texto,
            showHideTransition: 'slide',
            position: 'top-right',
            icon: 'success'
        })
    },2000);
}
function error(title, ms, mensaje) {
    var myToast = $.toast({
        heading: title,
        text: ms,
        icon: 'info',
        position: 'top-right',
        hideAfter: false
    });

    // Update the toast after three seconds.
    window.setTimeout(function(){
        myToast.update({
            heading: 'Error',
            text: mensaje,
            icon: 'error',
            position: 'top-right',
            hideAfter: false,
            showHideTransition: 'slide',
        });
    }, 2000)
}