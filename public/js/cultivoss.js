// funcion que me trae los tipos de registro de cultivos
// disponibles para registar
function abrirModalRegistro() {
    // abrimos modal
    $('#modalRegistroCultivo').click();

    // enviamos parametro  de consulta de tipo cultivos registos
    var param ={'Funcion':'selectInformacionCultivosXRegistrar' };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var json = JSON.parse(data);
            var htm = "";
            for (var i = 0; i <json.length; i++ ) {
                htm = htm + '<option value="'+ json[i].id + '">' + json[i].nombre+  '</option>';
            }
            $('#tipo_cultivo').show();
            $('#tipo_cultivo').html(htm);
        }
    });
}
function inserRegisCultivo() {
    var id_usu = $('#cod_usu').val();
    var tipo = $('#tipo_cultivo').val();
    var param ={'Funcion':'verifRegistroCultivoUsuario', 'id_usu':id_usu, 'tipo':tipo };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            if (data != 0 ) {
                var title = 'Procesando!';
                var ms = ' Por favor espere...';
                var mensaje = 'Cultivo ya exite ! No puedes registrar dos veces el mismo cultuvo ! ';
                error(title,ms,mensaje);
            }else{
                var descripcion = $('#desRegisCultivo').val();
                var lotes = $('#cant_lotes').val();
                if (lotes <= 5) {
                    var param ={'Funcion':'insertRegistroCultivo', 'id_usu':id_usu, 'lotes':lotes, 'tipo':tipo, 'descripcion':descripcion };
                    $.ajax({
                        data: JSON.stringify(param),
                        type: "JSON",
                        url: 'ajax.php',
                        success: function (data) {
                            if (data != 0 ) {
                                var menj = 'Procesando! Por favor espere..';
                                var head = 'Registrado';
                                var texto = 'Se ha registrado un nuevo Cultivo';
                                success(menj,head,texto);
                                setTimeout(function(){
                                    $('#resert_RegistroCultivo').click();
                                    $('#cerrarModalRegistroCultivos').click();
                                    selectCultivosXusuario();

                                    $('#opt_despl_cultivosPlag').val(1);
                                    $('#opt_despl_cultivosEnfer').val(1);
                                    mostrarPlagCultivosUsuario();
                                    mostrarCultivosUsuario();
                                    funtion_inicio();
                                },1500);
                            }else{
                                var title = 'Procesando!';
                                var ms = ' Por favor espere...';
                                var mensaje = 'Algo ha salido mal... Vuelve a intentarlo ! ';
                                error(title,ms,mensaje);
                                $('#cerrarRegCultivos').click();
                            }
                        }
                    });                                    
                }else{
                    var title = 'Procesando!';
                    var ms = ' Por favor espere...';
                    var mensaje = 'Numero de Lotes superado... Máximo 5 ! ';
                    error(title,ms,mensaje);
                }
            }
        }
    });
}
// funcion table de detalles cultivos 
function selectCultivosXusuario() {
    var id_usu = parseInt(  $('#cod_usu').val() );
    var param ={'Funcion':'selectCultivosUsuario', 'id_usu':id_usu };
    $.ajax({
        data: JSON.stringify (param),
        type:"JSON",
        url: 'ajax.php',
        success:function(data){
            var json = JSON.parse(data);
            var cult = json[0].tipoC;
            var nombre = json[0].nombre;

            $('#cult_inicio').val(cult);
            $('#lote_inicio').val(1);
            $('#nomCult_inicio').val(nombre);

            var htm = "";
            for (var i = 0; i <json.length; i++ ) {
                htm = htm + '<tr>' +
                    '<td>'+ json[i].nombre + '</td>'+
                    '<td>'+ json[i].descripcion + '</td>' +
                    '<td>' +
                        '<a class="iconos" onclick="detalleCultivos('+json[i]['id']+')">' +
                            '<i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>' +
                        '</a>' +
                    '</td>' +
                    '<td>' +
                        '<a class="iconos" onclick="detalleDeleteCultivo('+json[i]['id']+')">' +
                            '<i class=\"fa fa-trash\" aria-hidden=\"true\"></i>' +
                        '</a>' +
                    '</td>' +
                '</tr>';
            }
            $('#cuerpoCultivos').html(htm);

        }
    });
}
// funcion de actualizar cultivos
function detalleCultivos(id) {
    // id de cultivo registrado, por lo que no es necesario saber cual es el del usuario
    var param ={'Funcion':'detalleXCultivosUsuario', 'id':id};
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var json = JSON.parse(data)

            $('#detalle_cultivo').click(); // click en boton para abrir modal  donde cargamos informacion para actualiar informacion de cultivo
            $('#detalle_tipo_cul').show();

            var n = json[0].nombre;
            if (n == 'Fresa') {
                $("#detalle_tipo_cul option[value="+ 1 +"]").attr("selected",true);
            }else{
                if (n == 'Durazno') {
                    $("#detalle_tipo_cul option[value="+ 2 +"]").attr("selected",true);
                }else{
                    if (n == 'Papa') {
                        $("#detalle_tipo_cul option[value="+ 3 +"]").attr("selected",true);
                    }
                }
            }
            //$('#detale_tipo_cul').val();
            $('#id_cultivo').val(json[0].id)
            $('#detalle_cant_lotes').val(json[0].lotes)
            $('#detalle_desRegisCultivo').val(json[0].descripcion)
        }
    });
}
function actualizarCultivoXusuario() {
    var id = $('#id_cultivo').val();
    var id_usu = $('#cod_usu').val();
    var tipo = $('#detalle_tipo_cul').val();
    var lotes = $('#detalle_cant_lotes').val();
    var descripcion = $('#detalle_desRegisCultivo').val();
     
    //if (tipo.length > 0  && descripcion.length > 0 ) {
        var param ={'Funcion':'actualizarCultivoXusuario', 'id':id, 'id_usu':id_usu, 'tipo':tipo, 'lotes':lotes, 'descripcion':descripcion   };
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: 'ajax.php',
            success: function (data) {
                if (data != 0 ) {
                    var menj = 'Procesando! Por favor espere...';
                    var head = 'Actualizado';
                    var texto = 'EL registro se ha actualizado';
                    success(menj,head,texto);
                    setTimeout(function(){
                        $('#noActcultivo').click();
                        $('#cerrarModalUpdateCultivos').click();
                        selectCultivosXusuario();

                        $('#opt_despl_cultivosPlag').val(1);
                        $('#opt_despl_cultivosEnfer').val(1);
                        mostrarPlagCultivosUsuario();
                        mostrarCultivosUsuario();
                    },1500);
                }else{
                    var title = 'Procesando!';
                    var ms = ' Por favor espere...';
                    var mensaje = 'Algo ha salido mal... Vuelve a intentarlo ! ';
                    error(title,ms,mensaje);
                    $('#noActUsuario').click();
                }
            }
        });
    /*}else{
        var title = 'Processing!';
        var ms = ' Please wait..';
        var mensaje = 'No pueden haber campos vacios... vuelve a intentarlo ! ';
        error(title,ms,mensaje);
        $('#noActUsuario').click();
    }*/
}
function detalleDeleteCultivo(id) {
    var param ={'Funcion':'detalleXCultivosUsuario', 'id':id};
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var json = JSON.parse(data)

            $('#ModalDeleteCultivo').click()

            $('#id_cultivo').val(json[0].id)
            $('#cultivoDetalle').html(json[0].nombre)
        }
    });
}
function deleteCultivoUsuario() {
    var id = $('#id_cultivo').val();
    var param ={'Funcion':'deleteCultivoXusuario', 'id':id };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            if (data != 0 ) {
                var menj = 'Procesando! Por favor espere...';
                var head = 'Eliminado';
                var texto = 'Cultivo ha sido Eliminado';
                success(menj,head,texto);
                setTimeout(function(){
                    $('#noDeleteCultivo').click();
                    selectCultivosXusuario();
                    $('#opt_despl_cultivosPlag').val(1);
                    $('#opt_despl_cultivosEnfer').val(1);
                    mostrarPlagCultivosUsuario();
                    mostrarCultivosUsuario();
                },1500);
            }else{
                var title = 'Procesando!';
                var ms = ' Por favor espere...';
                var mensaje = 'Algo ha salido mal... Vuelve a intentarlo ! ';
                error(title,ms,mensaje);
                $('#noDeleteCultivo').click();
            }
        }
    });
}