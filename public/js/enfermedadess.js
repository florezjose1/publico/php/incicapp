function busquedaEnf_cult_Xfecha(){
    $('#val_opt_busquedaEnf_Cult').val(1);
    var cult = $('#tip_CultivoBuscar').val();
    var lote = $('#lotes_CultivoBuscar').val();
    var usu = $('#cod_usu').val();
    selectEnfermedadesCultivos(cult,lote,usu);
}
function mostrarCultivosUsuario(){
	var o = $('#opt_despl_cultivosEnfer').val();
	if (o == 0 ) {
        $('#opt_inf_enf_x_cultivo').val(1);
        detalleEnfermedadesUsuario();


		$('#sigEnfCultivos').html('-')
		$('#opt_despl_cultivosEnfer').val(1);
		ejecutarMostrarCultivos();
	}else{
		$('#sigEnfCultivos').html('+')
		$('#opt_despl_cultivosEnfer').val(0);
        $('#sub-cultivos_enf').hide('slow');
	}
}
// funcion que me muestra cultivos registrados por usuario
function ejecutarMostrarCultivos() {
	var usu = $('#cod_usu').val();
    var param ={'Funcion':'cultivosXusuario', 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data) {
        	var json = JSON.parse(data);
            
            var a = json.length
            var h = json[a-1].id;

        	$('#cant_cult_X_usuario').val(h)
        	$('#nom_enf').html( (json[0].nombre).toUpperCase());

        	var chtml ="";
        	for (var i = 0; i < json.length; i++) {
        		var nombre = json[i].nombre;
        		var idC = parseInt(json[i].id);
                chtml = chtml + '<li>'+ 
                '<a onclick=\'detalleCultivoXusuario("'+idC+'","'+nombre+'","'+usu+ '")\' >'+ 
                    '<i class="fa fa-angle-double-right"></i> ' + nombre + 
                '</a>' + 
                '<ul id=\'loteXCult'+idC+'\'  style="display: none;">'+
                    
                '</ul>' + '</li>';
        	}
        	$('#sub-cultivos_enf').html(chtml);
        	$('#sub-cultivos_enf').show('slow');
        }
    });
}
function detalleCultivoXusuario(idC,nombre,usu) {
	var cc = $('#cant_cult_X_usuario').val(); // cantidad de cultivos
	var m = $('#i_ocultMenu_cult').val(); // input-funcion de ocultar menu de cultivos por usuario

	if (m == 0 ) {
        for(var i = 0; i <= cc; i++) {
            if (i == idC ) {
                h = '#loteXCult'+i;
				mostrarcultivosXusuario(idC,nombre,usu);
                //$(h).show('slow');
            }else{
                h = '#loteXCult'+i;
                $(h).hide('slow');
            }
        }
        $('#i_ocultMenu_cult').val(1);  
    }else{
        h = '#loteXCult'+idC;
        $(h).hide('slow');
        $('#i_ocultMenu_cult').val(0);
        //mostrarcultivosXusuario(idC,nombre,usu);
    }
}
// funcion con cantidad de lotes registrados por cultivos por usuario
function mostrarcultivosXusuario(idC,nombre,usu) {
	$('#nom_enf').html( (nombre).toUpperCase());
    var param ={'Funcion':'lotescultivosXusuario', 'usu':usu, 'id_cult':idC };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data) {

        	var json = JSON.parse(data);
        	var cant = json[0].lotes;
        	$('#cant_lotes_cult_X_usuario').val(cant)
        	var L_html ="";
        	for (var i = 1; i<= cant; i++) {
        		var lote = parseInt(i);
                L_html = L_html + '<li>'+ 
                '<a id="btnLote_'+lote+'_cultivo_'+idC+'_usu_'+usu+'" onclick=\'detalleXlotes("'+lote+'","'+idC+'","'+usu+ '")\' >'+ 
                    '<i class="fa fa-caret-right"></i> Lote' + i + 
                '</a>' + 
                '<ul id=\'id_contenCul'+idC+'Xlotes'+lote+'\'  style="display: none;">'+
                    '<li><a onclick=\'mostarGraficalinealEnf("'+idC+'","'+lote+'","'+usu+ '")\' >'+
                        '<i class="">-</i> Grafica Lineal</a>'+
                    '</li>'+
                    '<li><a onclick=\'mostrarGraficabarrasEnf("'+idC+'","'+lote+'","'+usu+ '")\' >'+
                        '<i class="">-</i>  Grafica Barras</a>'+
                    '</li>'+
                    '<li><a onclick=\'mostrarGraficatortaEnf("'+idC+'","'+lote+'","'+usu+ '")\' >'+
                        '<i class="">-</i>  Grafica Torta</a>'+
                    '</li>'+
                '</ul>' + '</li>';
        	}
        	$('#loteXCult'+idC).html(L_html);
        	$('#loteXCult'+idC).show('slow');
        }
    });
}
function mostarGraficalinealEnf(idC,lote,usu) {
    $('#graEnfermedad').val(0);
    selectEnfermedadesCultivos(idC,lote,usu);
}
function mostrarGraficabarrasEnf(idC,lote,usu) {
    $('#graEnfermedad').val(1);
    selectEnfermedadesCultivos(idC,lote,usu);
}
function mostrarGraficatortaEnf(idC,lote,usu) {
    $('#graEnfermedad').val(2);
    selectEnfermedadesCultivos(idC,lote,usu);
}
function detalleXlotes(lote,idC,usu){
	$('#i_ocultMenu_cult').val(0);
	var cl = $('#cant_lotes_cult_X_usuario').val(); // cantidad de lotes
	var a = $('#i_ocultMenu_lotes').val(); // input-funcion de ocultar menu de lotes por cultivo

	if (a == 0 ) {
        for(var i = 0; i <= cl; i++) {
            if (i == lote ) {
                n = '#id_contenCul'+idC+'Xlotes'+i;
                $(n).show('slow');
                mostarGraficalinealEnf(idC,lote,usu);
            }else{
                n = '#id_contenCul'+idC+'Xlotes'+i;
                $(n).hide('slow');
            }
        }
        $('#i_ocultMenu_lotes').val(1);     
    }else{
        n = '#id_contenCul'+idC+'Xlotes'+lote;
        $(n).hide('slow');
        $('#i_ocultMenu_lotes').val(0);
        //detalleXlotes(lote);
    }
}

function selectEnfermedadesCultivos(idC,lote,usu) {
    $('#numeroLoteEnfermedad').html('<b>Lote ' + lote+'</b>')
    var bus = $('#val_opt_busquedaEnf_Cult').val();
    if (bus != 0 ) {
        var fechaIN = $('#fechaInEnf').val();
        var fechaFi = $('#fechaFiEnf').val();
        var param ={'Funcion':'informacionEnfermedadesXcultivoLote', 'usu':usu, 'id_cult':idC, 'lote':lote, 'fecha1':fechaIN, 'fecha2':fechaFi, 'var':'1' };
    }else{
        var id =idC;
        //console.log("id", id);
        var lot =lote;
        //console.log("lot", lot);
        var usuario =usu;
        //console.log("usuario", usuario);
        var param ={'Funcion':'informacionEnfermedadesXcultivoLote', 'usu':usuario, 'id_cult':id, 'lote':lot, 'var':'2' };
        //console.log("param", param);
    }
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            //console.log("data", data);
            
            var json = JSON.parse(data);
            var nombre = json[0].cultivo;
            $('#nom_enf').html(nombre.toUpperCase())

                    var enf_1 = []; // variable para guardar array
                    var enf_2 = []; // variable para guardar array
                    var enf_3 = []; // variable para guardar array
                    var enf_4 = []; // variable para guardar array
                    var enf_5 = []; // variable para guardar array
                    var fecha = [];

                    var lim_1_for_E1 = 1;
                    var lim_2_for_E2 = 1;
                    var lim_3_for_E3 = 1;
                    var lim_4_for_E4 = 1;
                    var lim_5_for_E5 = 1;

                    var result1 = []; // array torta
                    var result2 = [];
                    var result3 = [];
                    var result4 = [];
                    var result5 = [];

                    var n = (json.length-1);

                    // for en cual vamos a acceder a cada una de las posiciones del array
                    for(var i = json.length-1; i>=0; i-- ) {
                        // variable que me trae enfermedad referente a la posicion del for
                        var enfermedad_1 = json[i].enf1;
                        var enfermedad1_1 = json[i].enf1;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_1 == 0 ) { 
                            proE1 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_1 != 9999 ) {
                                proE1 = enfermedad_1;
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_1 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_enf1 = 0;
                                    for(var n_1_E1 = i; n_1_E1 >= 0; n_1_E1-- ) {
                                        cont_1_enf1 += 1;
                                        var val_1_e1 = json[n_1_E1].enf1;
                                        if (val_1_e1 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e1 = 121212;
                                        }
                                    }
                                }

                                if (enfermedad1_1 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_enf1 = 0;
                                    for(var n_2_E1 = i; n_2_E1 < json.length; n_2_E1++ ) {
                                        cont_2_enf1 += 1;
                                        var val_2_e1 = json[n_2_E1].enf1;
                                        if (val_2_e1 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e1 = 323232;
                                        }
                                    }
                                }

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_enf1-1) + parseInt(cont_2_enf1-1);

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                if (val_1_e1 == 121212 || val_2_e1 == 323232) {
                                    var constt =898989;
                                }else{
                                    if (val_1_e1 == val_2_e1 ) {
                                        var constt = val_1_e1;
                                    }else{
                                        if (val_1_e1 > val_2_e1) {
                                            var constt = parseFloat(val_1_e1 - val_2_e1) / parseInt(espacios);
                                        }else{
                                            if (val_2_e1 == 9999 ) {
                                            }else{
                                                var constt = parseFloat(val_2_e1 - val_1_e1)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E1 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {
                                    for (var aE1 = lim_1_for_E1; aE1 <= reFor_E1; aE1++) {
                                        var rt_E1 = parseFloat(constt * aE1);
                                        lim_1_for_E1 += 1;
                                        if (lim_1_for_E1 > reFor_E1) {
                                            lim_1_for_E1 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E1 = 545454;
                                }
                                if (rt_E1 != 545454 ) {
                                    if (val_1_e1 == val_2_e1 ) {
                                        proE1 = val_1_e1;
                                    }else{
                                        if (val_1_e1 > val_2_e1 ) {
                                            proE1 = (parseFloat(val_1_e1 - rt_E1)).toFixed(1);
                                        }else{
                                            proE1 = (parseFloat(val_1_e1 + rt_E1)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE1 = 121212;
                                }

                            }
                        }
                        if (proE1 != 121212) {
                            var n1 = proE1.toFixed(1);
                            enf_1.push(parseFloat(n1));
                            result1.push([ json[i].fecha , parseFloat(proE1)  ]);
                        }




                        // variable que me trae enfermedad referente a la posicion del for
                        var enfermedad_2 = json[i].enf2;
                        var enfermedad2_2 = json[i].enf2;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_2 == 0 ) { 
                            proE2 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_2 != 9999 ) {
                                proE2 = enfermedad_2;
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_2 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_enf2 = 0;
                                    for(var n_1_E2 = i; n_1_E2 >= 0; n_1_E2-- ) {
                                        cont_1_enf2 += 1;
                                        var val_1_e2 = json[n_1_E2].enf2;
                                        if (val_1_e2 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e2 = 121212;
                                        }
                                    }
                                }

                                if (enfermedad2_2 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_enf2 = 0;
                                    for(var n_2_E2 = i; n_2_E2 < json.length; n_2_E2++ ) {
                                        cont_2_enf2 += 1;
                                        var val_2_e2 = json[n_2_E2].enf2;
                                        if (val_2_e2 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e2 = 323232;
                                        }
                                    }
                                }

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_enf2-1) + parseInt(cont_2_enf2-1);

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                if (val_1_e2 == 121212 || val_2_e2 == 323232) {
                                    var constt = 898989;
                                }else{
                                    if (val_1_e2 == val_2_e2 ) {
                                        var constt = val_1_e2;
                                    }else{
                                        if (val_1_e2 > val_2_e2) {
                                            var constt = parseFloat(val_1_e2 - val_2_e2) / parseInt(espacios);
                                        }else{
                                            if (val_2_e2 == 9999 ) {
                                                var constt =898989;
                                            }else{
                                                var constt = parseFloat(val_2_e2 - val_1_e2)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E2 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {

                                    for (var aE2 = lim_2_for_E2; aE2 <= reFor_E2; aE2++) {
                                        var rt_E2 = parseFloat(constt * aE2);
                                        lim_2_for_E2 += 1;
                                        if (lim_2_for_E2 > reFor_E2) {
                                            lim_2_for_E2 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E2 = 545454;
                                }
                                if (rt_E2 != 545454 ) {
                                    if (val_1_e2 == val_2_e2 ) {
                                        proE2 = val_1_e2;
                                    }else{
                                        if (val_1_e2 > val_2_e2 ) {
                                            proE2 = (parseFloat(val_1_e2 - rt_E2)).toFixed(1);
                                        }else{
                                            proE2 = (parseFloat(val_1_e2 + rt_E2)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE2 = 121212;
                                }

                            }
                        }
                        if (proE2 != 121212) {
                            var n2 = proE2.toFixed(1);
                            enf_2.push(parseFloat(n2));
                            result2.push([ json[i].fecha , parseFloat(proE2)] );
                        }



                        // variable que me trae enfermedad referente a la posicion del for
                        var enfermedad_3 = json[i].enf3;
                        var enfermedad3_3 = json[i].enf3;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_3 == 0 ) { 
                            proE3 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_3 != 9999 ) {
                                proE3 = enfermedad_3;
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_3 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_enf3 = 0;
                                    for(var n_1_E3 = i; n_1_E3 >= 0; n_1_E3-- ) {
                                        cont_1_enf3 += 1;
                                        var val_1_e3 = json[n_1_E3].enf3;
                                        if (val_1_e3 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e3 = 121212;
                                        }
                                    }
                                }

                                if (enfermedad3_3 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_enf3 = 0;
                                    for(var n_2_E3 = i; n_2_E3 < json.length; n_2_E3++ ) {
                                        cont_2_enf3 += 1;
                                        var val_2_e3 = json[n_2_E3].enf3;
                                        if (val_2_e3 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e3 = 323232;
                                        }
                                    }
                                }

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_enf3-1) + parseInt(cont_2_enf3-1);

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                if (val_1_e3 == 121212 || val_2_e3 == 323232) {
                                    var constt =898989;
                                }else{
                                    if (val_1_e3 == val_2_e3 ) {
                                        var constt = val_1_e3;
                                    }else{
                                        if (val_1_e3 > val_2_e3) {
                                            var constt = parseFloat(val_1_e3 - val_2_e3) / parseInt(espacios);
                                        }else{
                                            if (val_2_e3 == 9999 ) {
                                                var constt =898989;
                                            }else{
                                                var constt = parseFloat(val_2_e3 - val_1_e3)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E3 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {

                                    for (var aE3 = lim_3_for_E3; aE3 <= reFor_E3; aE3++) {
                                        var rt_E3 = parseFloat(constt * aE3);
                                        lim_3_for_E3 += 1;
                                        if (lim_3_for_E3 > reFor_E3) {
                                            lim_3_for_E3 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E3 = 545454;
                                }
                                if (rt_E3 != 545454 ) {
                                    if (val_1_e3 == val_2_e3 ) {
                                        proE3 = val_1_e3;
                                    }else{
                                        if (val_1_e3 > val_2_e3 ) {
                                            proE3 = (parseFloat(val_1_e3 - rt_E3)).toFixed(1);
                                        }else{
                                            proE3 = (parseFloat(val_1_e3 + rt_E3)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE3 = 121212;
                                }

                            }
                        }
                        if (proE3 != 121212) {
                            var n3 = proE3.toFixed(1);
                            enf_3.push(parseFloat(n3));
                            result3.push([ json[i].fecha , parseFloat(proE3)] );
                        }



                        // variable que me trae enfermedad referente a la posicion del for
                        var enfermedad_4 = json[i].enf4;
                        var enfermedad4_4 = json[i].enf4;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_4 == 0 ) { 
                            proE4 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_4 != 9999 ) {
                                proE4 = enfermedad_4;
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_4 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_enf4 = 0;
                                    for(var n_1_E4 = i; n_1_E4 >= 0; n_1_E4-- ) {
                                        cont_1_enf4 += 1;
                                        var val_1_e4 = json[n_1_E4].enf4;
                                        if (val_1_e4 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e4 = 121212;
                                        }
                                    }
                                }

                                if (enfermedad4_4 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_enf4 = 0;
                                    for(var n_2_E4 = i; n_2_E4 < json.length; n_2_E4++ ) {
                                        cont_2_enf4 += 1;
                                        var val_2_e4 = json[n_2_E4].enf4;
                                        if (val_2_e4 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e4 = 323232;
                                        }
                                    }
                                }

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_enf4-1) + parseInt(cont_2_enf4-1);

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                if (val_1_e4 == 121212 || val_2_e4 == 323232) {
                                    var constt =898989;
                                }else{
                                    if (val_1_e4 == val_2_e4 ) {
                                        var constt = val_1_e4;
                                    }else{
                                        if (val_1_e4 > val_2_e4) {
                                            var constt = parseFloat(val_1_e4 - val_2_e4) / parseInt(espacios);
                                        }else{
                                            if (val_2_e4 == 9999 ) {
                                                var constt =898989;
                                            }else{
                                                var constt = parseFloat(val_2_e4 - val_1_e4)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E4 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {

                                    for (var aE4 = lim_4_for_E4; aE4 <= reFor_E4; aE4++) {
                                        var rt_E4 = parseFloat(constt * aE4);
                                        lim_4_for_E4 += 1;
                                        if (lim_4_for_E4 > reFor_E4) {
                                            lim_4_for_E4 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E4 = 545454;
                                }
                                if (rt_E4 != 545454 ) {
                                    if (val_1_e4 == val_2_e4 ) {
                                        proE4 = val_1_e4;
                                    }else{
                                        if (val_1_e4 > val_2_e4 ) {
                                            proE4 = (parseFloat(val_1_e4 - rt_E4)).toFixed(1);
                                        }else{
                                            proE4 = (parseFloat(val_1_e4 + rt_E4)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE4 = 121212;
                                }

                            }
                        }
                        if (proE4 != 121212) {
                            var n4 = proE4.toFixed(1);
                            enf_4.push(parseFloat(n4));
                            result4.push([ json[i].fecha , parseFloat(proE4)] );
                        }



                        // variable que me trae enfermedad referente a la posicion del for
                        var enfermedad_5 = json[i].enf5;
                        var enfermedad5_5 = json[i].enf5;
                        // si es igual a cero, guardamos ese dato, por lo que sera un dato valido para graficar
                        if (enfermedad_5 == 0 ) { 
                            proE5 = 0;
                        }else{
                            // el 9999 es si el dato es nulo, por lo que, preguntamos si este dato no lo es!
                            // de ser así guardamos ese dato en una variable global referente a la enfermedad para al 
                            // final pasar a armar el array
                            if (enfermedad_5 != 9999 ) {
                                proE5 = enfermedad_5;
                            }else{
                                // y si el dato es nulo, procedemos a hacer un for anidado, en el cual, accederemos 
                                // a los datos de las posiciones anteriores y siguientes, para así calcular la tendencia
                                // que la grafica ha de tener y de esta manera, no tener colapsos en nuestra grafica.
                                if (enfermedad_5 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_1_enf5 = 0;
                                    for(var n_1_E5 = i; n_1_E5 >= 0; n_1_E5-- ) {
                                        cont_1_enf5 += 1;
                                        var val_1_e5 = json[n_1_E5].enf5;
                                        if (val_1_e5 != 9999 ) {
                                            break;
                                        }else{
                                            val_1_e5 = 121212;
                                        }
                                    }
                                }

                                if (enfermedad5_5 == 9999 ) {
                                    // contador, para saber posiciones que me recorre el for, hasta encontrar un dato valido
                                    var cont_2_enf5 = 0;
                                    for(var n_2_E5 = i; n_2_E5 < json.length; n_2_E5++ ) {
                                        cont_2_enf5 += 1;
                                        var val_2_e5 = json[n_2_E5].enf5;
                                        if (val_2_e5 != 9999 ) {
                                            break;
                                        }else{
                                            val_2_e5 = 323232;
                                        }
                                    }
                                }

                                // en el for, el contador siempre nos hace una posicion de mas, por lo que haremos
                                // una variable en la cual, esta nos guarde el dato verdadero.
                                var espacios = parseInt(cont_1_enf5-1) + parseInt(cont_2_enf5-1);

                                // si los valores son iguales, tomamos cualquiera de los dos!
                                if (val_1_e5 == 121212 || val_2_e5 == 323232) {
                                    var constt =898989;
                                }else{
                                    if (val_1_e5 == val_2_e5 ) {
                                        var constt = val_1_e5;
                                    }else{
                                        if (val_1_e5 > val_2_e5) {
                                            var constt = parseFloat(val_1_e5 - val_2_e5) / parseInt(espacios);
                                        }else{
                                            if (val_2_e5 == 9999 ) {
                                                var constt =898989;
                                            }else{
                                                var constt = parseFloat(val_2_e5 - val_1_e5)/parseInt(espacios);
                                            }
                                        }
                                    }                                    
                                }

                                // ya teniendo los datos, por los cuales la tendencia de mi grafica va a tener en 
                                // auto-incremente, o decremento, procedemos a hacerle, un for, para saber el
                                // numero real referente a las posiciones que este salte.
                                var reFor_E5 = parseInt(espacios - 1);

                                //if (reFor_E1 != 1) {}else{lim_1_for_E1 = 1;}
                                if (constt != 898989 ) {

                                    for (var aE5 = lim_5_for_E5; aE5 <= reFor_E5; aE5++) {
                                        var rt_E5 = parseFloat(constt * aE5);
                                        lim_5_for_E5 += 1;
                                        if (lim_5_for_E5 > reFor_E5) {
                                            lim_5_for_E5 = 1;
                                        }
                                        break;
                                    }
                                }else{
                                    rt_E5 = 545454;
                                }
                                if (rt_E5 != 545454 ) {
                                    if (val_1_e5 == val_2_e5 ) {
                                        proE5 = val_1_e5;
                                    }else{
                                        if (val_1_e5 > val_2_e5 ) {
                                            proE5 = (parseFloat(val_1_e5 - rt_E5)).toFixed(1);
                                        }else{
                                            proE5 = (parseFloat(val_1_e5 + rt_E5)).toFixed(1);
                                        }
                                    }                                    
                                }else{
                                    proE5 = 121212;
                                }

                            }
                        }
                        if (proE5 != 121212) {
                            var n5 = proE5.toFixed(1);
                            enf_5.push(parseFloat(n5));
                            result5.push([ json[i].fecha , parseFloat(proE5)] );
                        }

                        fecha.push(json[i].fecha);
                    }
                    

                    // variable para saber que tipo de grafica desea ver el usuario
                    // somo primera instancia, a este se le va a mostrar grafica lineal
                    var optioGrafic = $('#graEnfermedad').val();

                    if (optioGrafic == 0 ) {
                        linealEnf(idC, fecha, enf_1, enf_2, enf_3, enf_4, enf_5);
                    }else{
                        if (optioGrafic == 1 ) {
                            barrasEnf(idC, fecha, enf_1, enf_2, enf_3, enf_4, enf_5);
                        }else{
                            if (optioGrafic == 2 ) {
                                var id_cul = $('#id_CultConsulta').val();
                                var id_usu = $('#cod_usu').val();
                                //var fechaIn = $('#fechaIn').val();
                                //var fechaFi = $('#fechaFi').val();
                                //var a = $('#tipoBusqFechas').val();

                                //if (a == 0 ) {
                                    var param ={'Funcion':'infoEnfCultivosX_lotes_Torta', 'usu':usu, 'id_cult':idC, 'lote':lote };
                                //}else{
                                //    var param ={'Funcion':'infoEnfermedadesCultivosTorta', 'id_usu':id_usu, 'id_cul':id_cul, 'fechaIn':fechaIn, 'fechaFi':fechaFi, 'opt':'1' };
                                //}
                                $.ajax({
                                    data: JSON.stringify(param),
                                    type: "JSON",
                                    url: 'ajax.php',
                                    success: function (data) {
                                        var json = JSON.parse(data);
                                        var proEnf1 = json[0].enf1;
                                        var proEnf2 = json[0].enf2;
                                        var proEnf3 = json[0].enf3;
                                        var proEnf4 = json[0].enf4;
                                        var proEnf5 = json[0].enf5;

                                        tortaEnf(idC,proEnf1,proEnf2,proEnf3,proEnf4,proEnf5,result1,result2,result3,result4,result5 );
                                    }
                                });
                            }
                        }
                    }
        }
    });
}

function linealEnf(id, fecha, enf_1, enf_2, enf_3, enf_4, enf_5) {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionEnfermedadesUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var ne = JSON.parse(data);
            var w = ne.length;
            var n1,n2,n3,n4,n5;
            if (w == 2 ) {
                n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                $('#detaEnfermedaes').hide('slow');
                $('#detallesoptGraficaEnfer').hide('slow');
                $('#optGrafica').css('display','block');
                Highcharts.chart('optGrafica', {
                    chart: {
                        type: 'spline'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: fecha
                    },
                    yAxis: {
                        title: {
                            text: '% incidencias'
                        },
                        plotLines: [{
                            value: l1,
                            color: 'red',
                            width: 2,
                            label: {
                                text:  sig1,
                                //align: 'center',
                                style: {
                                    color: 'gray'
                                }
                            }
                        },{
                            value: l2,
                            color: 'red',
                            width: 2,
                            label: {
                                text: '<div> <br> ' + sig2 + ' </div>',
                                //align: 'center',
                                style: {
                                    color: 'gray'
                                }
                            }
                        }]
                    },
                    tooltip: {
                        crosshairs: true,
                        shared: true
                    },
                    plotOptions: {
                        spline: {
                            marker: {
                                radius: 4,
                                lineColor: '#666666',
                                lineWidth: 1
                            }
                        }
                    },
                    series: [{
                        name: n1 + ' ' + sig1 ,
                        data: enf_1
                    }, {
                        name: n2 + ' ' + sig2 ,
                        data: enf_2
                    }]
                });
            }else{
                if (w == 3 ) {
                    n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                    n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                    n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                    $('#detaEnfermedaes').hide('slow');
                    $('#detallesoptGraficaEnfer').hide('slow');
                    $('#optGrafica').css('display','block');
                    Highcharts.chart('optGrafica', {
                        chart: {
                            type: 'spline'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: fecha
                        },
                        yAxis: {
                            title: {
                                text: '% incidencias'
                            },
                            plotLines: [{
                                value: l1,
                                color: 'red',
                                width: 2,
                                label: {
                                    text:  sig1,
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            },{
                                value: l2,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: '<div> <br> ' + sig2 + ' </div>',
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            },{
                                value: l3,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: sig3,
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            }]
                        },
                        tooltip: {
                            crosshairs: true,
                            shared: true
                        },
                        plotOptions: {
                            spline: {
                                marker: {
                                    radius: 4,
                                    lineColor: '#666666',
                                    lineWidth: 1
                                }
                            }
                        },
                        series: [{
                            name: n1 + ' ' + sig1 ,
                            data: enf_1
                        }, {
                            name: n2 + ' ' + sig2 ,
                            data: enf_2
                        }, {
                            name: n3 + ' ' + sig3 ,
                            data: enf_3
                        }]
                    });
                }else{
                    if (w == 4 ) {
                        n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                        n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                        n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                        n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        $('#detaEnfermedaes').hide('slow');
                        $('#detallesoptGraficaEnfer').hide('slow');
                        $('#optGrafica').css('display','block');
                        Highcharts.chart('optGrafica', {
                            chart: {
                                type: 'spline'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                categories: fecha
                            },
                            yAxis: {
                                title: {
                                    text: '% incidencias'
                                },
                                plotLines: [{
                                    value: l1,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text:  sig1,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l2,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: '<div> <br> ' + sig2 + ' </div>',
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l3,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: sig3,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l4,
                                    color: 'red',
                                    width: 1,
                                    label: {
                                        text: sig4,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                }]
                            },
                            tooltip: {
                                crosshairs: true,
                                shared: true
                            },
                            plotOptions: {
                                spline: {
                                    marker: {
                                        radius: 4,
                                        lineColor: '#666666',
                                        lineWidth: 1
                                    }
                                }
                            },
                            series: [{
                                name: n1 + ' ' + sig1 ,
                                data: enf_1
                            }, {
                                name: n2 + ' ' + sig2 ,
                                data: enf_2
                            }, {
                                name: n3 + ' ' + sig3 ,
                                data: enf_3
                            }, {
                                name: n4 + ' ' + sig4 ,
                                data: enf_4
                            }]
                        });
                    }else{
                        if (w==5) {
                            n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                            n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                            n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                            n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                            n5 = ne[4].nombre; l5 = ne[4].limite; sig5 = ne[4].sigla;
                            $('#detaEnfermedaes').hide('slow');
                            $('#detallesoptGraficaEnfer').hide('slow');
                            $('#optGrafica').css('display','block');
                            Highcharts.chart('optGrafica', {
                                chart: {
                                    type: 'spline'
                                },
                                title: {
                                    text: ''
                                },
                                subtitle: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: fecha
                                },
                                yAxis: {
                                    title: {
                                        text: '% incidencias'
                                    },
                                    plotLines: [{
                                        value: l1,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig1,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l2,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div><br>' + sig2 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l3,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div><br>' + sig3 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l4,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig4,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l5,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig5,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    }]
                                },
                                tooltip: {
                                    crosshairs: true,
                                    shared: true
                                },
                                plotOptions: {
                                    spline: {
                                        marker: {
                                            radius: 4,
                                            lineColor: '#666666',
                                            lineWidth: 1
                                        }
                                    }
                                },
                                series: [{
                                    name: n1 + ' ' + sig1 ,
                                    data: enf_1
                                }, {
                                    name: n2 + ' ' + sig2 ,
                                    data: enf_2
                                }, {
                                    name: n3 + ' ' + sig3 ,
                                    data: enf_3
                                }, {
                                    name: n4 + ' ' + sig4 ,
                                    data: enf_4
                                },{
                                    name: n5 + ' ' + sig5 ,
                                    data: enf_5
                                }]
                            });
                        }
                    }
                }
            }
                
        }
    });
}
function barrasEnf(id, fecha, enf_1, enf_2, enf_3, enf_4, enf_5) {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionEnfermedadesUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var ne = JSON.parse(data);
            var w = ne.length;
            var n1,n2,n3,n4,n5;
            if (w == 2 ) {
                n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                $('#detaEnfermedaes').hide('slow');
                $('#detallesoptGraficaEnfer').hide('slow');
                $('#optGrafica').css('display','block');
                Highcharts.chart('optGrafica', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: fecha ,
                        crosshair: true
                    },
                    yAxis: {
                        title: {
                            text: '% incidencias'
                        },
                        plotLines: [{
                            value: l1,
                            color: 'red',
                            width: 2,
                            label: {
                                text:  sig1,
                                //align: 'center',
                                style: {
                                    color: 'gray'
                                }
                            }
                        },{
                            value: l2,
                            color: 'red',
                            width: 2,
                            label: {
                                text: '<div> <br> ' + sig2 + ' </div>',
                                //align: 'center',
                                style: {
                                    color: 'gray'
                                }
                            }
                        }]
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} % </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: n1 + ' ' + sig1 ,
                        data: enf_1

                    }, {
                        name: n2 + ' ' + sig2 ,
                        data: enf_2

                    }]
                });
            }else{
                if (w == 3 ) {
                    n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                    n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                    n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                    $('#detaEnfermedaes').hide('slow');
                    $('#detallesoptGraficaEnfer').hide('slow');
                    $('#optGrafica').css('display','block');
                    Highcharts.chart('optGrafica', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: fecha ,
                            crosshair: true
                        },
                        yAxis: {
                            title: {
                                text: '% incidencias'
                            },
                            plotLines: [{
                                value: l1,
                                color: 'red',
                                width: 2,
                                label: {
                                    text:  sig1,
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            },{
                                value: l2,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: '<div> <br> ' + sig2 + ' </div>',
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            },{
                                value: l3,
                                color: 'red',
                                width: 2,
                                label: {
                                    text: sig3,
                                    //align: 'center',
                                    style: {
                                        color: 'gray'
                                    }
                                }
                            }]
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} % </b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: n1 + ' ' + sig1 ,
                            data: enf_1

                        }, {
                            name: n2 + ' ' + sig2 ,
                            data: enf_2

                        }, {
                            name: n3 + ' ' + sig3 ,
                            data: enf_3
                        }]
                    });
                }else{
                    if (w == 4 ) {
                        n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                        n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                        n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                        n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        $('#detaEnfermedaes').hide('slow');
                        $('#detallesoptGraficaEnfer').hide('slow');
                        $('#optGrafica').css('display','block');
                        Highcharts.chart('optGrafica', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                categories: fecha ,
                                crosshair: true
                            },
                            yAxis: {
                                title: {
                                    text: '% incidencias'
                                },
                                plotLines: [{
                                    value: l1,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text:  sig1,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l2,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: '<div> <br> ' + sig2 + ' </div>',
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l3,
                                    color: 'red',
                                    width: 2,
                                    label: {
                                        text: sig3,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                },{
                                    value: l4,
                                    color: 'red',
                                    width: 1,
                                    label: {
                                        text: sig4,
                                        //align: 'center',
                                        style: {
                                            color: 'gray'
                                        }
                                    }
                                }]
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} % </b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                            series: [{
                                name: n1 + ' ' + sig1 ,
                                data: enf_1

                            }, {
                                name: n2 + ' ' + sig2 ,
                                data: enf_2

                            }, {
                                name: n3 + ' ' + sig3 ,
                                data: enf_3

                            }, {
                                name: n4 + ' ' + sig4 ,
                                data: enf_4

                            }]
                        });
                    }else{
                        if (w==5) {
                            n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                            n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                            n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                            n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                            n5 = ne[4].nombre; l5 = ne[4].limite; sig5 = ne[4].sigla;

                            $('#detaEnfermedaes').hide('slow');
                            $('#detallesoptGraficaEnfer').hide('slow');
                            $('#optGrafica').css('display','block');
                            Highcharts.chart('optGrafica', {
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: ''
                                },
                                subtitle: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: fecha ,
                                    crosshair: true
                                },
                                yAxis: {
                                    title: {
                                        text: '% incidencias'
                                    },
                                    plotLines: [{
                                        value: l1,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig1,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l2,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div><br>' + sig2 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l3,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: '<div><br>' + sig3 + ' </div>',
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l4,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig4,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    },{
                                        value: l5,
                                        color: 'red',
                                        width: 2,
                                        label: {
                                            text: sig5,
                                            //align: 'center',
                                            style: {
                                                color: 'gray'
                                            }
                                        }
                                    }]
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    '<td style="padding:0"><b>{point.y:.1f} % </b></td></tr>',
                                    footerFormat: '</table>',
                                    shared: true,
                                    useHTML: true
                                },
                                plotOptions: {
                                    column: {
                                        pointPadding: 0.2,
                                        borderWidth: 0
                                    }
                                },
                                series: [{
                                    name: n1 + ' ' + sig1 ,
                                    data: enf_1
                                }, {
                                    name: n2 + ' ' + sig2 ,
                                    data: enf_2
                                }, {
                                    name: n3 + ' ' + sig3 ,
                                    data: enf_3
                                }, {
                                    name: n4 + ' ' + sig4 ,
                                    data: enf_4
                                },{
                                    name: n5 + ' ' + sig5 ,
                                    data: enf_5
                                }]
                            });
                        }
                    }
                }
            }
        }
    });
}
function tortaEnf(id,proEnf1,proEnf2,proEnf3,proEnf4,proEnf5,result1,result2,result3,result4,result5 ) {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionEnfermedadesUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var ne = JSON.parse(data);
            var w = ne.length;
            if (w == 2 ) {
                n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                $('#detaEnfermedaes').hide('slow');
                $('#detallesoptGraficaEnfer').hide('slow');
                $('#optGrafica').css('display','block');
                // Create the chart
                Highcharts.chart('optGrafica', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}: {point.y:.1f}%'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                    },
                    series: [{
                        name: 'Enfermedades',
                        colorByPoint: true,
                        data: [{
                            name: n1 + ' ' + sig1,
                            y: proEnf1,
                            drilldown: 'enf_1'
                        }, {
                            name: n2 + ' ' + sig2,
                            y: proEnf2,
                            drilldown: 'enf_2'
                        }]
                    }],
                    drilldown: {
                        series: [{
                            name: n1 + ' ' + sig1,
                            id: 'enf_1',
                            data: result1
                        }, {
                            name: n2 + ' ' + sig2,
                            id: 'enf_2',
                            data: result2
                        }]
                    }
                });
            }else{
                if (w == 3 ) {
                    n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                    n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                    n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                    $('#detaEnfermedaes').hide('slow');
                    $('#detallesoptGraficaEnfer').hide('slow');
                    $('#optGrafica').css('display','block');
                    // Create the chart
                    Highcharts.chart('optGrafica', {
                        chart: {
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y:.1f}%'
                                }
                            }
                        },

                        tooltip: {
                            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                        },
                        series: [{
                            name: 'Enfermedades',
                            colorByPoint: true,
                            data: [{
                                name: n1 + ' ' + sig1,
                                y: proEnf1,
                                drilldown: 'enf_1'
                            }, {
                                name: n2 + ' ' + sig2,
                                y: proEnf2,
                                drilldown: 'enf_2'
                            }, {
                                name: n3 + ' ' + sig3,
                                y: proEnf3,
                                drilldown: 'enf_3'
                            }]
                        }],
                        drilldown: {
                            series: [{
                                name: n1 + ' ' + sig1,
                                id: 'enf_1',
                                data: result1
                            }, {
                                name: n2 + ' ' + sig2,
                                id: 'enf_2',
                                data: result2
                            }, {
                                name: n3 + ' ' + sig3,
                                id: 'enf_3',
                                data: result3
                            }]
                        }
                    });
                }else{
                    if (w == 4 ) {
                        n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                        n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                        n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                        n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        $('#detaEnfermedaes').hide('slow');
                        $('#detallesoptGraficaEnfer').hide('slow');
                        $('#optGrafica').css('display','block');
                        // Create the chart
                        Highcharts.chart('optGrafica', {
                            chart: {
                                type: 'pie'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            plotOptions: {
                                series: {
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.name}: {point.y:.1f}%'
                                    }
                                }
                            },

                            tooltip: {
                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                            },
                            series: [{
                                name: 'Enfermedades',
                                colorByPoint: true,
                                data: [{
                                    name: n1 + ' ' + sig1,
                                    y: proEnf1,
                                    drilldown: 'enf_1'
                                }, {
                                    name: n2 + ' ' + sig2,
                                    y: proEnf2,
                                    drilldown: 'enf_2'
                                }, {
                                    name: n3 + ' ' + sig3,
                                    y: proEnf3,
                                    drilldown: 'enf_3'
                                }, {
                                    name: n4 + ' ' + sig4,
                                    y: proEnf4,
                                    drilldown: 'enf_4'
                                }]
                            }],
                            drilldown: {
                                series: [{
                                    name: n1 + ' ' + sig1,
                                    id: 'enf_1',
                                    data: result1
                                }, {
                                    name: n2 + ' ' + sig2,
                                    id: 'enf_2',
                                    data: result2
                                }, {
                                    name: n3 + ' ' + sig3,
                                    id: 'enf_3',
                                    data: result3
                                }, {
                                    name: n4 + ' ' + sig4,
                                    id: 'enf_4',
                                    data: result4
                                }]
                            }
                        });
                    }else{
                        n1 = ne[0].nombre; l1 = ne[0].limite; sig1 = ne[0].sigla;
                        n2 = ne[1].nombre; l2 = ne[1].limite; sig2 = ne[1].sigla;
                        n3 = ne[2].nombre; l3 = ne[2].limite; sig3 = ne[2].sigla;
                        n4 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        n5 = ne[3].nombre; l4 = ne[3].limite; sig4 = ne[3].sigla;
                        $('#detaEnfermedaes').hide('slow');
                        $('#optGrafica').css('display','block');
                        // Create the chart
                        Highcharts.chart('optGrafica', {
                            chart: {
                                type: 'pie'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            plotOptions: {
                                series: {
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.name}: {point.y:.1f}%'
                                    }
                                }
                            },

                            tooltip: {
                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                            },
                            series: [{
                                name: 'Enfermedades',
                                colorByPoint: true,
                                data: [{
                                    name: n1 + ' ' + sig1,
                                    y: proEnf1,
                                    drilldown: 'enf_1'
                                }, {
                                    name: n2 + ' ' + sig2,
                                    y: proEnf2,
                                    drilldown: 'enf_2'
                                }, {
                                    name: n3 + ' ' + sig3,
                                    y: proEnf3,
                                    drilldown: 'enf_3'
                                }, {
                                    name: n4 + ' ' + sig4,
                                    y: proEnf4,
                                    drilldown: 'enf_4'
                                }, {
                                    name: n5 + ' ' + sig5,
                                    y: proEnf5,
                                    drilldown: 'enf_5'
                                }]
                            }],
                            drilldown: {
                                series: [{
                                    name: n1 + ' ' + sig1,
                                    id: 'enf_1',
                                    data: result1
                                }, {
                                    name: n2 + ' ' + sig2,
                                    id: 'enf_2',
                                    data: result2
                                }, {
                                    name: n3 + ' ' + sig3,
                                    id: 'enf_3',
                                    data: result3
                                }, {
                                    name: n4 + ' ' + sig4,
                                    id: 'enf_4',
                                    data: result4
                                }, {
                                    name: n5 + ' ' + sig5,
                                    id: 'enf_5',
                                    data: result5
                                }]
                            }
                        });
                    }
                }
            }
        }
    });
}



function detalleEnfermedadesUsuario(){
    var a = $('#opt_inf_enf_x_cultivo').val();
    if (a == 0 ) {
    	$('#opt_despl_cultivosEnfer').val(1); // damos valor a menu para que este se cierre al intentar abrir otro
    	mostrarCultivosUsuario(); //ejecutamos funcion para que me cierre menú abierto
    	$('#sub-cultivos_enf').hide('slow');
        
        $('#desplegar1_Enf').html('-')
        $('#opt_inf_enf_x_cultivo').val(1);
        detalleInfEnfCultivosUsu();

    }else{
        $('#desplegar1_Enf').html('+')
        $('#sub_detalles').hide('slow');
        $('#opt_inf_enf_x_cultivo').val(0);
    }
}
function detalleInfEnfCultivosUsu() {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'cultivosUsuario', 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var datos = JSON.parse(data);
            
            var dd = "";
            for(var i = 0; i < datos.length; i++) {
                var nom = datos[i].nombre;
                dd = dd + '<li>'+ 
                    '<a onclick=\'detalleXenfermedad_us("'+datos[i].id+'","'+nom+ '")\' >'+ 
                        '<i class="fa fa-caret-right"></i> ' + datos[i].nombre + 
                    '</a>' + 
                '</li>';
            }
            $('#sub_detalles').html(dd); // lista de opciones de cultivos
            $('#sub_detalles').show('slow'); // abrimos contenedor de lista
        }
    });
}
function detalleXenfermedad_us(id,nom) {
    var usu = $('#cod_usu').val();
    var cul = id;
    var param ={'Funcion':'InformacionEnfermedadesUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data) {
            var json = JSON.parse(data)
            var n = "";
            for(var a = 0; a < json.length; a++) {
                n = n + '<tr>'+
                    '<td>'+json[a].nombre+'</td>'+
                    '<td>'+json[a].sigla+'</td>'+
                    '<td>'+json[a].limite+'</td>'+
                    '<td>' +
                        '<a class="iconos" onclick="DetallesupdateInformacionEnfermedades('+json[a]['id_enfe']+','+cul+')">' +
                            '<i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>' +
                        '</a>' +
                    '</td>' +
                    '<td>' +
                        '<a class="iconos" onclick="InformacionEnfermedades('+json[a]['id_enfe']+','+cul+')">' +
                            '<i class=\"fa fa-low-vision\" aria-hidden=\"true\"></i>' +
                        '</a>' +
                    '</td>' +
                '</tr>'    
            }
            $('#optGrafica').hide('slow');
            $('#detallesoptGraficaEnfer').hide('slow');
            $('#nomEnfDetalles').html(nom);
            $('#nom_cult').val(nom);
            $('#cuerpoEnfermedad').html(n);
            $('#detaEnfermedaes').show('slow');
        }
    });
}
function DetallesupdateInformacionEnfermedades(id,cul) {
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionEnfermedadesxUser', 'id':id, 'cul':cul, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            $('#updateInformacionEnfermedades').click();
            var json = JSON.parse(data)
            var title = json[0].nombre;
            var sigla = json[0].sigla;
            var limite = json[0].limite;
            var cod = json[0].id_enfe;
            var id_cult = json[0].id_cult;
            $('#cod_enf').val(cod)
            $('#cod_cult').val(id_cult)
            $('#nomEnfUpdate').val(title);
            $('#sigEnfUpdate').val(sigla);
            $('#limEnfUpdate').val(limite);
        }
    });
}
function UpdateDetallesEnfermedad() {
    var usu = $('#cod_usu').val();
    var cod = $('#cod_enf').val();
    var cul = $('#cod_cult').val();
    var nombre = $('#nomEnfUpdate').val();
    var sigla = $('#sigEnfUpdate').val();
    var limite = $('#limEnfUpdate').val();
    var param ={'Funcion':'UpdateDetallesEnfermedad', 'usu':usu, 'cod':cod, 'cul':cul, 'nombre':nombre, 'sigla':sigla, 'limite':limite };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            if (data != 0 ) {
                var menj = 'Processing! Please wait..';
                var head = 'Actualizado';
                var texto = 'Se ha Actualizado un nuevo registro';
                success(menj,head,texto);
                setTimeout(function(){
                    $('#cerrarUpdateEnf').click();
                    var n = $('#nom_cult').val();
                    detalleXenfermedad_us(cul,n)
                },1500);
            }else{
                var title = 'Procesando!';
                var ms = ' Por favor espere..';
                var mensaje = 'error!!! Algo ha salido mal... ';
                error(title,ms,mensaje); 
            }
        }
    });
}
function InformacionEnfermedades(id,cul) {
    var param ={'Funcion':'InformacionEnfermedades', 'id':id, 'cul':cul };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            var json = JSON.parse(data)
            var title = json[0].nombre;
            var descripcion = json[0].descripcion;
            var sinSing = json[0].sint_sig;
            $('#detallesInformacion').click();
            $('#nameDetalleEnf').html(title);
            $('#descripcionEnfermedad').html(descripcion);
            $('#sinSigEnfermedad').html(sinSing);
        }
    });
}

function abrirModalRegistroEnfermedades() {
    $('#regisControl').click();
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'cultivosUsuario', 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var json = JSON.parse(data);
            var h = "";
            for(var i = 0; i < json.length; i++) {
                h = h + '<option value="' + json[i].id +'">' + json[i].nombre + '</option>';
            }
            $('#tip_Cultivo').html('<option value="0"></option>' + h);
            $('#tip_Cultivo').show();
            $('#tip_enfermedad').show();
        }
    });
}
function selectEnfermedad() {
    var id = $('#tip_Cultivo').val();
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'InformacionEnfermedadesUser', 'id':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function (data) {
            var json = JSON.parse(data);
            var h = "";
            for(var i = 0; i < json.length; i++) {
                h = h + '<option value="' + json[i].id_enfe +'">' + json[i].nombre + '</option>';
            }
            $('#tip_enfermedad').html(h);

            var param ={'Funcion':'lotescultivosXusuario', 'id_cult':id, 'usu':usu };
            $.ajax({
                data: JSON.stringify(param),
                type: "JSON",
                url: 'ajax.php',
                success: function(data){
                    var json = JSON.parse(data);
                    var lhtm = "";
                    for(var i = 1; i <= json[0].lotes; i++) {
                        lhtm = lhtm + '<option value="' + i +'">Lote ' + i + '</option>';
                    }
                    $('#tip_lote').html(lhtm);
                }
            });   
            
        }
    });
}
function insertRegPlanEnfermas() {
    var id = $('#tip_Cultivo').val();
    var id1 = $('#tip_enfermedad').val();
    var planSem = $('#plantasSem').val();
    var planEnf = $('#plantasEnf').val();
    var fecha = $('#fechaR').val();
    var lote = $('#tip_lote').val();
    var usu = $('#cod_usu').val();
    var f = new Date();
    var dia =  parseInt( f.getDate() );
    var mes =  parseInt( (f.getMonth() +1) );
    var anno =  parseInt( f.getFullYear() );

    if (dia < 10 ) {
        var dia = '0'+f.getDate();
    }
    if (mes < 10 ) {
        var mes = '0'+(f.getMonth() +1);
    }

    var n = f.getFullYear() + "-" + mes + "-" + dia ;

    ////////////////
    var dateDescomp = fecha.split("-");
    var mesComp = dateDescomp[1];
    /////////////////

            console.log("id", id);
            console.log("planEnf", planEnf);
            console.log("planSem", planSem);

    if (mesComp == mes ) {
        if (planSem.length>0 && planEnf.length>0 && id.length>0 ) {
            var param ={'Funcion':'insertEnfermedadCultivoLotes', 'usu':usu, 'id':id, 'id1':id1, 'lote':lote, 'fecha':fecha, 'variable':'1' };
            $.ajax({
                data: JSON.stringify(param),
                type: "JSON",
                url: 'ajax.php',
                success: function (data) {
                    if (data != 0 ) {
                       var param ={'Funcion':'insertEnfermedadCultivoLotes', 'usu':usu, 'id':id, 'id1':id1, 'planSem':planSem, 'planEnf':planEnf, 'lote':lote, 'fecha':fecha, 'variable':'2' };
                    }else{
                        var param ={'Funcion':'insertEnfermedadCultivoLotes', 'usu':usu, 'id':id, 'id1':id1, 'planSem':planSem, 'planEnf':planEnf, 'lote':lote, 'fecha':fecha, 'variable':'3' };
                    }
                    $.ajax({
                        data: JSON.stringify(param),
                        type: "JSON",
                        url: 'ajax.php',
                        success: function (data) {
                            if (data != 0 ) {
                                var menj = 'Procesando! Por Favor Espere...';
                                var head = 'Registrado';
                                var texto = 'Se ha insertado un nuevo registro';
                                success(menj,head,texto);
                                setTimeout(function(){
                                    $('#resertInserCultivoXlotes').click();
                                    $('#cerrarRegCultivos').click();
                                    var usu = $('#cod_usu').val();
                                    selectEnfermedadesCultivos(id,lote,usu);

                                    var usu = $('#cod_usu').val();
                                    var param ={'Funcion':'InformacionCult_LoteEnfxUser', 'id':id1, 'cul':id, 'usu':usu };
                                    $.ajax({
                                        data: JSON.stringify(param),
                                        type: "JSON",
                                        url: 'ajax.php',
                                        success: function(data){
                                            var json = JSON.parse(data)
                                            var limite = json[0].limite;

                                            var alert = parseFloat(planEnf/planSem)*100;

                                            setTimeout(function(){
                                                if (alert > limite) {
                                                    $.toast({
                                                        text: "Debes tomar medidas curativas ! consulte al asesor técnico / agrónomo", // Text that is to be shown in the toast
                                                        heading: 'Alerta !', // Optional heading to be shown on the toast
                                                        icon: 'error', // Type of toast icon
                                                        showHideTransition: 'slide', // fade, slide or plain
                                                        allowToastClose: false, // Boolean value true or false
                                                        hideAfter: 20000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                                                        stack: false, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                                                        position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                                                        
                                                        
                                                        
                                                        textAlign: 'left',  // Text alignment i.e. left, right or center
                                                        loader: true,  // Whether to show loader or not. True by default
                                                        loaderBg: '#20c600',  // Background color of the toast loader
                                                        beforeShow: function () {}, // will be triggered before the toast is shown
                                                        afterShown: function () {}, // will be triggered after the toat has been shown
                                                        beforeHide: function () {}, // will be triggered before the toast gets hidden
                                                        afterHidden: function () {}  // will be triggered after the toast has been hidden
                                                    });
                                                }else{
                                                    $.toast({
                                                        text: "Debes tomar medidas preventivas, como labores culturales ! ", // Text that is to be shown in the toast
                                                        heading: 'Alerta !', // Optional heading to be shown on the toast
                                                        icon: 'warning', // Type of toast icon
                                                        showHideTransition: 'slide', // fade, slide or plain
                                                        allowToastClose: false, // Boolean value true or false
                                                        hideAfter: 20000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                                                        stack: false, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                                                        position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                                                        
                                                        
                                                        
                                                        textAlign: 'left',  // Text alignment i.e. left, right or center
                                                        loader: true,  // Whether to show loader or not. True by default
                                                        loaderBg: '#20c600',  // Background color of the toast loader
                                                        beforeShow: function () {}, // will be triggered before the toast is shown
                                                        afterShown: function () {}, // will be triggered after the toat has been shown
                                                        beforeHide: function () {}, // will be triggered before the toast gets hidden
                                                        afterHidden: function () {}  // will be triggered after the toast has been hidden
                                                    });
                                                }                                            
                                            },3000);
                                        }
                                    });
                                },1500);
                            }else{
                                var title = 'Processing!';
                                var ms = ' Please wait..';
                                var mensaje = 'Algo ha salido mal... Vuelve a intentarlo ! ';
                                error(title,ms,mensaje);
                            }
                        }
                    });
                }
            });
        }else{
            var title = 'Procesando!';
            var ms = ' Por favor espere..';
            var mensaje = 'No pueden haber campos vacios! ';
            error(title,ms,mensaje);     
        }
     }else{
        var title = 'Procesando!';
        var ms = ' Por favor espere..';
        var mensaje = 'La fecha no es correcta ! ';
        error(title,ms,mensaje); 
    }   
}

function buscarLotes() {
    var id = $('#tip_CultivoBuscar').val();
    var usu = $('#cod_usu').val();
    var param ={'Funcion':'lotescultivosXusuario', 'id_cult':id, 'usu':usu };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data){
            var json = JSON.parse(data);
            var lhtm = "";
            for(var i = 1; i <= json[0].lotes; i++) {
                lhtm = lhtm + '<option value="' + i +'">Lote ' + i + '</option>';
            }
            $('#lotes_CultivoBuscar').html(lhtm);
            $('#lotes_CultivoBuscar').show();
        }
    });   
}