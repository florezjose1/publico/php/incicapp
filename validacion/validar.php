<?php
    require_once '../app/models/conexion.php';
    $modelo     = new Connnection();
    $connect    = $modelo->get_connection();
    $correo     = htmlentities(addslashes($_POST['correo']));
    $clave      = htmlentities(addslashes($_POST['clave']));
    $passS      = MD5($clave);
    try{
        $stm    = $connect->prepare("SELECT * FROM usuarios WHERE correo = :correo");
        $stm->execute(array(":correo"=>$correo));
        $row    = $stm->fetch(PDO::FETCH_ASSOC);
        $count  = $stm->rowCount();
        if($row['clave'] == $passS){
            if($count != 0){
                session_start();
                $_SESSION['usuarios'] = $row['id_usu'];
                header('location: ../app/usuario.php');
            }else{
                echo '<div class="alert alert-dismissible alert-danger"><p><strong>ERROR!</strong> El email no existe.</p></div>';
                header('location: ../index.php');
            }
        }else{
            echo '<div class="alert alert-dismissible alert-danger"><p><strong>ERROR!</strong> Contraseña incorrecta.</p></div>';
            header('location: ../index.php');

        }
    }catch(PDOException $e){
        die("Error de conexión: ".$e->getMessage());
    }