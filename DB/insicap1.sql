-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-05-2017 a las 17:45:04
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `insicap1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_enfermedades`
--

CREATE TABLE `control_enfermedades` (
  `id_registro` int(60) NOT NULL,
  `fecha_r` date DEFAULT NULL,
  `tipo_cultivo` int(20) DEFAULT NULL,
  `enf_1_L1` float DEFAULT '9999',
  `enf_2_L1` float DEFAULT '9999',
  `enf_3_L1` float DEFAULT '9999',
  `enf_4_L1` float DEFAULT '9999',
  `enf_5_L1` float DEFAULT '9999',
  `enf_1_L2` float DEFAULT '9999',
  `enf_2_L2` float DEFAULT '9999',
  `enf_3_L2` float DEFAULT '9999',
  `enf_4_L2` float DEFAULT '9999',
  `enf_5_L2` float DEFAULT '9999',
  `enf_1_L3` float DEFAULT '9999',
  `enf_2_L3` float DEFAULT '9999',
  `enf_3_L3` float DEFAULT '9999',
  `enf_4_L3` float DEFAULT '9999',
  `enf_5_L3` float DEFAULT '9999',
  `enf_1_L4` float DEFAULT '9999',
  `enf_2_L4` float DEFAULT '9999',
  `enf_3_L4` float DEFAULT '9999',
  `enf_4_L4` float DEFAULT '9999',
  `enf_5_L4` float DEFAULT '9999',
  `enf_1_L5` float DEFAULT NULL,
  `enf_2_L5` float DEFAULT NULL,
  `enf_3_L5` float DEFAULT NULL,
  `enf_4_L5` float DEFAULT NULL,
  `enf_5_L5` float DEFAULT NULL,
  `id_usu` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `control_enfermedades`
--

INSERT INTO `control_enfermedades` (`id_registro`, `fecha_r`, `tipo_cultivo`, `enf_1_L1`, `enf_2_L1`, `enf_3_L1`, `enf_4_L1`, `enf_5_L1`, `enf_1_L2`, `enf_2_L2`, `enf_3_L2`, `enf_4_L2`, `enf_5_L2`, `enf_1_L3`, `enf_2_L3`, `enf_3_L3`, `enf_4_L3`, `enf_5_L3`, `enf_1_L4`, `enf_2_L4`, `enf_3_L4`, `enf_4_L4`, `enf_5_L4`, `enf_1_L5`, `enf_2_L5`, `enf_3_L5`, `enf_4_L5`, `enf_5_L5`, `id_usu`) VALUES
(1, '2017-05-13', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(2, '2017-05-13', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(3, '2017-05-13', 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(4, '2017-05-13', 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(5, '2017-05-13', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_plagas`
--

CREATE TABLE `control_plagas` (
  `id_registro` int(60) NOT NULL,
  `fecha_r` date DEFAULT NULL,
  `tipo_cultivo` int(20) DEFAULT NULL,
  `plag_1_L1` float DEFAULT '9999',
  `plag_2_L1` float DEFAULT '9999',
  `plag_3_L1` float DEFAULT '9999',
  `plag_4_L1` float DEFAULT '9999',
  `plag_5_L1` float DEFAULT '9999',
  `plag_1_L2` float DEFAULT '9999',
  `plag_2_L2` float DEFAULT '9999',
  `plag_3_L2` float DEFAULT '9999',
  `plag_4_L2` float DEFAULT '9999',
  `plag_5_L2` float DEFAULT '9999',
  `plag_1_L3` float DEFAULT '9999',
  `plag_2_L3` float DEFAULT '9999',
  `plag_3_L3` float DEFAULT '9999',
  `plag_4_L3` float DEFAULT '9999',
  `plag_5_L3` float DEFAULT '9999',
  `plag_1_L4` float DEFAULT '9999',
  `plag_2_L4` float DEFAULT '9999',
  `plag_3_L4` float DEFAULT '9999',
  `plag_4_L4` float DEFAULT '9999',
  `plag_5_L4` float DEFAULT '9999',
  `plag_1_L5` float DEFAULT '9999',
  `plag_2_L5` float DEFAULT '9999',
  `plag_3_L5` float DEFAULT '9999',
  `plag_4_L5` float DEFAULT '9999',
  `plag_5_L5` float DEFAULT '9999',
  `id_usu` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `control_plagas`
--

INSERT INTO `control_plagas` (`id_registro`, `fecha_r`, `tipo_cultivo`, `plag_1_L1`, `plag_2_L1`, `plag_3_L1`, `plag_4_L1`, `plag_5_L1`, `plag_1_L2`, `plag_2_L2`, `plag_3_L2`, `plag_4_L2`, `plag_5_L2`, `plag_1_L3`, `plag_2_L3`, `plag_3_L3`, `plag_4_L3`, `plag_5_L3`, `plag_1_L4`, `plag_2_L4`, `plag_3_L4`, `plag_4_L4`, `plag_5_L4`, `plag_1_L5`, `plag_2_L5`, `plag_3_L5`, `plag_4_L5`, `plag_5_L5`, `id_usu`) VALUES
(1, '2017-05-13', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(2, '2017-05-13', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(3, '2017-05-13', 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(4, '2017-05-13', 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(5, '2017-05-13', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermedades`
--

CREATE TABLE `enfermedades` (
  `id_enfe` int(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `sigla` varchar(10) NOT NULL,
  `limite_porcentaje` int(10) NOT NULL,
  `id_cult` int(10) NOT NULL,
  `descripcion` varchar(10000) NOT NULL,
  `sint_sig` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `enfermedades`
--

INSERT INTO `enfermedades` (`id_enfe`, `nombre`, `sigla`, `limite_porcentaje`, `id_cult`, `descripcion`, `sint_sig`) VALUES
(1, 'Botrytis cinérea (Moho gris)', '(BO)', 20, 1, 'Esta enfermedad es conocida como pudrición de la fresa, o moho gris\r\n\r\nde la fruta.', 'Su sintomatología se manifiesta e inicia como una mancha de color marrón oscura a gris. Luego de\r\n\r\nesta mancha presenta unos signos de micelio y esporas de color grisáceo que varía dependiendo de\r\n\r\nla parte de la planta infectada y del estado fisiológico de ese tejido.'),
(3, 'Colletotrichum fragariae', '(CF)', 10, 1, 'Colletotrichum fragariae y/o Colletotrichum gloeosporioides (Antracnosis): Esta enfermedad', 'Presenta síntomas en frutos: como lesiones oscuras, secas, firmes y hundidas con borde de color'),
(4, 'Phytophthora cactorum', '(PH)', 7, 1, 'Phytophthora cactorum (Mal del cuello): Esta enfermedad produce colapso vascular; es decir, que estrangula los haces vasculares y por ende se marchita la planta.', 'Esta enfermedad produce Muerte de raicillas y lesiones necróticas en las raíces principales. Desprendimiento de coronas al manipular la planta que deja al descubierto necrosis central de color café rojizo.'),
(1, 'Monilinia fructicola', '(MO)', 7, 2, 'Esta es la enfermedad más importante que afecta a los cultivos de durazneros. Su importancia radica en el ataque a flores, brotes y frutos ocasionando la destrucción de los mismos. ', 'El primer órgano en ser atacado es la flor produciéndose el marchitamiento, o atizonamiento de la misma. '),
(2, 'Botrytis cinerea', '(BO)', 10, 2, 'Es un hongo patógeno de muchas especies vegetales, animales y bacterias. En viticultura se\r\n\r\nconoce comúnmente como podredumbre de Botrytis; en horticultura normalmente se llama moho\r\n\r\ngris. Esta enfermedad es conocida como pudrición de la fresa, o moho gris de la fruta. Botrytis en\r\n\r\nla fresa, causando pérdidas significativas antes y después de la cosecha debido a que se desarrolla\r\n\r\ntanto en el campo como en el almacenaje y en tránsito. Botrytis es uno de los patógenos más\r\n\r\ndifíciles de controlar cuando las condiciones ambientales favorecen su crecimiento y desarrollo.', 'La manifestación del moho gris varía dependiendo de la parte de la planta infectada y del estado fisiológico de ese tejido. Cuando las hojas infectadas maduran, al iniciar la senescencia, el hongo puede activarse y producir una cubierta aterciopelada gris en las partes muertas de la hoja.'),
(3, 'Taphrina deforms', '(TQ)', 15, 2, 'Este hongo puede afectar hojas, brotes, flores y frutos. El primer síntoma que se observa en\r\n\r\nprimavera es la formación de áreas rojizas sobre las hojas, posteriormente éstas tomarán un\r\n\r\naspecto enrulado y caerán prematuramente. Las flores y frutos atacados, también caerán\r\n\r\ntempranamente, aunque pueden encontrarse frutos afectados en la cosecha. En este caso se\r\n\r\nverán sobre las mismas áreas salientes, de tamaño y forma irregular.', 'Este hongo puede afectar hojas, brotes, flores y frutos. El primer síntoma que se observa es la formación de áreas rojizas sobre las hojas, posteriormente éstas tomarán un aspecto enrulado y caerán prematuramente. Las flores y frutos atacados, también caerán tempranamente, aunque pueden encontrarse frutos afectados en la cosecha. '),
(4, 'Roya', '(RY)', 10, 2, 'Esta enfermedad tiene amplia distribución en el mundo y ocurre esporádicamente, causando graves daños de excesiva humedad, durante la estación de desarrollo vegetativo y reproductivo del cultivo. ', 'En el haz de las hojas, se observan pequeñas manchas de color amarillo, delimitadas por las nervaduras y dispuestas irregularmente, por el envés y en coincidencia con las manchas, se desarrollan los fructificaciones del hongo, que al tacto son aterciopeladas y un poco polvosas. '),
(5, 'Gomosis ', '(GO)', 15, 2, 'La gomosis en duraznos suele derivarse de fenómenos naturales de carácter ambiental, como es el caso de una excesiva sequía o la presencia de agua acumulada en la base del terreno. Como vemos, la goma es la respuesta del árbol a un proceso natural, que se ocasiona por causas muy diferentes. Además de la humedad o la sequía, la insuficiencia de cobre también originará esta enfermedad', 'Las primeras señales visuales van apareciendo en la corteza, en forma de pequeñas gotas, sobre todo cuando los árboles aún son jóvenes. Esta aparición de goma es la consecuencia de que se forme sustancia blanda y gelatinosa, que irá resudando por las grietas, hendiduras o heridas del durazno en forma de lágrimas que se alargan siguiendo la forma de las ramas, y se van cuajando y secando al aire.'),
(1, 'Roña', '(RÑ)', 15, 3, 'Los síntomas de la sarna son muy variables y no se presentan en la parte aérea de la planta.\r\n\r\nGeneralmente son lesiones corchosas irregulares, levantadas, cafesosas, de tamaño variable, que\r\n\r\nse desarrollan en cualquier lugar de la superficie del tubérculo. A veces, los síntomas se\r\n\r\ndesarrollan sólo como una capa superficial de tejido corchoso que cubre gran parte de la\r\n\r\nsuperficie del tubérculo (sarna russet), o como lesiones de hasta 1cm de profundidad, café oscuras\r\n\r\ncon el tejido bajo la lesión de color café claro y translúcido (sarna profunda). En el mismo\r\n\r\ntubérculo pueden presentarse ambos tipos de lesiones. Los síntomas se desarrollan durante la\r\n\r\nestación de crecimiento , pero el período de infección es cuando el tubérculo se está formando.\r\n\r\nPocas semanas después de la infección aparecen pequeñas lesiones circulares acuosas en el\r\n\r\ntubérculo en crecimiento . Los tubérculos maduros ya no son susceptibles a la infección, pero las\r\n\r\nlesiones ya formadas pueden seguir expandiéndose a medida que el tubérculo crece, aumentando\r\n\r\nla severidad del daño. Los síntomas de sarna son más severos cuando el tubérculo se desarrolla\r\n\r\nbajo condiciones templadas y secas. Así, suelos que se secan rápido son más conductivos a la\r\n\r\nsarna.', 'Los síntomas de la sarna son muy variables y  no se presentan en la parte aérea de la planta. Generalmente son lesiones corchosas irregulares, levantadas, cafesosas, de tamaño variable, que se desarrollan en cualquier lugar de la superficie del tubérculo. A veces, los síntomas se desarrollan sólo como una capa superficial de tejido corchoso que cubre gran parte de la superficie del tubérculo (sarna russet), o como lesiones de hasta 1cm de profundidad, café oscuras con el tejido bajo la lesión de color café claro y translúcido (sarna profunda). '),
(2, 'Tizón de la papa', '(PH)', 7, 3, 'El Tizón tardío es la enfermedad más seria en el cultivo de la papa en el mundo. Afecta hojas, tallos y tubérculos y se dispersa rápidamente pudiendo abarcar grandes superficies cuando las condiciones climáticas son favorables.', 'Los síntomas incluyen la aparición de manchas oscuras en las hojas y tallos de plantas. En condiciones de humedad aparecerá un polvo blanco debajo de las hojas y toda la planta puede colapsarse rápidamente. Los tubérculos infectados desarrollan manchas de color gris o negro y que son de color marrón rojizo por debajo de la piel. '),
(3, 'Rhizoctonia solani', '(RS)', 10, 3, 'Es un hongo patógeno de planta con una amplia gama de huéspedes y distribución en todo el\r\n\r\nmundo que existen con frecuencia, como el crecimiento filiforme en las plantas o en cultivo, y se\r\n\r\nconsidera un patógeno transmitido por el suelo. La Rhizoctonia solani es el más conocido para\r\n\r\ncausar diversas enfermedades de las plantas tales como la pudrición del cuello, podredumbre de\r\n\r\nla raíz, la podredumbre, y el vástago de alambre, esta ataca a sus anfitriones cuando están en sus\r\n\r\nprimeras etapas de desarrollo, tales como las semillas y plantas, que se encuentran normalmente\r\n\r\nen el suelo. El patógeno se sabe que causa pérdidas de plantas graves atacando principalmente las\r\n\r\nraíces y tallos de las plantas inferior. A pesar de que tiene una amplia gama de huéspedes, sus\r\n\r\nprincipales objetivos son plantas herbáceas. Rhizoctonia solani sería considerado un hongo\r\n\r\nBasidiomycota si la etapa teleomorfa fueron más abundantes. El patógeno no se conoce\r\n\r\nactualmente para producir las esporas asexuales (conidios), a pesar de que se considera que tiene\r\n\r\nun ciclo de vida asexual. De vez en cuando, las esporas sexuales (basidiosporas) se producen en las\r\n\r\nplantas infectadas. El ciclo de la enfermedad de R. solani es importante en la gestión y el control\r\n\r\ndel patógeno.', 'Rhizoctonia solani no produce esporas. Es un hongo patógeno de planta con una amplia gama de huéspedes y distribución en todo el mundo que existen con frecuencia, como el crecimiento filiforme en las plantas o en cultivo, y se considera un patógeno transmitido por el suelo.  La enfermedad es la más conocido para causar diversas enfermedades de las plantas tales como la pudrición del cuello, podredumbre de la raíz, la podredumbre, y el vástago de alambre, esta ataca a sus anfitriones cuando están en sus primeras etapas de desarrollo, tales como las semillas y plantas, que se encuentran normalmente en el suelo. '),
(4, 'Rosellinia sp', '(RR)', 10, 3, 'Los síntomas externos de la enfermedad son la flacidez del follaje, el marchitamiento y por último,\r\n\r\nla muerte de la planta. Un signo muy notorio en las plantas enfermas es la presencia de una masa\r\n\r\nalgodonosa en la base de los tallos, debida a que los tubérculos infectados se recubren por el\r\n\r\nmicelio del hongo de color blanco. La enfermedad predomina en suelos ricos en materia orgánica,\r\n\r\nmuy ácidos, muy húmedos o mal drenados, donde los residuos de cosecha tardan mucho tiempo\r\n\r\nen descomponerse por las bajas temperaturas, favoreciendo la supervivencia del inóculo. La\r\n\r\nenfermedad se disemina principalmente por tubérculos-semilla infectados, suelo contaminado,\r\n\r\nagua y herramientas.', 'Los síntomas externos de la enfermedad son la flacidez del follaje, el marchitamiento y por último, la muerte de la planta. Un signo muy notorio en las plantas enfermas es la presencia de una masa algodonosa en la base de los tallos, debida a que los tubérculos infectados se recubren por el micelio del hongo de color blanco. La enfermedad predomina en suelos ricos en materia orgánica, muy ácidos, muy húmedos o mal drenados, donde los residuos de cosecha tardan mucho tiempo en descomponerse por las bajas temperaturas, favoreciendo la supervivencia del inóculo. '),
(1, 'Phytophthora ', '(PH)', 5, 5, 'Denominada también pudrición del pie, es una enfermedad fungosa común en los cítricos de Colombia. ', 'Presentan desarrollo lento con débil emisión de hojas y amarilla-miento generalizado.'),
(2, 'Colletotrichum ', '(CO)', 13, 5, 'Los síntomas se manifiestan en los pétalos de las flores en forma de manchas de color marrón, sobre las cuales crecen los fructificaciones del hongo en forma de acérvalos de color anaranjado, muy vivos.', 'Normalmente, esta enfermedad causa lesiones solamente en flores abiertas, pero si las condiciones son muy favorables también puede afectar las flores en botón e incluso en cabeza de alfiler.'),
(1, 'Roya, Hemileia vastratix', '(RY)', 10, 4, 'Corresponden al polvillo amarillo o naranja que se visualiza en el envés de las hojas de café y que es característico de esta enfermedad. ', ''),
(2, 'La llaga macana, Ceratocystis ', '(CF)', 7, 4, 'Los troncos del cafeto son atacados generalmente al nivel del suelo.', 'La característica de esta enfermedad es la presencia de lesiones de color bruno rojizo o negro, las cuales no penetran profundamente en el leño y pueden verse al extraer la corteza. Las plantas se ponen amarillas, posteriormente pierden las hojas y finalmente mueren.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plagas`
--

CREATE TABLE `plagas` (
  `id_plaga` int(20) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `sigla` varchar(20) NOT NULL,
  `limite_porcentaje` int(10) NOT NULL,
  `id_cult` int(10) NOT NULL,
  `descripcion` text,
  `sint_sig` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `plagas`
--

INSERT INTO `plagas` (`id_plaga`, `nombre`, `sigla`, `limite_porcentaje`, `id_cult`, `descripcion`, `sint_sig`) VALUES
(1, 'Chiza o mojoy', '(CH)', 15, 1, 'El Mojojoy es una larva subterránea, de varios tipos de cucarrones, que tiene como costumbre\r\n\r\narrasar las raíces de todas las plantas, causando un daño económico enorme. A veces echa a\r\n\r\nperder toda una cosecha o hace que los productos se presenten en forma antiestética en los\r\n\r\nmercados.\r\n\r\nEl control de esta chiza representa el 22 por ciento de los costos de producción y se realiza con\r\n\r\nproductos de categoría toxicológica I, lo que ha generado el uso indiscriminado de pesticidas\r\n\r\nocasionando una fuerte contaminación ambiental.\r\n\r\nSin embargo, existen controladores naturales que pueden ser usados en forma comercial, entre\r\n\r\nlos más importantes se encuentran varios tipos de hongos.', 'Hojas rojizas; al tocar la planta se encuentra floja o sin raíces. Larvas capaces de cortar las raíces hasta el inicio de las coronas.'),
(3, 'Acaros', '(AC)', 5, 1, 'Es uno de los ácaros que más ataca a los frutales, es muy polífago. Presentan tubérculos blancos\r\n\r\ncon tecas. El huevo es muy característico, está achatado por los polos y lleva una especie de mástil\r\n\r\ndel cual surgen vientos de cera que lo unen al sustrato. Hibernan en estado de huevo,\r\n\r\napelotonados en los brotes de las axilas, las larvas aparecen en primavera y se localizan en el\r\n\r\nenvés de las hojas, produciendo decoloraciones plomizas en las hojas de los frutales. Estas hojas\r\n\r\nacaban secándose y caen al suelo. Es una plaga típica de verano. El mejor momento para tratarlo\r\n\r\nes cuando han avivado el 70 % de los huevos.', 'Se localizan en el envés de las hojas, produciendo decoloraciones plomizas en las hojas de los frutales. Estas hojas acaban secándose y caen al suelo. Es una plaga típica de verano.'),
(1, 'Tiro de munición', '(TM)', 20, 2, 'Produce conidias en acérvulos provistos de setas de color oscuro que se desarrollan en los tejidos\r\n\r\ninfectados. Las conidias son multicelulares agazadas hacia los extremos y provistas de septos.', 'Pequeñas lesiones necróticas, circulares rodeadas de un halo rojizo se presentan en hojas, ramillas\r\n\r\ny frutos. Las lesiones de las hojas se caen dejandolas perforadas. Causan una seria desfoliación.'),
(2, 'Mosca de la fruta', '(MF)', 4, 2, 'Es una especie de díptero braquícero de la familia Drosophilidae. Recibe su nombre debido a que\r\n\r\nse alimenta de frutas en proceso de fermentación. Esta es una especie utilizada frecuentemente\r\n\r\nen experimentación genética, dado que posee un reducido número de cromosomas (4 pares),\r\n\r\nbreve ciclo de vida (15-21 días) y aproximadamente el 61 % de los genes de enfermedades\r\n\r\nhumanas que se conocen tienen una contrapartida identificable en el genoma de las moscas de la\r\n\r\nfruta, y el 50 % de las secuencias proteínicas de la mosca tiene análogos en los mamíferos.', 'Los primeros daños que producen a la fruta son debidos a las picadas que efectúa la hembra para depositar los huevos. La picada produce, de entrada, una vía de infección de hongos que favorecen el deterioro del fruto.\r\nEl segundo síntoma lo producen las larvas que se alimentan de la carne del fruto y destruyen la pulpa por completo.\r\n'),
(3, 'Acaros', '(AC)', 10, 2, NULL, NULL),
(4, 'Trips', '(TR)', 7, 2, 'Los trips pertenecen al orden de los tisanópteros, son insectos pequeños, normalmente de entre 1\r\n\r\ny 6 mm. Se conocen unas 5600 especies y muchas de ellas son plagas de especies vegetales\r\n\r\ncultivadas. Pueden vivir de un mes a un año y pasan distintas fases de metamorfosis dependiendo\r\n\r\nde la especie. Suelen tener un color oscuro, siendo normalmente marrones o negros los adultos y\r\n\r\namarillas las larvas. Los hay alados y ápteros (sin alas).\r\nLos trips, al igual que la mayoría de insectos chupadores, se sitúan en el envés de las hojas ya que\r\n\r\nes la zona con mayor porosidad(es en el envés donde se sitúan los estomas y se realiza el\r\n\r\nintercambio gaseoso) y accesibilidad para su aparato bucal chupador. El haz en cambio es\r\n\r\ntotalmente impermeable y por tanto inaccesible para el pulgón.', NULL),
(1, 'Pulguilla', '(PU)', 20, 3, 'La pulguilla también se llama epitrix, pulga saltona, o piqui piqui. Es una plaga que hace mucho\r\n\r\ndaño a la papa, baja la calidad de la producción y se vende a un precio menor, ocasionando\r\n\r\npérdidas económicas a los productores. La pulguilla es como una waka waka pequeña. Sus crías\r\n\r\nson como pequeños gusanos.\r\nLos gusanitos viven en el suelo, causando daños en la piel de la papa, haciendo unos caminitos en\r\n\r\ntoda la cáscara de la papa. Cuando el se convierte en pulguilla sube a las hojas de la planta y hace\r\n\r\nunos pequeños hoyitos redondos. También puede moverse de cultivos viejos a cultivos nuevos. La\r\n\r\npulguilla come hierba que parece tomate silvestre. Si aplica insecticidas no específicos o si fumiga\r\n\r\ncon el mismo veneno por más de dos veces, el insecto se acostumbra y el veneno no lo mata.', NULL),
(2, 'Chiza', '(CH)', 15, 3, 'Esta es una larva subterránea, de varios tipos de cucarrones, que tiene como costumbre arrasar las\r\n\r\nraíces de todas las plantas, causando un daño económico enorme. A veces echa a perder toda una\r\n\r\ncosecha o hace que los productos se presenten en forma antiestética en los mercados.\r\n\r\nEl control de esta chiza representa el 22 por ciento de los costos de producción y se realiza con\r\n\r\nproductos de categoría toxicológica I, lo que ha generado el uso indiscriminado de pesticidas\r\n\r\nocasionando una fuerte contaminación ambiental.\r\n\r\nSin embargo, existen controladores naturales que pueden ser usados en forma comercial, entre\r\n\r\nlos más importantes se encuentran varios tipos de hongos.', 'Esta es una larva subterránea, de varios tipos de cucarrones, que tiene como costumbre arrasar las raíces de todas las plantas, causando un daño económico enorme. A veces echa a perder toda una cosecha o hace que los productos se presenten en forma antiestética en los mercados.'),
(3, 'Polilla', '(PO)', 7, 3, 'La pulguilla también se llama epitrix, pulga saltona, o piqui piqui. Es una plaga que hace mucho daño a la papa, baja la calidad de la producción y se vende a un precio menor, ocasionando pérdidas económicas a los productores.', 'Los gusanitos viven en el suelo, causando daños en la piel de la papa, haciendo unos caminitos en toda la cáscara de la papa. Cuando el se convierte en pulguilla sube a las hojas de la planta y hace unos pequeños hoyitos redondos. También puede moverse de cultivos viejos a cultivos nuevos. La pulguilla come hierba que parece tomate silvestre. Si aplica insecticidas no específicos o si fumiga con el mismo veneno por más de dos veces, el insecto se acostumbra y el veneno no lo mata. '),
(4, 'Babosas', '(BA)', 15, 3, 'Las babosas están clasificadas como moluscos fitófagos, del Orden Gasterópoda; son organismos\r\n\r\nherbívoros de distribución mundial, conocidos como plagas de jardines, plantas ornamentales,\r\n\r\nfrutales, leguminosas, musáceas, forestales, viveros y de hortalizas sembradas en exteriores o en\r\n\r\ninvernadero. Las babosas presentan hábitos nocturnos, viven en colonias en sitios húmedos y\r\n\r\noscuros. Son hermafroditas (los dos sexos en un mismo individuo) y pueden autofecundarse.\r\n\r\nTambién ocurre apareamiento mutuo entre individuos, por eso en un momento dado, pueden\r\n\r\nactuar como machos o hembras.', NULL),
(5, 'Cogollero', '(CG)', 10, 3, 'El gusano cogollero (Helicoverpa armigera), también conocido como oruga de la col, oruga del\r\n\r\ntabaco y oruga del Viejo Mundo es una especie de lepidóptero ditrisio de la familia Noctuidae\r\n\r\ncuyas larvas se alimentan de una amplia gama de plantas incluyendo plantas cultivadas. Son plagas\r\n\r\npolífagas y cosmopolitas.', NULL),
(1, 'Compsus', '(SP)', 7, 5, 'Los daños causados por los estados larvarios no solo afectan el rendimiento, sino el tamaño y la calidad del fruto. ', 'Causan daño al alimentarse de brotes y frutos tiernos, este último daño es más significativo en plantaciones cuyas frutas tienen como destino el mercado del producto en fresco.'),
(2, 'Phyllocnistis citrella', '(PC)', 15, 5, 'Las larvas viven en galerías, también llamadas minas, que son subepidérmicas, produciendo una pérdida de la masa foliar que se traduce en una reducción del rendimiento y de la cosecha.', 'Los ataques provocan una disminución del crecimiento. Las hojas y los brotes atacados se secan como consecuencia de la rotura y el desprendimiento de la cutícula que deja el parénquima al sol.'),
(3, 'Planococcus citrí', '(PN)', 20, 5, 'El par de estiletes maxilares delimitan el canal salivar, a través del cual la saliva es inyectada en la planta, y el canal alimenticio, a través del cual es absorbida la savia.', 'Cuando se sitúa en el cáliz de los frutos recién cuajados puede provocar su caída prematura.'),
(4, 'Ácaros', '(AC)', 10, 5, 'Pueden sufrir daños en las hojas, flores y frutos debido al ataque de ácaros.', 'Vive en los frutos y las hojas, especialmente en el envés. Los daños producidos en frutos y hojas se conocen como: tostado, piel de lija y bronceado y son muy comunes en el cultivo de la naranja.'),
(1, 'Broca del café, Hypothenemus h', '(BC)', 5, 4, 'Las larvas son vermiformes, ápteras, ápodas, blancas y de cabeza marrón. Miden de 0,7 a 2,2 mm de largo y de 0,2 a 0,6 mm de diámetro. Hay dos fases larvales para las hembras y una para los machos. Tienen mandíbulas fuertes prolongadas hacia adelante, su cuerpo está cubierto por pilosidad blanca; este estadio dura de 10 a 26 días. Las pupas son al principio blancas pero luego se van amarilleando y pueden medir entre 0,5 y 1,9 mm.\r\n\r\nLos adultos son similares a pequeños gorgojos de color negro. Las hembras miden de 1,4 a 1,8 mm de largo y 0,8 de ancho y los machos, más pequeños, miden de 1,2 a 0,6 mm. Los machos son ápteros mientras que las hembras pueden volar distancias cortas. Las hembras tienen el margen frontal del pronoto con cuatro, o a veces seis, dientes (quetas erectas). La sutura mediana frontal de la cabeza es grande y bien definida. H. hampei se confunde a veces con la falsa broca (H. obscurus o H. seriatus), pero éstas no llegan a acceder al endoesperma de las semillas; además se pueden diferenciar por la forma de las sedas de los élitros. Basándose en las similitudes morfológicas H. hampei también puede confundirse con Xylosandrus (Scolytidae) o algunos otros géneros que causan daños similares en los cultivos de café.', 'Hace el daño al atacar el fruto del café y reproducirse internamente en el endospermo, causando la pérdida total del grano y en muchos casos su caída prematura, además reduce la calidad del producto final.'),
(2, 'Minador de la hoja, Leucoptera', '', 15, 4, NULL, 'El daño lo ocasiona la larva. Una sola larva puede consumir entre 1,0 y 2,0 cm² de área foliar durante su desarrollo y causar necrosamiento de mas del 80% de las hojas, posteriormente defoliación.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registo_cultivo`
--

CREATE TABLE `registo_cultivo` (
  `id` int(20) NOT NULL,
  `tipo_cultivo` int(20) DEFAULT NULL,
  `descripcion` text,
  `lotes` int(10) DEFAULT NULL,
  `id_usu` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registo_cultivo`
--

INSERT INTO `registo_cultivo` (`id`, `tipo_cultivo`, `descripcion`, `lotes`, `id_usu`) VALUES
(1, 1, 'cultivo de 1000 plantas Chitaga.', 2, 1),
(2, 4, 'Cultivo de 100 plantas Villacaro', 3, 1),
(3, 5, '1', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_enfermedades`
--

CREATE TABLE `registro_enfermedades` (
  `id_usu` int(10) DEFAULT NULL,
  `id_enfe` int(10) DEFAULT NULL,
  `id_cult` int(10) DEFAULT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `sigla` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `limite_porcentaje` varchar(10) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `registro_enfermedades`
--

INSERT INTO `registro_enfermedades` (`id_usu`, `id_enfe`, `id_cult`, `nombre`, `sigla`, `limite_porcentaje`) VALUES
(1, 1, 1, 'Botrytis cinérea (Moho gris)', '(BO)', '20'),
(1, 3, 1, 'Colletotrichum fragariae', '(CF)', '10'),
(1, 4, 1, 'Phytophthora cactorum', '(PH)', '7'),
(1, 1, 2, 'Monilinia fructicola', '(MO)', '7'),
(1, 2, 2, 'Botrytis cinerea', '(BO)', '10'),
(1, 3, 2, 'Taphrina deforms', '(TQ)', '15'),
(1, 4, 2, 'Roya', '(RY)', '10'),
(1, 5, 2, 'Gomosis ', '(GO)', '15'),
(1, 1, 3, 'Roña', '(RÑ)', '15'),
(1, 2, 3, 'Tizón de la papa', '(PH)', '7'),
(1, 3, 3, 'Rhizoctonia solani', '(RS)', '10'),
(1, 4, 3, 'Rosellinia sp', '(RR)', '10'),
(1, 1, 5, 'Phytophthora ', '(PH)', '5'),
(1, 2, 5, 'Colletotrichum ', '(CO)', '13'),
(1, 1, 4, 'Roya, Hemileia vastratix', '(RY)', '10'),
(1, 2, 4, 'La llaga macana, Ceratocystis ', '(CF)', '7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_plagas`
--

CREATE TABLE `registro_plagas` (
  `id_usu` int(10) DEFAULT NULL,
  `id_plag` int(10) DEFAULT NULL,
  `id_cult` int(10) DEFAULT NULL,
  `nombre` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `sigla` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `limite_porcentaje` varchar(10) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `registro_plagas`
--

INSERT INTO `registro_plagas` (`id_usu`, `id_plag`, `id_cult`, `nombre`, `sigla`, `limite_porcentaje`) VALUES
(1, 1, 1, 'Chiza o mojoy', '(CH)', '15'),
(1, 3, 1, 'Acaros', '(AC)', '5'),
(1, 1, 2, 'Tiro de munición', '(TM)', '20'),
(1, 2, 2, 'Mosca de la fruta', '(MF)', '4'),
(1, 3, 2, 'Acaros', '(AC)', '10'),
(1, 4, 2, 'Trips', '(TR)', '7'),
(1, 1, 3, 'Pulguilla', '(PU)', '20'),
(1, 2, 3, 'Chiza', '(CH)', '15'),
(1, 3, 3, 'Polilla', '(PO)', '7'),
(1, 4, 3, 'Babosas', '(BA)', '15'),
(1, 5, 3, 'Cogollero', '(CG)', '10'),
(1, 1, 5, 'Compsus', '(SP)', '7'),
(1, 2, 5, 'Phyllocnistis citrella', '(PC)', '15'),
(1, 3, 5, 'Planococcus citrí', '(PN)', '20'),
(1, 4, 5, 'Ácaros', '(AC)', '10'),
(1, 1, 4, 'Broca del café, Hypothenemus h', '(BC)', '5'),
(1, 2, 4, 'Minador de la hoja, Leucoptera', '', '15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cultivos`
--

CREATE TABLE `tipo_cultivos` (
  `id_cult` int(10) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_cultivos`
--

INSERT INTO `tipo_cultivos` (`id_cult`, `nombre`) VALUES
(1, 'Fresa'),
(2, 'Durazno'),
(3, 'Papa'),
(4, 'Cafe'),
(5, 'Citricos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usu` int(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `genero` int(5) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `date_registry` varchar(250) NOT NULL,
  `photo` longblob NOT NULL,
  `alt_photo` varchar(250) NOT NULL,
  `name_photo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usu`, `nombre`, `telefono`, `genero`, `correo`, `clave`, `date_registry`, `photo`, `alt_photo`, `name_photo`) VALUES
(1, 'jose rodolfo', '3229008256', 0, 'jose@gmail.com', '662eaa47199461d01a623884080934ab', '', '', '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `control_enfermedades`
--
ALTER TABLE `control_enfermedades`
  ADD PRIMARY KEY (`id_registro`),
  ADD KEY `id_usu` (`id_usu`),
  ADD KEY `tipo_cultivo` (`tipo_cultivo`);

--
-- Indices de la tabla `control_plagas`
--
ALTER TABLE `control_plagas`
  ADD PRIMARY KEY (`id_registro`),
  ADD KEY `id_usu` (`id_usu`),
  ADD KEY `tipo_cultivo` (`tipo_cultivo`);

--
-- Indices de la tabla `enfermedades`
--
ALTER TABLE `enfermedades`
  ADD KEY `id_cult` (`id_cult`);

--
-- Indices de la tabla `plagas`
--
ALTER TABLE `plagas`
  ADD KEY `id_cult` (`id_cult`);

--
-- Indices de la tabla `registo_cultivo`
--
ALTER TABLE `registo_cultivo`
  ADD KEY `id` (`id`),
  ADD KEY `tipo_cultivo` (`tipo_cultivo`),
  ADD KEY `id_usu` (`id_usu`);

--
-- Indices de la tabla `registro_enfermedades`
--
ALTER TABLE `registro_enfermedades`
  ADD KEY `id_usu` (`id_usu`);

--
-- Indices de la tabla `registro_plagas`
--
ALTER TABLE `registro_plagas`
  ADD KEY `id_usu` (`id_usu`);

--
-- Indices de la tabla `tipo_cultivos`
--
ALTER TABLE `tipo_cultivos`
  ADD PRIMARY KEY (`id_cult`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `control_enfermedades`
--
ALTER TABLE `control_enfermedades`
  MODIFY `id_registro` int(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `control_plagas`
--
ALTER TABLE `control_plagas`
  MODIFY `id_registro` int(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `registo_cultivo`
--
ALTER TABLE `registo_cultivo`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tipo_cultivos`
--
ALTER TABLE `tipo_cultivos`
  MODIFY `id_cult` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usu` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `control_enfermedades`
--
ALTER TABLE `control_enfermedades`
  ADD CONSTRAINT `control_enfermedades_ibfk_1` FOREIGN KEY (`tipo_cultivo`) REFERENCES `tipo_cultivos` (`id_cult`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `control_enfermedades_ibfk_2` FOREIGN KEY (`id_usu`) REFERENCES `usuarios` (`id_usu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `control_plagas`
--
ALTER TABLE `control_plagas`
  ADD CONSTRAINT `control_plagas_ibfk_1` FOREIGN KEY (`id_usu`) REFERENCES `usuarios` (`id_usu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `control_plagas_ibfk_2` FOREIGN KEY (`tipo_cultivo`) REFERENCES `tipo_cultivos` (`id_cult`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `enfermedades`
--
ALTER TABLE `enfermedades`
  ADD CONSTRAINT `enfermedades_ibfk_1` FOREIGN KEY (`id_cult`) REFERENCES `tipo_cultivos` (`id_cult`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `plagas`
--
ALTER TABLE `plagas`
  ADD CONSTRAINT `plagas_ibfk_1` FOREIGN KEY (`id_cult`) REFERENCES `tipo_cultivos` (`id_cult`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `registo_cultivo`
--
ALTER TABLE `registo_cultivo`
  ADD CONSTRAINT `registo_cultivo_ibfk_1` FOREIGN KEY (`tipo_cultivo`) REFERENCES `tipo_cultivos` (`id_cult`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `registo_cultivo_ibfk_2` FOREIGN KEY (`id_usu`) REFERENCES `usuarios` (`id_usu`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
